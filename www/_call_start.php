<?php
//tipo de ENV
/*if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' || $_SERVER['REMOTE_ADDR'] == '127.0.1.1' || $_SERVER['REMOTE_ADDR'] == '::1') {
    define('ENV', 'local');
    define('FRAMEWORK_PATH', 'C:/xampp/htdocs/LSCFW/Framework/');
} else {
    define('ENV', 'online');
    //define('AMBIENTE', 'test');
    define('FRAMEWORK_PATH', SYS_PATH . 'lib/Framework/');
}

NAO SUBIR ESTE ARQUIVO PARA PRODUÇÃO!

*/
define('ENV', 'local');
    define('FRAMEWORK_PATH', SYS_PATH . 'lib/Framework/');
//include path
set_include_path(implode(PATH_SEPARATOR, array(
            FRAMEWORK_PATH,
            SYS_PATH . 'app_layers/controllers/',
            SYS_PATH . 'app_layers/models/table/'
        )));

//testando a existencia de framework
if(!is_dir(FRAMEWORK_PATH)){
    if(ENV != 'online'){
        die('Framework n&atilde;o encontrado em '.FRAMEWORK_PATH);
    }
    die('Framework n&atilde;o encontrado.');
}

//aplicação de tratamento para arquivos e não rotas
$requisicao = explode('?', $_SERVER['REQUEST_URI']);
$requisicao = "http://" . $_SERVER['HTTP_HOST'] . $requisicao[0];
//só pra garantir que houve o trabalho correto do rewrite
if (!file_exists($requisicao) && !is_dir($requisicao) && file_exists(SYS_PATH . 'bootstrap.php')) {
    require_once SYS_PATH . 'bootstrap.php';
}
?>
