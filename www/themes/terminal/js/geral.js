function scrollTerminal(){
    $j("#terminal .area").attr({
        scrollTop: $j("#terminal .area").attr("scrollHeight")
    });
}
var lastC = new Array();//las command
var c_array = c_array_atual = 0;
function enviar(e,valor){
    if(e.keyCode == 13){
        $j('#send').val('');
        var v = valor;
        var p = v.split(' ');
        if(p.length > 1 || valor == 'logout'){
            //exceções
            if(p.length == 1){
                v = valor+' ';
                p = v.split(' ');
            }
            var c = p.shift();//comando
            if(c == 'safe'){
                var ctrl = 'index/';
                c = p.shift();//retira o safe e get comando
            }else{
                if(c == 'sql'){
                    var ctrl = 'sql/';
                    c = p.shift();//retira o sql e get comando
                }else{
                    var ctrl = 'comandos/';
                }
            }
            $j('.comandos .status').html('Enviando...');
            var params = p.join(' ');//parametros
            $j.ajax({
                url: BASE_URL+PERMALINK+'/'+ctrl+c,
                type: "POST",
                data: {
                    format : 'ajax',
                    params: params
                },
                error: function(resp){
                    console.log(resp);
                    $j('.comandos .status').html('Erro!');
                    scrollTerminal();
                },
                success: function(resp){
                    var string = '';
                    if(strpos(resp,'<div style="color: red;')){
                        string = '<a href="javascript: void(0)">Retornou erros.</a><div class="exception">'+resp+'</div>';
                        //epa, aih tem erro
                        $j('.comandos .status').html('Erro.');
                    }else{
                        string = resp;
						if(string == '#login-ok#'){
							string = 'Identificação realizada com sucesso.';
							$j('#send-login').hide();
						}
                        $j('.comandos .status').html('Pronto!');
                    }
                    //montando as divs
                    resposta = '<div class="comando">'+c+' &raquo;</div><div class="resposta">'+string+'</div>';
                    $j('#terminal .area').append(resposta);
                    scrollTerminal();
                }
            });
            lastC.push(valor);
            c_array++;
            c_array_atual = lastC.length-1;
        }
        //comandos simples da tela
        if(valor == 'clear'){
            $j('#terminal .area').html('');
        }
        if(valor == 'exit'){
            window.location = SITE_URL;
        }
    }else{
        //up: 38 e down: 40
        if(e.keyCode == 38){
            $j('#send').val(lastC[c_array_atual]);
            c_array_atual--;
            if(c_array_atual <0)
                c_array_atual=0;
        }
        if(e.keyCode == 40){
            $j('#send').val(lastC[c_array_atual]);
            c_array_atual++;
            if(c_array_atual > lastC.length)
                c_array_atual=lastC.length;
        }
    }
}
jQuery(document).ready(function($){
    $('#send').focus();
    $j('.resposta a').live('click',function(){
        $j(this).next().toggle();
    });
	try{
		$('#send-login input').focus();
	}catch(e){}
});