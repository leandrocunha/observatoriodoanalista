/* 
 * Montando componentes em jquery ui
 */
jQuery(document).ready(function($) {

    $.fx.speeds._default = 1000;
    //botões
    $("#container .input-button").button();
    $("#login-box .input-button").button();
    $('#caixa-dialogo').dialog({autoOpen: false, resizable: false, modal: true});
    //qualker coisa q levar a classe location tratar o atributo url
    $('.acoes a').each(function(){
        if($(this).attr('url') != undefined){
            $(this).attr('href','javascript:void(0)');
        }
    });
    $('.location').click(function(){
        var ok = true;
        if($(this).attr('url') != undefined){
            if($(this).hasClass('deletar')){
                ok = confirm("Deseja realmente excluir o registro?");
            }
        }
        if(ok){
            window.location = $(this).attr('url');
        }
    });
    //zebra
    $('.listagem .tabela tbody tr:odd').addClass('zebra');
    window.setTimeout(sumirAlertas,15000);
    //ajax
    $j.prettyLoader({
        loader: BASE_URL+'lib/prettyLoader/images/prettyLoader/ajax-loader.gif'
    });
    get_prettyphoto();
    //adicionando a classe nos label obrigatorios
    $j('input.obrigatorio,textarea.obrigatorio,select.obrigatorio,file.obrigatorio').each(function(){
        $j(this).prev('div.label').addClass('obrigatorio');
    });
    //tooltips
    $j('.tooltip').each(function(){
        var tit = $j(this).attr('title');
        if(tit.length > 1){
            //tratando o title como original-title
            $j(this).removeAttr('title');
            $j(this).attr('original-title',tit);
            var g = $j(this).data('gravity');
            if(g.length < 1){
                g = 'se';
            }
            //tipsy
            $j(this).tipsy({
                gravity: g,
                html: true,
                fade: false
            });
        }
    });
    $('.tablesorter').each(function(){
        var ind = parseInt($(this).find('th').length)-1;
        if(ind > 0 && $(this).hasClass('sorter-grid')){
            eval('$(this).tablesorter({headers: {'+ind+': {sorter: false}}});');
        }else{
            $(this).tablesorter();
        }
    }); 
});
function alertar(msg){
    $j('#caixa-dialogo').html('<p>'+msg+'</p>');
    $j('#caixa-dialogo').dialog('open');
}
function sumirAlertas(){
    $j('#container .box-alert').slideUp(300);
}
function get_prettyphoto(){
    $j('a[rel^=prettyPhoto]').prettyPhoto({
            show_title: false,
            theme: 'dark_rounded',
            deeplinking: false,
            slideshow: false,
            autoplay: false,
            social_tools: ''
        });
}
function lembrarSenha(){
    $j('#caixa-dialogo').dialog('open');
}
function alterarStatus(elem,path){
    $j.ajax({
        url: path,
        data: {
            layout: 'ajax'
        },
        success: function( resp ) {
            _elem = $j(elem).parent().parent().find('.status-col');
            _elem.removeClass();
            _elem.addClass('status-col status-'+resp);
        },
        error: function(resp){
            console.log(resp);
            alert('Ajax Error');
        }
    });
}