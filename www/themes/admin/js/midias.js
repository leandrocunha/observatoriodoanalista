function excluirMidias(){
    if(confirm('Deseja realmente excluir as mídias selecionadas?')){
        $j('#form_del_midias').submit();
    }
}
function editarMidia(m_id,tipo){
    $j("#ui-dialog-title-caixa-dialogo").html('Editando mídia');
    $j('#caixa-dialogo #form_edit #id').val(m_id);
    $j('#caixa-dialogo #form_edit #ordem').val($j("#midia_linha_"+m_id+' .m-ordem').html());
    $j('#caixa-dialogo #form_edit #titulo').val($j("#midia_linha_"+m_id+' .m-titulo').html());
    $j("#caixa-dialogo #form_edit #tipo option[value="+tipo+"]").attr('selected','selected');
    $j('#caixa-dialogo').dialog('open');
}
function editarItem(m_id,tipo){
    $j("#ui-dialog-title-caixa-dialogo").html('Editando mídia');
    $j('#caixa-dialogo #form_edit #id').val(m_id);
    $j('#caixa-dialogo #form_edit #ordem').val($j("#midia_linha_"+m_id+' .m-ordem').html());
    $j('#caixa-dialogo #form_edit #titulo').val($j("#midia_linha_"+m_id+' .m-titulo').html());
    $j('#caixa-dialogo #form_edit #galeria_tipo').val($j("#midia_linha_"+m_id+' .m-galtipo').html());
    $j("#caixa-dialogo #form_edit #tipo option[value="+tipo+"]").attr('selected','selected');
    $j('#caixa-dialogo').dialog('open');
}
function selecionarTM(){
    //desmarcar somente se todos estiverem marcados
    var aux = false;//se algum estiver marcado vai ser true
    $j('.tabela .midias input[type=checkbox]').each(function(){
        if(!$j(this).is(':checked')){
            aux = true;
        }
    });
    if(aux){//se tiver pelo menos 1, eh pra marcar todos msm assim
        $j('.tabela .midias input[type=checkbox]').attr('checked',true);
    }else{
        //senao segue a regra normal
        if($j('.tabela .midias input[type=checkbox]').is(':checked')){
            $j('.tabela .midias input[type=checkbox]').attr('checked',false);
        }else{
            $j('.tabela .midias input[type=checkbox]').attr('checked',true);
        }
    }
}