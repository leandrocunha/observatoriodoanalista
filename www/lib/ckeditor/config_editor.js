/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config ){
        config.extraPlugins = 'embed';
        config.filebrowserBrowseUrl = SITE_URL + 'lib/ckeditor/ckfinder/ckfinder.html';
        config.filebrowserImageBrowseUrl = SITE_URL + 'lib/ckeditor/ckfinder/ckfinder.html?type=Images';
        config.filebrowserFlashBrowseUrl = SITE_URL + 'lib/ckeditor/ckfinder/ckfinder.html?type=Flash';
        config.filebrowserUploadUrl = BASE_URL + 'lib/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
        config.filebrowserImageUploadUrl = BASE_URL + 'lib/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
        config.filebrowserFlashUploadUrl = BASE_URL + 'lib/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
        config.language = 'pt-br';
        config.uiColor = '#D7DBDD';
        config.toolbar_root = [
            { name: 'document',    items : [ 'Source','-','Save','DocProps','Print','-','Templates' ] },
            { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
            { name: 'editing',     items : [ 'Find','Replace','-','SelectAll'] },
            { name: 'colors',      items : [ 'TextColor','BGColor' ] },
            '/',
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
            { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
            { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
            '/',
            { name: 'styles',      items : [ 'Format','Font','FontSize' ] },
            { name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
            { name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','Embed' ] }
        ];
        config.toolbar_basico = [
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat','SelectAll' ] },
            { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
            { name: 'links',       items : [ 'Link','Unlink','Anchor' ] }
        ];
        config.toolbar_comum = [
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat','SelectAll' ] },
            { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
            { name: 'outers',       items : [ 'Link','Unlink','Anchor','TextColor','BGColor','Format' ] }
        ];
};
