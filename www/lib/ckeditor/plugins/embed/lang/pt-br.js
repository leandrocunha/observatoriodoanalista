CKEDITOR.plugins.setLang('embed', 'pt-br',
{
  embed : 
  {
    title : "Colar conteúdo embutido (EMBED)",
    button : "Colar conteúdo embutido (EMBED)",
    pasteMsg : "Cole (Ctrl + V) abaixo, seu conteúdo embutido (embed-code) seja ele do Youtube, GoogleMaps, Myspace, Flickr ou outros tipos de EMBED, e click OK."
  }
});
