/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config ){
        config.extraPlugins = 'embed';
        config.language = 'pt-br';
        config.uiColor = '#A7BDD3';
        config.toolbar_root = [
            { name: 'document',    items : [ 'Source','-','Save','DocProps','Print','-','Templates' ] },
            { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
            { name: 'editing',     items : [ 'Find','Replace','-','SelectAll'] },
            { name: 'colors',      items : [ 'TextColor','BGColor' ] },
            '/',
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
            { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
            { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
            '/',
            { name: 'styles',      items : [ 'Format','Font','FontSize' ] },
            { name: 'insert',      items : [ 'Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
            { name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','Embed' ] }
        ];
        config.toolbar_basico = [
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat','SelectAll' ] },
            { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
            { name: 'links',       items : [ 'Link','Unlink','Anchor' ] }
        ];
};
