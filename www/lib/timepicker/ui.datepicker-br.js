/**
 * Tradução da API JQuery UI Datepicker
 * Autor: Leandro Cunha
 * Fonte: www.ferramentasparaweb.com.br
 *
 */
jQuery(function($){
	$.datepicker.regional['br'] = {
                clearText: 'Limpar', clearStatus: '',
		closeText: '[X]', closeStatus: 'Fechar sem modificar',
		prevText: 'Anterior', prevStatus: 'Anterior',
		nextText: 'Proximo', nextStatus: 'Proximo',
		currentText: 'Agora', currentStatus: 'Mês atual',
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho',
		'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
		'Jul','Ago','Set','Out','Nov','Dez'],
		monthStatus: 'Mostra outro mês', yearStatus: 'Mostra outro ano',
		weekHeader: 'Sem', weekStatus: '',
		dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
		dayNamesMin: ['D','S','T','Q','Q','S','S'],
		dayStatus: 'Utilize DD como 1º dia da semana', dateStatus: 'Escolha o DD, MM d',
		dateFormat: 'dd/mm/yy', firstDay: 0,
                timeOnlyTitle: 'Horário',
                timeText: 'Tempo',
                hourText: 'Horas',
                minuteText: 'Minutos',
                secondText: 'Segundos',
		initStatus: 'Escolha a data', isRTL: false
            };
	$.datepicker.setDefaults($.datepicker.regional['br']);

});
function mudarBRDTP(){
    $j(".ui-widget-content button.ui-datepicker-current").html('Agora');
    $j(".ui-widget-content button.ui-datepicker-close").html('OK');
    $j(".ui-timepicker-div .ui_tpicker_time_label").html('Horário');
    $j(".ui-timepicker-div .ui_tpicker_hour_label").html('Horas');
    $j(".ui-timepicker-div .ui_tpicker_minute_label").html('Minutos');
}