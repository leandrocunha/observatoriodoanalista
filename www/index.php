<?php

#CONSTANTES GERAIS:
define('ABSOLUTE_URL', "http://" . $_SERVER['HTTP_HOST'] . preg_replace('/index.php.*/', '', $_SERVER['PHP_SELF'])); //http://localhost/fpw/projeto/www/
define('INDEX_PATH', (preg_replace('/index.php.*/', '', __FILE__)));//usado em includes pelas libs e plugins
define('SYS_PATH', dirname(__FILE__) . '/../sys/'); //usado pela aplicação em includes
define('BASE_URL', preg_replace('/index.php.*/', '', $_SERVER['PHP_SELF'])); ///fpw/projeto/www/
define('ROBOTS',false);

$start = '_call_start.php';
if(file_exists($start)){
    include $start;
}else{
    die('"call_start" not found');
}
