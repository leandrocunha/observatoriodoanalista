<?php
#################################
//  rotas predefinidas
#################################

$routes = array(
//    'outro-teste' => array(
//        'flow' => MODULE_MAIN,
//        'ctrl' => 'outro',
//        'action' => 'teste',
//        'params' => 'id/1'
//        )
    'cadastre-se' => array(
        'flow' => MODULE_SITE,
        'ctrl' => 'formularios',
        'action' => 'cadastre-se'
        ),
    'contato' => array(
        'flow' => MODULE_SITE,
        'ctrl' => 'formularios',
        'action' => 'contato'
        ),
    'revista' => array(
        'flow' => MODULE_SITE,
        'ctrl' => 'index',
        'action' => 'documentos'
        ),
    'videos' => array(
        'flow' => MODULE_SITE,
        'ctrl' => 'index',
        'action' => 'videos'
        )
    );


/* 
 * ROTAS DINAMICAS 
 * Utilizar a variavel $tmp
 * $tmp[$row->permalink] = array(
            'flow' => MODULE_SITE,
            'ctrl' => $tab,
            'action' => 'index',
            'params' => 'id/' . $row->id
        );
 */
$cache = Cylix_Cache::me();
$cache->type(Cylix_Cache::TYPE_QUERY);
$time = (ENV == 'local') ? 1 : 10*60;
$cache->lifetime($time);
foreach(array('areas','paginas') as $tab){
    $modelo = getModelTableFile($tab);
    if (is_file($modelo)) {
        $name = $tab.'-permalinks';
        if($cache->isValid($name)){
            $rows = $cache->get($name);
        }else{
            //procurando permalinks
            $sql = Cylix_SQL::select('id,permalink')->from($tab)->where("permalink IS NOT NULL AND permalink <> ?", '');
            $rows = Cylix_SQL::exec($sql, $tab);
            $cache->set($name, $rows);
        }
        foreach ($rows as $row) {
            $routes[$row->permalink] = array(
                'flow' => MODULE_SITE,
                'ctrl' => 'index',
                'action' => $tab,
                'params' => 'id/' . $row->id
            );
        }
    }
}