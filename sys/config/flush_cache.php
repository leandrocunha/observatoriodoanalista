<?php

/*
 * impeza de cache
 */
$array = array();
switch ($_GET['flush_cache']){
    case 'GERAL': $array = array('cache','selections','views');
        break;
    case 'CSS': $array = array('sass');
        break;
    case 'LOG': $array = array('log');
        break;
	case 'ALL': $array = array('cache','selections','views','sass','log');
		break;
}
Cylix_Cache::me()->flush($array);