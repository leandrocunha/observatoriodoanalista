<?php
/*
 * Flags acionadas via terminal ou não
 */
//manutenção - maintenance
$f_site = SYS_PATH.'tmp/terminal/maintenance';
if(file_exists($f_site) && FLOW != MODULE_TERMINAL){//soh o terminal pode ser acessível
    //buscado a pagina maintenance.html no www
    $html = INDEX_PATH.'maintenance.html';
    if(file_exists($html)){
        die(@file_get_contents($html));
    }else{
        ob_start();
        ?>
<html>
    <head>
        <title>Em manutenção</title>
    </head>
    <body>
        <h2>Em manutenção</h2>
        <p>Desculpe-nos o transtorno.</p>
    </body>
</html>
        <?php
        die(ob_get_clean());
    }
}