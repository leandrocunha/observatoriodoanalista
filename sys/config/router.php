<?php
$routes = array();
//lista de rotas predefinidas na variavel $routes
include SYS_PATH.'config/router_list.php';

###########
//Pedidos da URI
############
$_URI = explode('?', $_SERVER['REQUEST_URI']); //retirando os GET's
$aux = preg_replace('/index.php.*/', '', $_SERVER['PHP_SELF']);
$_URI = ($aux != '/') ? str_replace($aux, '', $_URI[0]) : $_URI[0]; //request
//testando módulo

while(strpos($_URI, '/') === 0){
    //tem um '/' no inicio
    $_URI = substr($_URI, 1);
}

while(substr($_URI, strlen($_URI)-1) == '/'){
    //tem um '/' no final
    $_URI = substr($_URI, 0, strlen($_URI)-1);
}

//ajuste para index de cada modulo
if ($_URI == '') {
    $_URI = 'index';
}
//eliminando aspas repetidas seguintes
$_URI = preg_replace("/\/\/\/+/",'/',$_URI);

//modulo,chave,controle,ação
$_Link = $_Ctrl = $_Action = '';

$route_found = false; //caso tenha encontrado a rota

#################################
//percorrendo as rotas definidas (em array ou por banco
#################################

function setup_uri(&$_URI,$settings){
    //garantindo os parametros obrigatorios
    if(!isset($settings['flow']) || !isset($settings['ctrl']) || !isset($settings['action'])){
        throw new Cylix_Exception('problema nas rotas definidas (parameters not found)', 'Router Error');
    }
    define('FLOW', $settings['flow']);
    $_URI = $settings['ctrl'].'/'.$settings['action'];
    //tornando os parametros opcional
    if(isset($settings['params'])){
        $_URI .= '/'.$settings['params'];
    }
}
if(isset($routes[$_URI])){
    $_Link = $_URI;
    //por conteúdos
    $founded = $routes[$_URI];
    setup_uri($_URI, $founded);
    $route_found = true;
}else{
    //por estimativa no array
    foreach($routes as $key => $array){
        if(trim($key) && @strpos($_URI, $key) === 0){
            //começa com esta chave, então basta cortá-la e pegar o resto
            $aux = str_replace($key, '', $_URI);
            while(strpos($aux, '/') === 0){
                //tem um '/' no inicio
                $aux = substr($aux, 1);
            }
            setup_uri($_URI, $array);
            //incluindo os demais parametros GET
            if(strlen($aux) > 0){
                $_URI .= '/'.$aux;
            }
            $route_found = true;//aciona o flag para desconsiderar os demais testes
        }
    }
}



#################################
//  definindo o FLOW (fluxo de pasta que ele seguirá)
#################################

if(isset($root_permalinks) && is_array($root_permalinks)){
    if(!$route_found){
        foreach($root_permalinks as $permalink => $flow){
            if(strpos($_URI, $permalink) === 0){
                if(!defined('FLOW')){
                    define('FLOW', $flow);//fluxo de pasta
                }
                //começa com esta chave, então basta cortá-la e pegar o resto
                $_Link = $permalink;
                $aux = str_replace($permalink, '', $_URI);
                while(strpos($aux, '/') === 0){
                    //tem um '/' no inicio 
                    $aux = substr($aux, 1);
                }
                if ($aux == '') {
                    $_URI = 'index';
                }else{
                    $_URI = $aux;
                }
            }
        }
        if(!defined('FLOW')){
            define('FLOW', $root_permalinks['default']);//fluxo de pasta do site
        }
    }
}else{
    throw new Cylix_Exception('root_permalinks not found');
}

#################################
//  montando os parametros GET
//  definindo ctrl e model
#################################

//separando os parametros
$auxP = explode('/', $_URI);
$_Ctrl = 'index';
$_Action = 'index';
//caso tiver mais parâmetros da URI:
$ind = 0;
if (count($auxP) > $ind) {
    if (count($auxP) > $ind + 1) {//procura o controle e a ação
        $_Ctrl = (strlen($auxP[$ind]) > 1) ? $auxP[$ind] : 'index';
        $_Action = (strlen($auxP[$ind + 1]) > 1) ? $auxP[$ind + 1] : 'index';
        //caso tenha mais coisas na uri
        if (count($auxP) > $ind + 1) {
            for ($i = $ind + 2; $i < count($auxP); $i++) {
                $key = $auxP[$i];
                $i++;
                $v = $auxP[$i];
                $_GET[$key] = $v;
            }
        }
    } else {
        $_Ctrl = (strlen($auxP[$ind]) > 1) ? $auxP[$ind] : 'index';
        $_Action = 'index';
    }
}

/*
echo '<br/>flow: '.FLOW;
echo '<br/>permalink: '.$_Link;
echo '<br/>ctrl: '.$_Ctrl;
echo '<br/>acao: '.$_Action;
echo '<br/>get: <br/>';
var_dump($_GET);
die();
/**/
