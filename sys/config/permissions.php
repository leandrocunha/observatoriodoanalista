<?php
/* 
 * pastas basicas e suas permissões
 * 
 */
$folders = array(
    SYS_PATH."tmp",
    SYS_PATH."tmp/views",
    SYS_PATH."tmp/selections",
    SYS_PATH."tmp/sass",
    SYS_PATH."tmp/log",
    SYS_PATH."tmp/terminal",
    INDEX_PATH."uploads"
);

foreach($folders as $dir){
    if(!is_dir($dir)){
        $u = umask(0);
        mkdir($dir, 0777);
        umask($u);
    }
}