<?php
//Incuidas em default
function _listDir($dir = '.', $filtro = 'is_file', $ext = '*', $semelhante = false) {
    $diretorio = dir($dir);
    $r = array();
    while ($conteudo = $diretorio->read()) {
        if ($conteudo != '.' && $conteudo != '..') {
            //todos as extensões ou se é pra procurar algo semelhante
            if ($ext == '*' || $semelhante) {
                $excluir = false;
                if ($semelhante) {//há filtragem para comparar se é realmente semelhante
                    if (strpos($conteudo, $semelhante) !== false) {
                        //achou algo semelhante
                        $excluir = true;
                    }//se não achar é pra pular e não fazer nada
                } else {
                    //pega de qualquer forma
                    $excluir = true;
                }
                if ($excluir) {
                    ///já era, capturou
                    $conteudo_dir = "$dir/$conteudo"; //soh pra poder usar os filtros
                    /* todos */
                    eval("if ($filtro(\$conteudo_dir)){ \$r[] = \$conteudo; }");
                }
            } else {
                $aux = explode('.', $conteudo);
                $aux = $aux[count($aux) - 1]; //ultimo pra ser a extensão
                if ($aux == $ext) {
                    $r[] = $conteudo;
                }
            }
        }
    }
    $diretorio->close();
    return $r;
}

/**
 * Retorna a url absoluta
 * @param string $url_direta
 * @return string
 */
function getUrl($url_direta) {
    $url_direta = trim($url_direta);
    $url_direta = (strpos($url_direta, '/') === 0) ? substr($url_direta, 1) : $url_direta;
    $string = (strpos($url_direta, 'http') === 0) ? $url_direta : ABSOLUTE_URL . $url_direta;
    return $string;
}

/**
 * Retorna a url com '/' no inicio
 * @param string $url_direta
 * @return string
 */
function getBaseUrl($url_direta) {
    $url_direta = trim($url_direta);
    $url_direta = (strpos($url_direta, '/') === 0) ? substr($url_direta, 1) : $url_direta;
    $string = (strpos($url_direta, 'http') === 0) ? $url_direta : BASE_URL . $url_direta;
    return $string;
}

/**
 * Retorna a url com '/#!/' no inicio para ajax crawler
 * @param string $url_direta
 * @return string
 */
function getBaseUrlAjax($url_direta) {
    $url_direta = trim($url_direta);
    $url_direta = (strpos($url_direta, '/') === 0) ? substr($url_direta, 1) : $url_direta;
    $string = (strpos($url_direta, 'http') === 0) ? $url_direta : BASE_URL . '#!/' . $url_direta;
    return $string;
}

/**
 * pega o caminho do tema, mas só deve ser usado nas camadas
 * @param string $url_direta
 * @param boolean $is_base utiliza baseUrl ou não
 * @return string 
 */
function getSkinUrl($url_direta, $is_base = true) {
    /* pega o caminho do skin atual
     * na tabela de temas terá a chave para acessar o caminho do skin
     * ex: tema Natal ->
     * chave    |   Titulo  |   ini    |   fim
     * natal    |   Natal   | 01/12/11 | 29/12/11
     *
     * resultando eno caminho dirname(__FILE__).'/temas/natal/'
     */
    $string = trim($url_direta);
    $string = (strpos($string, '/') === 0) ? substr($string, 1) : $string;
    $string = 'themes/' . SKIN . '/' . trim($string);
    return ($is_base) ? getBaseUrl($string) : getUrl($string);
}

/**
 * redireciona com Header PHP
 * @param string $redirect
 */
function redirect($redirect) {
    if (substr($redirect, 0, 3) == 'www') {
        $redirect = "http://$redirect";
    } elseif (substr($redirect, 0, 4) != 'http') {
        $redirect = getUrl($redirect);
    }
    header("Location: $redirect");
}

/**
 * Redireciona com javascript
 * @param string $url
 */
function setUrl($url) {
    if (substr($redirect, 0, 3) == 'www') {
        $redirect = "http://$redirect";
    } elseif (substr($redirect, 0, 4) != 'http') {
        $redirect = getUrl($redirect);
    }
    echo '<script type="text/javascript">window.location="' . $redirect . '";</script>';
}

function echoScript($url, $tipo = 'js', $cache = true, $media = 'screen') {
    if ($cache && (strpos($url, '/') === 0)) {
        $file = $_SERVER['DOCUMENT_ROOT'] . $url;
        $file = str_replace('//', '/', $file);
        if (file_exists($file)) {
            $url .= '?' . filemtime($file);
        }
    }
    if ($tipo == 'js') {
        ?>
        <script type="text/javascript" src="<?php echo $url; ?>"></script>
        <?php
    } else {
        ?>
        <link rel="stylesheet" href="<?php echo $url; ?>" type="text/css" media="<?php echo $media ?>" />
        <?php
    }
}

/**
 * normaliza em CamelCase ex: teste-meu >> TesteMeu
 * @param string $str
 * @param boolean $lc_first 1ª letra será minuscula
 * @param boolean $undescore incluir '_' no split
 * @return string
 */
function camelcase($str, $lc_first = false,$undescore=true) {
    if($undescore){
		$aux = split("[-_]", $str);
	}else{
		$aux = explode("-", $str);
	}
    $aux2 = array();
    foreach ($aux as $v) {
        $aux2[] = ucfirst($v);
    }
    $str = implode('', $aux2);
    $str = ($lc_first) ? lcfirst($str) : $str;
    return $str;
}

function vardump($v) {
    echo '<pre>';
    var_dump($v);
    echo '</pre>';
    die;
}

function getModelTableFile($name, $is_row = false) {
    return ($is_row) ? SYS_PATH . 'app_layers/models/row/M_' . camelcase($name,false,false) . '_Row.php' : SYS_PATH . 'app_layers/models/table/M_' . camelcase($name,false,false) . '.php';
}

function getModelTableName($name, $is_row = false) {
    return ($is_row) ? camelcase($name,false,false) . 'Model_Row' : camelcase($name,false,false) . 'Model';
}

function getCtrlFile($name) {
    return SYS_PATH . 'app_layers/controllers/' . FLOW . '/C_' . camelcase($name,false,false) . '.php';
}

function getCtrlName($name) {
    return camelcase(FLOW) . '_' . camelcase($name,false,false) . 'Controller';
}

function noQuotes($valor) {
    return str_replace(array("'"), array("\'"), $valor);
}

/**
 * retorna o 1º do array de Rows
 * @param array $array
 * @return Cylix_Model_Row 
 */
function first(&$array) {

    if (is_array($array)) {
        $array = $array[0];
        return $array;
    } else {
        return $array;
    }
}

/**
 * retorna o ultimo do array de Rows
 * @param array $array
 * @return Cylix_Model_Row 
 */
function last(&$array) {
    if (!is_array($array))
        return $array;
    if (!count($array))
        return null;
    end($array);
    return $array[key($array)];
}

function moneyToSQL($array = array(), &$post = array()) {
    foreach ($array as $cada) {
        $post[$cada] = trim(str_replace(array('.', ',', 'R$'), array('', '.', ''), $post[$cada]));
    }
}

function floatToSQL($array = array(), &$post = array()){
    moneyToSQL($array, $post);
}

/**
 * parse date in sql format
 * @param array $array keys from $post
 * @param mixed $post array/keys or the string to parse (array de chave/valor ou valor direto)
 * @return mixed $post
 */
function dateToSQL($array = array(), &$post = array()) {
    if (!is_array($array)) {
        $array = array($array);
    }
    foreach ($array as $cada) {
        $valor = (is_array($post)) ? trim($post[$cada]) : trim($post);
        if (strlen($valor) > 0) {
            $aux_split = explode(' ', $valor);
            $aux = $aux_split[0];
            if (strlen($aux) == 10) {
                $aux = explode('/', $aux);
                if (count($aux) == 3) {
                    $res = $aux[2] . '-' . $aux[1] . '-' . $aux[0];
                    $res = count($aux_split)==2 ? "$res ".$aux_split[1] : $res;
                    if (is_array($post)) {
                        $post[$cada] = $res;
                    } else {
                        $post = $res;
                    }
                }
            }
        }
    }
    return $post;
}

function escapeHttp($url) {
    return preg_replace('/^(s?f|ht)tps?:\/\/[^\/]+/i', '', (string) $url);
}

/**
 * retorna o que estava scaped à sua forma crua
 * @param string $string
 * @param int $quotes tratamento de aspas ENT_COMPAT,ENT_QUOTES,ENT_NOQUOTES
 * @return string 
 */
function raw($string = '', $quotes = ENT_QUOTES) {
    //ENT_COMPAT,ENT_QUOTES,ENT_NOQUOTES
    return str_replace(array('&apos;'),array("'"),html_entity_decode((string) $string, $quotes, 'utf-8'));
}

function now($datetime=true){
	return ($datetime) ? date('Y-m-d H:i:s') : date('Y-m-d');
}

function transPermalink($str) {
    $str = trim($str);
    $troca = array(
        'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
        'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
        'C' => '/&Ccedil;/',
        'c' => '/&ccedil;/',
        'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
        'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
        'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
        'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
        'N' => '/&Ntilde;/',
        'n' => '/&ntilde;/',
        'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
        'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
        'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
        'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
        'Y' => '/&Yacute;/',
        'y' => '/&yacute;|&yuml;/',
        'a.' => '/&ordf;/',
        'o.' => '/&ordm;/',
        '' => '/[\!\?]/',
        '-' => '/[\,\.]/'
    );
    $enc = 'UTF-8';
    $permalink = preg_replace($troca, array_keys($troca), htmlentities($str, ENT_NOQUOTES, $enc));
    $permalink = strtolower($permalink);
    $outros = array(
        "\\" => '-',
        "/" => '-',
        "(" => '',
        ")" => '',
        '%' => '',
        '@' => '',
        ':' => '',
        ';' => '',
        ' ' => '-',
        '$' => 's',
        '#' => '',
        '*' => '.',
        '§' => '',
        '>' => '',
        '<' => '',
        '|' => '_'
    );
    $permalink = str_replace(array_keys($outros), $outros, $permalink);
    return $permalink;
}

/**
 * retorna um password aleatorio
 * @param int $length tamanho
 * @param int $type 0-tudo | 1-somente numeros | 2-somente minusculas | string-caracteres definidos
 * @param boolean $special terá caracteres especiais
 * @return type 
 */
function randomPassword($length = 7, $type = 0, $special = false) {
    // add numbers,alphabets and special chars

    switch ((string) $type) {
        case '0' : $random_chars = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            break;
        case '1' : $random_chars = "1234567890";
            break;
        case '2' : $random_chars = "abcdefghijklmnopqrstuvwxyz";
            break;
        default : $random_chars = strlen($type > 2) ? $type : '???';
    }

    if ($special && $type != 1) {
        $random_chars .= "!@#$%*()%";
    }
    $getstring = $password = "";
    // While loop will execute until the length of password reaches the $passwordlength
    $count = 0;
    $max_count = 100;
    while (strlen($password) < $length) {
        $count++;
        if ($count == $max_count) {
            die('randomPassword - loop infinito');
        }
        // returns a sigle character from the set of random_chars
        $getstring = substr($random_chars, mt_rand(0, strlen($random_chars) - 1), 1);
        //Avoid already existed character from the password.
        if (!strstr($password, $getstring)) {
            //append the generated value to password 
            $password .= $getstring;
        }
    }
    return($password);
}

function getYouTubeIdFromURL($url)
{
  $url_string = parse_url($url, PHP_URL_QUERY);
  parse_str($url_string, $args);
  return isset($args['v']) ? $args['v'] : false;
}
function getVimeoIdFromURL($url)
{
  preg_match('/(\d+)/', $url, $matches);
  return $matches[0];
}

//adaptações
if (false === function_exists('lcfirst')){
    /**
     * (PHP 5 >= 5.3.0) <br>Make a string's first character lowercase
     * @param string $str
     * @return string 
     */
    function lcfirst($str) {
        return (string) (strtolower(substr($str, 0, 1)) . substr($str, 1));
    }
}
/**
 * Content-Type test for images
 * @param string $mime_type Content-Type
 * @return boolean 
 */
function is_image($c_type=''){
    return strpos($c_type, 'image') !== false;
}

function cropText($string,$limit=255,$end='...'){
	$string = trim($string);
	return (strlen($string)>$limit) ? substr($string, 0, $limit).$end : $string;
}