<?php
$usar = false;
//somente pro flow site
if(FLOW == MODULE_MAIN && $usar){
    $today = date('Ymd');//somente para fins de comparação mais facilitada
    //temas estáticos
    $skins = array(
    //    'ferias' => array(
    //        'start' => '2012-01-09',
    //        'finish' => '2012-01-29'
    //    )
    );

    //temas dinamicos
    $tab = 'temas';//tabela
    $m_themes = getModelTableFile($tab);

    if(is_file($m_themes)){
        $sql = Cylix_SQL::select()->from($tab)->orderBy('inicio DESC');
        $rows = Cylix_SQL::exec($sql,$tab);
        foreach($rows as $row){
            $skins[$row->skin] = array(
                'start' => $row->inicio,
                'finish' => $row->fim
            );
        }
    }

    //achando o skin de acordo com a data atual
    foreach($skins as $skin => $p){
        $S = str_replace('-', '', $p['start']);
        $F = str_replace('-', '', $p['finish']);
        if(strlen($S) > 5){
            if($today >= $S){
                if(strlen($F) > 5){
                    if($today <= $F){
                        define('SKIN', $skin);
                    }
                }else{
                    define('SKIN', $skin);
                }
            }
        }
    }
}
//deafult
if(!defined('SKIN')){
    define('SKIN', FLOW);
}