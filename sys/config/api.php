<?php
/*
 * API bootstrap
 * caso algum arquivo public queira usar os modelos e o restante do framework
 */
session_start();
$umask = umask(0);
include_once SYS_PATH . 'config/main_exception.php';
try{
    
    //constantes e o autoloader
    include_once SYS_PATH . 'config/main_settings.php';
    
    //funcoes basicas
    include_once SYS_PATH . 'config/functions.php';
    
    //no php-cache for AJAX
    if(IS_AJAX){
        Cylix_Cache::noHeaderCache();
    }
    
} catch (Cylix_Exception $e) {
    die(Cylix_Exception::msg500($e));
}
umask($umask);
