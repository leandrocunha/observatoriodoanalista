<?php
//numero serial para eventuais salts
define('SERIAL', 'S2013-AM2104');
/*
 * constantes definidas no index: ABSOLUTE_URL, INDEX_PATH, SYS_PATH, BASE_URL, FRAMEWORK_PATH, IS_AJAX e ENV (local/online)
 */
define('ERRORS_EMAIL', 'erros@ferramentasparaweb.com.br');

//flag para otimização quando for ajax
$is_ajax = (isset($_GET['layout']) && $_GET['layout'] == 'ajax') || (isset($_GET['ajax']) && $_GET['ajax'] == '1');
define('IS_AJAX', $is_ajax);

//modulos + permalinks
$root_permalinks=array();//variavel usada pelo router.php
include SYS_PATH.'config/modules.php';

//timezone
date_default_timezone_set('America/Sao_Paulo');

//tratamento de erros
include SYS_PATH.'config/errors.php';

//autoloader
require_once(FRAMEWORK_PATH . 'AutoLoader.php');
spl_autoload_register( array('Cylix_AutoLoader', 'includeClass') );

//mailer

if(ENV != 'local'){
    Cylix_Mailer::$logFolder = SYS_PATH.'tmp/log';//se habilitar começa a gerar, senao deixa quieto
	Cylix_Mailer::$StaticSendThrough = Cylix_Mailer::SEND_POSTFIX;//mail() do linux
	Cylix_Mailer::$paramPostfix = 'sistema@analistamais.com.br';
	Cylix_Mailer::$defaultReturnPath = array('sistema@analistamais.com.br','Sistema');
}else{
	Cylix_Mailer::$logFolder = SYS_PATH.'tmp/log';//se habilitar começa a gerar, senao deixa quieto
    Cylix_Mailer::$StaticSendThrough = Cylix_Mailer::SEND_LOCAL_NULL;
}

//captcha
Cylix_Form::$captchaFilePath = INDEX_PATH."uploads/configs";