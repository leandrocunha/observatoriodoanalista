<?php
/**
 * SASS Parser - 2012
 * 
 * projeto original:
 * http://code.google.com/p/phamlp/
 */

$lib = SYS_PATH . "lib/PHPCSSParser/sass";
if(is_dir($lib)){
    include "$lib/SassParser.php";
    ##############
    # montando o parser de acordo com o tipo de ENV
    ##############
    $cfg_compass = array(
                    'images_path' => '../img',
                    'project_path'=>INDEX_PATH.'themes/' . SKIN . '/img',
                    'real_path'=>INDEX_PATH.'themes/' . SKIN . '/img'/*'images_path' => INDEX_PATH.'themes/' . SKIN . '/css/images',*/
                    );
    if(ENV == 'online'){
        $sass_parser_config = array(
            'style' => 'compressed', #compressed, expanded ou nested
            'cache' => true,
            'cache_location' => SYS_PATH.'tmp/sass',
            'debug_info' => false, //soh funciona com expanded
            'vendor_properties' => true,
            'extensions' => array('compass' => $cfg_compass)
        );
    }else{
        $sass_parser_config = array(
            'style' => 'nested', 
            'cache' => false,
            'cache_location' => SYS_PATH.'tmp/sass',
            'debug_info' => true, 
            'vendor_properties' => true,
            'extensions' => array(
                'compass' => $cfg_compass
                )
        );
    }
    
    #######
    # pastas baseadas nos FLOWs
    #######
    $folders = array();
    if(isset($root_permalinks)){
        foreach($root_permalinks as $permalink => $module){
            if($module == FLOW):
                //passando cada permalink pra pegar o module/flow
                $folders[] = array(
                    'sass' => SYS_PATH.'/stylesheets/sass/'.$module,
                    'css' => INDEX_PATH."themes/$module/css"
                );
            endif;//otimizando para obter somente os flow q estarei visitando
        }
    }else{
        throw new Cylix_Exception('root_permalinks not found');
    }
    
    //percorrendo as pastas
    $scss_modif = array();
    foreach ($folders as $folder) {
        if (is_dir($folder['sass']) && $handle = @opendir($folder['sass'])):
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != ".." && strpos($file,'_') !== 0) {
                    $lm_scss = filemtime($folder['sass']."/$file");//last modific do scss
                    $file_name = str_replace(array(SassFile::SASS, SassFile::SASSC, SassFile::SCSS), '', $file);
                    $new_file = $folder['css'] . "/$file_name" . 'css';
                    $lm_css = @filemtime($new_file);//last modific do CSS
                    if($lm_scss > $lm_css){
                        //soh modifica se o scss foi alterado
                        $scss_modif[] = array('scss2css' => $folder['sass']."/$file",'newcss'=>$new_file);
                    }
                }
            }
            @closedir($handle);
        endif;
    }
    
    //parser somente dos arquivos que não têm _
    if(count($scss_modif)){
        $sass = new SassParser($sass_parser_config);
        foreach($scss_modif as $F){
            $scss_to_css = $F['scss2css'];
            $new_file = $F['newcss'];
            if ($css = $sass->toCss($scss_to_css)) {
                $fp = @fopen($new_file, 'w');
                @fwrite($fp, $css);
                @fclose($fp);
            }
        }
    }
}else{
    throw new Cylix_Exception($lib.' -> not found.','SassParser');
}