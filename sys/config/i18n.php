<?php
if(FLOW == MODULE_MAIN){
    /*
     * mecanismos de tradução para o flow site
     */
    if(!isset($_SESSION['locale'])){
        $_SESSION['locale'] = 'pt-BR';
    }
    //Cylix_I18n::getCSV($_SESSION['locale'],SYS_PATH.'etc/locale');
}
//funcao com/sem case sensitive (um alias)
/**
 * alias do Cylix_I18n::t()
 * @param string $string frase
 * @param boolean $cs (case-sensitive)
 * @return string 
 */
function t($string){
    return Cylix_I18n::t($string);
}
?>
