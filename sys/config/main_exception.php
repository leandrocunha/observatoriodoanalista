<?php
//classe principal
$exc_class = FRAMEWORK_PATH . 'Exception.php';
if(file_exists($exc_class)){
    require_once $exc_class;
    set_exception_handler(array('Cylix_Exception','show'));
}else{
    die('Exception class not found!');
}