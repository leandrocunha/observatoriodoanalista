<?php

$modules = array('site','terminal','admin');
foreach($modules as $dir){
    define('MODULE_'. strtoupper($dir), $dir);
}

define('PERMALINK_TERMINAL', 'terminal');
define('PERMALINK_ADMIN', 'cms');

//associação de permalinks com seus respectivos diretórios nos módulos
$root_permalinks = array(
    'default' => MODULE_SITE,
    PERMALINK_TERMINAL => MODULE_TERMINAL,
    PERMALINK_ADMIN => MODULE_ADMIN
);
