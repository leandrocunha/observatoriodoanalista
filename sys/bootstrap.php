<?php
session_start();
$umask = umask(0);
include_once SYS_PATH . 'config/main_exception.php';
try{
    
    //constantes e o autoloader
    include_once SYS_PATH . 'config/main_settings.php';
    
    //funcoes basicas
    include_once SYS_PATH . 'config/functions.php';
    
    ################################
    #  flush cache via GET
    ################################
    include_once SYS_PATH . 'config/flush_cache.php';
    
    //no php-cache for AJAX
    if(IS_AJAX){
        Cylix_Cache::noHeaderCache();
    }
    
    ################################
    #  setando as permissoes
    ################################
    //pastas e permissoes de escrita
    if(!IS_AJAX){//otimização para ajax
        include_once SYS_PATH . 'config/permissions.php';
    }
    
    ################################
    #  roteador (determinar o modulo, controle, acao e parametro get
    ################################
    
    include_once SYS_PATH . 'config/router.php';#critico2
    //jah foi guardado as variaveis $_GET, $_Link (permalink atual), $_Ctrl e $_Action
    //definido o FLOW (pasta de referencia != permalink)
    
    ################################
    #  I18n - mecanismos de tradução inline via csv
    ################################
    
    include_once SYS_PATH . 'config/i18n.php';
    
    ################################
    #  flags
    ################################
    
    //principais flags
    include_once SYS_PATH . 'config/flags.php';
    
    
    ##################################
    #  Temas
    ##################################
    
    //chamada do tema em www(admin,site,natal,etc.)
    include_once SYS_PATH . 'config/themes.php';
    if(!IS_AJAX){//otimização para ajax
        ##################################
        #  SASS
        ##################################

        //chamada da lib SASS_PHP
        include_once SYS_PATH . 'config/sass.php';#ponto critico1
    }
    
    ##################################
    #  Helpers
    ##################################
    $_Helper = new Cylix_Helper(SYS_PATH.'helpers');
    
    ##################################
    #  App
    ##################################
    $_Layout = (isset($_GET['layout'])) ? $_GET['layout'] : 'html';
    Cylix_App::run($_Ctrl,$_Action,$_Helper,$_Layout,$_Link);
    
} catch (Cylix_Exception $e) {
    echo Cylix_Exception::msg500($e);
}
umask($umask);
