<?php

class Cylix_App {

    static $_script;
    static $_css;
    static $_dir_tmp_terminal = 'tmp/terminal';
    static $_dir_maintenance = '/maintenance';

    static function setJS($chave, $url, $cached=true) {
        if (@is_file($url) && $cached) {
            $url .= '?' . filemtime($url);
        }
        self::$_script[$chave] = $url;
    }

    static function setCSS($chave, $url, $media='all', $cached=true, $rel='stylesheet') {
        if (@is_file($url) && $cached) {
            $url .= '?' . filemtime($url);
        }
        self::$_css[$chave] = array($url, $media, $rel);
    }
    
    static function getJavascripts(){
        if(isset(self::$_script)):
            foreach (self::$_script as $key => $valor) {
                echo '<script type="text/javascript" src="' . $valor . '"></script>
';
            }
        endif;
    }

    static function getStylesheets(){
        if(isset(self::$_css)):
            foreach (self::$_css as $key => $valor) {
                echo '<link rel="' . $valor[2] . '" media="' . $valor[1] . '" href="' . $valor[0] . '" />
';
            }
        endif;
    }

    static function getScripts() {
        self::getStylesheets();
        self::getJavascripts();
        
    }

    /**
     * Acontece tudo
     */
    static function run($_ctrl,$_action,$_helper=null,$_layout='html',$_permalink='') {
        try{
            
            //puxa o abstract dos controles, caso tenha
            $abstract = getCtrlFile('abstract');
            
            $arqCtrl = getCtrlFile($_ctrl);
            $CtrlReal = self::camelcase(FLOW).'_'.self::camelcase($_ctrl.'-controller');
            //controle requerido
            if (@file_exists($arqCtrl)) {
                //construindo o controller, setando a view, format e helpers
                eval("\$CTRL = new $CtrlReal('".$_ctrl."', '".$_action."', '".$_layout."',\$_helper,'".$_permalink."');");
                //testando se a action existe
                $action = self::camelcase("action-$_action", true);
                if(method_exists($CTRL, $action)){
                    //init
                    $e = $CTRL->beforeAction();
                    if($e instanceof Cylix_Exception){
                        throw $e;
                    }
                    //chamando a action
                    eval("\$e = \$CTRL->{$action}();");
                    if($e instanceof Cylix_Exception){
                        throw $e;
                    }
                    //end
                    $e = $CTRL->afterAction();
                    if($e instanceof Cylix_Exception){
                        throw $e;
                    }
                    //renderizando
                    if($CTRL->view->show()){
                        //achando o yield
                        if(!isset($CTRL->view->yield)){
                            //pegando o yield, caso nao tenha mexido nele ainda
                            $r = $CTRL->view->setYield();
                            if($r instanceof Cylix_Exception){
                                //se retornou uma excessao
                                throw $r;
                            }
                        }
                        //puxando o layout
                        $r = $CTRL->view->getLayout();
                        if($r instanceof Cylix_Exception){
                            //se retornou uma excessao
                            throw $r;
                        }else{
                            echo $r;
                        }
                    }
                }else{
                    throw new Cylix_Exception("Action: $action \nCtrl: $CtrlReal", "Action not found: $action");
                }
            } else {
                throw new Cylix_Exception($arqCtrl, "Controller not found: $CtrlReal");
            }
        } catch (Cylix_Exception $e) {
            ob_end_clean();
            die(Cylix_Exception::msg404($e));
        }
    }

    /**
     * normaliza em CamelCase ex: teste-meu >> TesteMeu
     * @param string $str
     * @param boolean $lc_first 1ª letra será minuscula
     * @return string
     */
    static function camelcase($str, $lc_first=false) {
        $aux = preg_split("/[-_]/", $str);
        $aux2 = array();
        foreach ($aux as $v) {
            $aux2[] = ucfirst($v);
        }
        $str = implode('', $aux2);
        $str = ($lc_first) ? lcfirst($str) : $str;
        return $str;
    }
    
    /**
     * Cria a flag de manutenção
     * @return boolean 
     */
    static function lockSite(){
        $path = SYS_PATH.self::$_dir_tmp_terminal;
        $f_site = $path.self::$_dir_maintenance;
        $r = false;
        if(@is_dir($path)){
            if(!@file_exists($f_site)){
                $u = umask(0);
                $fh = fopen($f_site, 'w') or die("impossível obter $f_site");
                @fwrite($fh, '1');
                fclose($fh);
                umask($u);
                $r = true;
            }
        }
        return $r;
    }

}

?>
