<?php

/**
 * Description of H_CrudFile
 * manipular arquivos nos CRUDs
 * $this->helper->crudFile()->model()->save()
 *
 * @author leandro.fpw@gmail.com
 */
class CrudFileHelper {
    
    private $_data;
    private $_post;
    private $_m_table;
    private $_m_row;
    
    public function __construct($data=null) {
        //set name - name no formulario
        $this->setData($data);
    }
    function setData($data){
        $this->_data = $data;
    }
    function setModels($model_table,$model_row){
        $this->_m_table = $model_table;
        $this->_m_row = $model_row;
    }
    
    function setPost($post){
        $this->_post = $post;
    }
    
    function getPost(){
        return $this->_post;
    }
    
    function hasUnlink($aux=null){
        $m = $this->_m_table;
        $row = $this->_m_row;
        $name = $this->_data;
        $aux = ($aux) ? $aux : $name.'_remove';
        $post = $this->_post;
        if ($post[$aux] == 1) {
            //remove
            $m->unlinkFiles($row->{$name}); //apaga as fotos
            //@unlink($reg->imagem);
            $post[$name] = '';
        }
        $this->_post = $post;//caso altere
    }
    
    function save($prop=1024, $nome='', $qualidade=80, $pasta=null){
        $m = $this->_m_table;
        $row = $this->_m_row;
        $name = $this->_data;
        $post = $this->_post;
        if ($post[$name]['size'] > 0) {
            //salvando a imagem
            $arquivo = new Cylix_Files($post[$name]);
            $arquivo->setFolder($m->getFilePath());
            $nome = uniqid();
            $arquivo->setName($nome);
            if (is_image($arquivo->getType())) {
                //resize
                $n = $arquivo->resizeMe($prop, $nome, $qualidade, $pasta);
                //jah enviada
                if ($n != 'erro') {
                    //upload tranquilo, pega o nome para salver
                    $post[$name] = $n;
                    //remove a velha
                    if (strlen($row->{$name}) > 0) {
                        $m->unlinkFiles($row->{$name}); //apaga as fotos
                    }
                } else {
                    return new Cylix_Exception("Não foi possível salvar o arquivo " . $arquivo->getOriginalName(), 'Falha em upload');
                }
            }
        } else {
            $post[$name] = $row->{$name};
        }
        $this->_post = $post;//caso altere
        return $this->_post;
    }
    
}

?>
