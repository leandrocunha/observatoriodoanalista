<?php

/**
 * coisas pra email
 *
 * @author Leandro
 */
class MailerHelper {
    
    private $_data;
    
    public function __construct($data) {
        $this->_data = $data;
    }
    static function createMailer($view,$post,$to,$titulo=null,$from_email=null,$from_nome=null,$msg_key=null,$tpl=null,$action='index'){
        $titulo = $titulo ? $titulo : 'Contato de '.$_SERVER['SERVER_NAME'];
        if($msg_key){
            $post[$msg_key] = nl2br($post[$msg_key]);
        }
        $tpl = ($tpl) ? $tpl : $action;
        $body = $view->getTemplate($tpl.'.html', array_merge($post, array('titulo'=>$titulo,'data'=>date('d/m/Y H:i:s'))));
        $email = new Cylix_Mailer($titulo, $body);
        $ret = false;
        if(strlen($to)){
            $email->to($to);
            if($from_email){
                $email->from($from_email, $from_nome);
            }
            
            if($email->send()){
                $ret=true;
            }
        }
        return $ret;
    }
}

?>
