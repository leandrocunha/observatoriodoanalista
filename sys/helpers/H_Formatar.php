<?php

/**
 * Description of H_Formatar
 *
 * @author Leandro
 */
class FormatarHelper {
    
    private $_data;
    
    public function __construct($data=null) {
        $this->_data = $data;
    }
	function set($data) {
        $this->_data = $data;
	}
    public function moeda($prefixoMoeda='R$ ') {
        $float = $this->_data;
        $n = number_format($float, 2, ',', '.');
        $n = str_replace(',', '<span class="decimal">,', $n);
        return '<span class="prefixo">' . $prefixoMoeda . "</span>" . $n . "</span>";
    }

    public function dataHora() {
        $value = $this->_data;
        if (strpos($value, '/') === false) {
            //caso nao esteja com barra
            $value = strtotime($value);
            if (!$value) {
                return null;
            } else {
                $value = date('d/m/Y H:i:s', $value);
            }
        }
        return $value;
    }

    public function data() {
        $value = $this->_data;
        if (!$value)
            return null;
        
        $return = $this->dataHora($value);
        $arr = explode(' ', $return);
        return $arr[0];
    }
    public function hora() {
        $value = $this->_data;
        if (!$value)
            return null;

        $return = $this->dataHora($value);
        $arr = explode(' ', $return);
        return (isset($arr[1]))?$arr[1]:'00:00:00';
    }

    public function getMes() {
        $value = $this->_data;
        $r = '';
        if(strlen($data) >= 10){
            $partes = explode(' ', $data);
            $data = (count($partes>1))?$partes[0]:$partes;
            $aux = explode('-', $data);
            $mes = $aux[1];
            
        }
        return $r;
    }
	
	function getMesNome($digits='00'){
		$array_meses_c = array(
							'01'=>'Janeiro',
							'02'=>'Fevereiro',
							'03'=>'Março',
							'04'=>'Abril',
							'05'=>'Maio',
							'06'=>'Junho',
							'07'=>'Julho',
							'08'=>'Agosto',
							'09'=>'Setembro',
							'10'=>'Outubro',
							'11'=>'Novembro',
							'12'=>'Dezembro'
							);
		try{
			$r = $array_meses_c[$digits];
			return $r;
		}  catch (Exception $e){

		}
	}

    static function substring($str,$tamanho=150,$padrao='...'){
        $str = strip_tags($str);
        return (strlen($str)>$tamanho) ? substr($str,0,$tamanho).$padrao : $str;
    }
}

?>
