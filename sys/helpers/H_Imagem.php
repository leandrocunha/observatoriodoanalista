<?php

/**
 * H_Imagem -> helper para resize de imagens sob demanda
 * 
 * @author Leandro
 */
class ImagemHelper {

    /**
     * Cylix_Resizer
     * @var Cylix_Resizer 
     */
    private $_resizer;
    private $_img;
    private $_name;
	
	function setFile($path){
		$this->_img = $path;
        if (file_exists($path)) {
            $this->_resizer = new Cylix_Resizer($path);
        } else {
            $this->_resizer = false;
        }
	}

    public function __construct($path=null) {
		$this->setFile($path);
    }
	

    /**
     * seta o nome e retorna o path;
     * @param int $w
     * @param int $h
     * @return string path sem o nome do arquivo 
     */
    private function setName($w, $h) {
        $path = explode('/', $this->_img);
        $this->_name = array_pop($path);
        $path = implode('/', $path);
        return $path;
    }

    //Resize image (options: exact, portrait, landscape, auto, crop)
    public function resize($w, $h = null, $q = '80') {
        $tipo = 'a'; //auto
        $h = ($h == null) ? $w : $h;
        $path = $this->setName($w, $h);
        $name = $this->_name;
        $new_name = $w . 'x' . $h . $tipo . '_' . $name; //800x600a_minhaimagem.jpg
        $new_file = "$path/$new_name";
        if (!file_exists($new_file)) {
            //hora de criar
            if ($this->_resizer) {
                $this->_resizer->resizeImage($w, $h, 'auto');
                $this->_resizer->saveImage($new_file, $q);
            }
        }
        return $new_file;
    }

    /**
     * crop
     * @param int $w
     * @param int $h
     * @param boolean $crop_top false: vertical-meio / true: vertical-topo
     * @param type $q
     * @return type 
     */
    public function crop($w, $h = null, $crop_top = false, $q = '80') {
        $tipo = ($crop_top) ? 'ct' : 'c'; //crop
        $h = ($h == null) ? $w : $h;
        $path = $this->setName($w, $h);
        $name = $this->_name;
        $new_name = $w . 'x' . $h . $tipo . '_' . $name; //800x600c_minhaimagem.jpg
        $new_file = "$path/$new_name";
        if (!file_exists($new_file)) {
            //hora de criar
            if ($this->_resizer) {
                $this->_resizer->resizeImage($w, $h, 'crop', $crop_top);
                $this->_resizer->saveImage($new_file, $q);
            }
        }
        return $new_file;
    }

    public function landscape($w, $h = null, $q = '80') {
        $tipo = 'l'; //landscape
        $h = ($h == null) ? $w : $h;
        $path = $this->setName($w, $h);
        $name = $this->_name;
        $new_name = $w . 'x' . $h . $tipo . '_' . $name; //800x600l_minhaimagem.jpg
        $new_file = "$path/$new_name";
        if (!file_exists($new_file)) {
            //hora de criar
            if ($this->_resizer) {
                $this->_resizer->resizeImage($w, $h, 'landscape');
                $this->_resizer->saveImage($new_file, $q);
            }
        }
        return $new_file;
    }

    public function portrait($w, $h = null, $q = '80') {
        $tipo = 'p'; //auto
        $h = ($h == null) ? $w : $h;
        $path = $this->setName($w, $h);
        $name = $this->_name;
        $new_name = $w . 'x' . $h . $tipo . '_' . $name; //800x600p_minhaimagem.jpg
        $new_file = "$path/$new_name";
        if (!file_exists($new_file)) {
            //hora de criar
            if ($this->_resizer) {
                $this->_resizer->resizeImage($w, $h, 'portrait');
                $this->_resizer->saveImage($new_file, $q);
            }
        }
        return $new_file;
    }

    public function exact($w, $h = null, $q = '80') {
        $tipo = 'e'; //auto
        $h = ($h == null) ? $w : $h;
        $path = $this->setName($w, $h);
        $name = $this->_name;
        $new_name = $w . 'x' . $h . $tipo . '_' . $name; //800x600e_minhaimagem.jpg
        $new_file = "$path/$new_name";
        if (!file_exists($new_file)) {
            //hora de criar
            if ($this->_resizer) {
                $this->_resizer->resizeImage($w, $h, 'exact');
                $this->_resizer->saveImage($new_file, $q);
            }
        }
        return $new_file;
    }

}

?>
