<?php

/**
 * Description of H_Formatar
 *
 * @author Leandro
 */
class HtmlHelper {
    
    private $_data;
    
    public function __construct($data=null) {
        $this->setData($data);
    }
    function semRegistro($txt="Nenhum registro",$cols=99,$rows=null){
        $rows = ($rows) ? ' rowspan="'.$rows.'"' : null;
        if(!count($this->_data)):
        ?><tr>
            <td class="linha-sem-registro" colspan="<?php echo $cols;?>"<?php echo $rows;?>><?php echo $txt;?></td>
        </tr><?php endif;
    }
    function setData($data){
        $this->_data = $data;
    }
}

?>
