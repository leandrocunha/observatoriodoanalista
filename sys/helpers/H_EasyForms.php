<?php
class EasyFormsHelper {
    private $_data;
    
    /*
     * 'name1' => array('label' => 'Título','type' => 'text','large' => true),
       'name2' => array('label' => 'Status','type' => 'select','option' => array('0'=>'Ativo','1'=>'Inativo')),
       'name3' => array('label' => 'CPF','type' => 'mask','option' => '999.999.999-99'),
       'name4' => array('label' => 'Publicado em','type'=>'date', 'value'=>'00/00/0000','blank'=>'right')
     */
    public function __construct($data) {
        $this->_data = $data;
    }
    
    /**
     * constroe formulario get para filtros
     * @param type $action
     * @param type $id
     * @return type 
     */
    function filterGet($action='',$id=null,$persist=true){
        $config = $this->_data;
        $id = ($id!=null) ? $id : 'form-filter-'.FLOW;
        $form = new Cylix_Form($action, Cylix_Form::METHOD_GET, $id);
        ob_start();
        ?>
        <?php echo $form->ini(); ?>
            <table width="100%">
                <?php
                $i=0;
                foreach($config as $chave => $linha):
                    //contador de TR's
                    $i = ($i >= 2) ? 1 : $i+1;
                    //se vai ser um campo com colspan 3
                    $flag_largo = false;
                    if(isset($linha['large']) && $linha['large'] == true){
                        $i++;
                        $flag_largo = true;
                    }
                    //valor
                    if(isset($linha['value'])){
                        $valor = $linha['value'];
                    }else{
                        $valor = $_GET[$chave];
                    }
                    ?>
                <?php if($i == 1):?><tr><?php endif;?>
                    <?php if(isset($linha['blank']) && $linha['blank'] == 'left'){ ?>
                    <td colspan="2">&nbsp;</td>
                    <?php $i++;} ?>
                    <td class="label" width="20%"><?php echo $linha['label'];?></td>
                    <td class="campo field" <?php echo ($flag_largo) ? 'colspan="3" width="80%"' : 'width="30%"'?>>
                        <?php
                        switch($linha['type']){
                            case 'select':
                                $form->select($chave, $valor, $linha['option'], false, true);
                                break;
                            case 'mask': 
                                $form->mask($chave, $valor, $linha['option'], false);
                                break;
                            case 'date':
                                $form->date($chave, $valor, false,false,array('style'=>'max-width: 100px;'));
                                break;
                            default :
                                $form->string($chave, $valor, false);
                                break;
                        }
                        echo $form->get($chave);?>
                    </td>
                    <?php if(isset($linha['blank']) && $linha['blank'] == 'right'){ ?>
                    <td colspan="2">&nbsp;</td>
                    <?php $i++;} ?>
                <?php if($i == 2):?></tr><?php endif;?>
                <?php endforeach;?>
                <tr>
                    <td class="botoes button-set" colspan="4"><input type="submit" class="botao input-button" value="Buscar" /></td>
                </tr>
            </table>
        <?php echo $form->end(false);/*no script*/?>
        <?php
        return ob_get_clean();
    }
    /*
     * ex de config: array(''=>'Mais recentes','id-a'=>'Mais velhos','a-z'=>'Título A-Z','sa'=>'Bloqueados')
     */
    function orderGet($id='form-order',$name='order'){
        $config = $this->_data;
        $value = $_GET[$name];
        ob_start();
?><form method="get" id="<?php echo $id;?>">
    <?php echo t('Ordenar por');?>: <select name="order" onchange="getElementById('<?php echo $id;?>').submit()">
        <?php foreach($config as $k => $v):?>
        <option value="<?php echo $k;?>"<?php echo ($k==$value) ? ' selected="selected"' : '';?>><?php echo $v;?></option>
        <?php endforeach;?>
    </select>
</form><?php
        return ob_get_clean();
    }
    
}
?>
