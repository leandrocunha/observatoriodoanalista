<?php

/**
 * Description of H_Navegacao
 *
 * @author Leandro
 */
class NavegacaoHelper {
    
    const CLASS_ACTIVE = 'active';
    /**
     * treeview
     * @param array $permissoes do usuario
     * @param boolean $is_root do usuario
     * @return string html 
     */
    function treeviewAdmin($permissoes=array(),$is_root='0'){
        $permissoes = (!is_array($permissoes)) ? array() : $permissoes;
        $is_root = ($is_root != '1')?false:true;
        //puxando as acls
		$file_acl = SYS_PATH.'etc/acl/admin.php';
		if(!file_exists($file_acl)){return '<!-- etc/acl/admin.php not found! -->';}
		$acl_geral=array();
        require $file_acl;
        ob_start();
?><ul id="browser" class="filetree">
<?php
##############      CONTEÚDO       ###############
$chave_ctrl = 'paginas';
$cont = 0;
//testar se tem  pelo menos 1 área de contaeudo
foreach($permissoes as $k => $v){
    $cont = (strpos($k,'paginas-')===0)?$cont+1:$cont;
}
if($cont > 0 || $is_root):
?>
    <li>
        <span class="folder">Conteúdos</span>
        <?php
        //buscando os conteudos relacionados as suas áreas
        //$rows = AreasModelo::eu()->tudo('titulo');
        $sql = AreasModel::me()->select()->where('status = 1')->orderBy('titulo');
        $rows = AreasModel::me()->exec($sql);
        if(count($rows)):
        ?>
        <ul>
        <?php foreach($rows as $row):
            if($is_root){
                $permitir=true;
            }else{
                $permitir=isset($permissoes[$chave_ctrl."-".$row->id]);
            }
            if($permitir):
            ?>
            <li>
                <span class="file" id="item-<?php echo $chave_ctrl."-".$row->id;?>">
                    <a href="<?php echo getBaseUrl(PERMALINK_ADMIN.'/'.$chave_ctrl.'/index/area/'.$row->id)?>">
                        <?php echo $row->titulo;?>
                    </a>
                </span>
            </li>
            <?php endif; ?>
        <?php endforeach; ?>
        </ul>
        <?php endif; ?>
    </li>
<?php endif; //--------------------------------
##############      OUTROS       ##############
$this->_foreachOutros($acl_geral,$permissoes,$is_root);
//-------------------------------- ?>
</ul>
<script type="text/javascript">
    //colocando o skema de active
    $j("#browser .file").click(function (){
        $j.cookie('nav-browser',null,{path:'/'});
        $j.cookie('nav-browser', this.id,{path:'/',expires: 1});
    });
    $j(document).ready(function (){
        $j("#"+$j.cookie('nav-browser')).addClass('<?php echo self::CLASS_ACTIVE?>');
    });
</script>
        <?php
        return ob_get_clean();
    }
	
	private function _foreachOutros($acl_geral,$permissoes,$is_root){
		foreach($acl_geral as $chave=>$valor){
			$chave_ctrl = $chave;
			$label = $valor['label'];
			$vis = $valor['visivel'];
			//exibir item
			if(isset($acl_geral[$chave_ctrl]['folder']) && $vis){
				$subs = $acl_geral[$chave_ctrl]['folder'];
				$label = $acl_geral[$chave_ctrl]['label'];
			?>
			<li>
				<span class="folder"><?php echo $label;?></span>
				<ul>
				<?php
				$this->_foreachOutros($subs,$permissoes,$is_root);
				?></ul>
			</li>
			<?php
			}else{
				$permitir = ($is_root)?true:isset($permissoes[$chave_ctrl]);
				if($permitir && $vis): #3
					if(isset($acl_geral[$chave_ctrl]['sub-item'])){
						$sub = $acl_geral[$chave_ctrl]['sub-item'];//subitens são casos de dependencia de um grupo, como no caso do config_grupos e config e banners com categs
				/* file */ ?>
			<li>
				<span class="folder"><?php echo $label;?></span>
				<?php
					//buscando os sub-itens
						$classe = camelcase("$sub-model");
						eval("\$t = {$classe}::me();");
						$sql = $t->select('id,nome')->orderBy('nome');
						if(!$is_root && $sub == 'grupos-config'){
							$sql->where('root <> 1')->where('status = 1');//configs ocultos
						}
						$rows = $t->exec($sql);
						if(count($rows)): #2
				?>
				<ul>
					<?php foreach($rows as $row): #1 ?>
					<li>
						<span class="file" id="item-<?php echo $chave_ctrl.'-'.$sub."-".$row->id;?>">
							<a href="<?php echo getBaseUrl(PERMALINK_ADMIN.'/'.$chave_ctrl.'/index/'.$sub.'/'.$row->id)?>">
								<?php echo $row->nome;?>
							</a>
						</span>
					</li>
					<?php endforeach; #1 ?>
				</ul>
				<?php endif; #2 ?>
			</li>
			<?php //subitens
					}else{
						$url = (isset($valor['url'])) ? $valor['url'] : $chave_ctrl;
				/* file */ ?>
			<li>
				<span class="file" id="item-<?php echo $chave_ctrl;?>">
					<a href="<?php echo getBaseUrl(PERMALINK_ADMIN.'/'.$url) ?>">
						<?php echo $label;?>
					</a>
				</span>
			</li>
			<?php
					}
				endif; #3
			}
		}
		
	}
    /**
     * paginador para consultas diretamente
     * @param Cylix_SQL_Select $sql obj select para os resultados
     * @param string $PGR variavel por referencia da view que irá receber o html do paginador
     * @param int $limite limit clause
     * @param array $params parametros adicionais, como por exemplo, os filtros
     * @param string $tot_sql sql para montar o total
     * @return Cylix_Model_Row array do sql passado 
     */
    function paginador(Cylix_SQL_Select $sql,&$PGR,$limite=40,$params=array(),$cfg_db='default',$tot_sql=null){
        //$tabela = $sql->_from;
        $sql_env = $sql->getSQL();
        $sub_sql = explode('FROM',$sql_env);
        if(count($sub_sql)==2){
            $sub_sql = ($tot_sql) ? $tot_sql : "SELECT COUNT(*) as total FROM".$sub_sql[1];
        }else{
            $e = new Cylix_Exception('Não será possível paginar a seguinte SQL<br/>'.$sql_env, 'Montagem incorreta de '.__CLASS__.' ->'.__FUNCTION__);
            $PGR = $e->getMessage();
            return array();
        }
        $res = Cylix_SQL::exec($sub_sql,'abstract',$cfg_db);
        if(strpos($sub_sql, 'GROUP') || strpos($sub_sql, 'group')){
            $total = count($res);
        }else{
            $total = $res[0]->total;
        }
        
        $total=($total > 0)?$total:1;//negativo e 0 nunca!
        $limite = ($limite > 0)?$limite:1;//negativo e 0 nunca!
        $pgs = ceil($total / $limite); //arredonda pra cima
        $atual = (isset($_GET['pg']) && $_GET['pg'] > 0) ? $_GET['pg'] : 1; //negativo e 0 nunca!
        $url_base = '';
        //montando a uri de parametros get q está no momento
        foreach ($params as $P) {
            if(isset($_GET["$P"])){
                $url_base .= '&' . $P . '=' . $_GET["$P"];
            }
        }
        $url_base = (strlen($url_base) > 1) ? substr($url_base, 1) : ''; //removendo 0 1º caso tenha
        $url_base = "?$url_base&pg=";
        ob_start();
        ?><nav class="pagination pagination-centered">
			<ul>
        <?php if ($pgs > 1): ?>
            <?php /* ANTERIOR */ ?>
            <?php if ($atual <= 1): ?>
				<li class="prev blocked"><a> &lt; </a></li>
            <?php else: ?>
				<li class="prev"><a href="<?php echo $url_base . ($atual - 1); ?>"> &lt; </a></li>
            <?php endif; ?>
            <?php /* MEIO de 1 até total */ ?>
            <?php for ($i = 1; $i <= $pgs; $i++) { ?>
                <?php if ($i == $atual): ?>
				<li class="active"><a href="#"> <?php echo $i; ?> </a></li>
                <?php else: ?>
				<li><a href="<?php echo $url_base . $i; ?>"> <?php echo $i; ?> </a></li>
                <?php endif; ?>
            <?php } ?>
            <?php /* PROXIMO */ ?>
            <?php if ($atual >= $pgs): ?>
				<li class="next blocked"><a> &gt; </a></li>
            <?php else: ?>
				<li class="next"><a href="<?php echo $url_base . ($atual + 1); ?>"> &gt; </a></li>
            <?php endif; ?>
        <?php endif; ?>
			</ul>
		</nav>
        <?php
        $PGR = ob_get_clean();
        $atual--;
        return $sql->limit($limite,$atual * $limite);
    }
    
    public function permissao($chave='index',$acao='index'){
        $u = (FLOW == MODULE_ADMIN) ? Admin_AbstractController::session() : new stdClass();
        $permissoes = $u->permissoes;
        if($u->is_root == '1'){
            return true;
        }else{
            return isset($permissoes[$chave][$acao]);
        }
    }
    /**
     * links e botoes para o grid do admin
     * @param type $ctrl
     * @param type $acao
     * @param type $params
     * @param type $sub_perm ex.: paginas-1
     * @param type $title
     * @param type $class 
     */
    public function acoesGrid($ctrl='index',$acao='index',$params='',$sub_perm='',$title='Informações',$class=''){
        $chave = $ctrl.$sub_perm;//caso seja para paginas,
        if($this->permissao($chave, $acao)){
            if($acao == 'index'): ?>
<a class="ver" title="Ver" href="<?php echo getBaseUrl("$ctrl/$acao/$params"); ?>" target="_blank">&nbsp;</a>
            <?php elseif($acao == 'novo'): ?>
<input type="button" value="Novo" class="botao input-button" onclick="window.location = '<?php echo getBaseUrl(PERMALINK_ADMIN."/$ctrl/$acao/$params"); ?>';"/>
            <?php elseif($acao == 'editar'): ?>
<a class="editar" title="Editar Registro" href="<?php echo getBaseUrl(PERMALINK_ADMIN."/$ctrl/$acao/$params"); ?>">&nbsp;</a>
            <?php elseif($acao == 'deletar'): ?>
<a class="deletar" title="Excluir Registro" href="<?php echo getBaseUrl(PERMALINK_ADMIN."/$ctrl/$acao/$params"); ?>" onclick="return confirm('Deseja realmente excluir este registro?');">&nbsp;</a>
            <?php elseif($acao == 'publicar'): ?>
<a class="publicar" title="Inverter Status" href="javascript: void(0);" onclick="alterarStatus(this,'<?php echo getBaseUrl(PERMALINK_ADMIN."/$ctrl/$acao/$params"); ?>');">&nbsp;</a>
            <?php else: ?>
<a class="<?php echo $class;?>" title="<?php echo $title;?>" href="<?php echo getBaseUrl(PERMALINK_ADMIN."/$ctrl/$acao/$params"); ?>">&nbsp;</a>
            <?php endif;
        }
    }
    /**
     * BreadCrumbs renderer
     * @param array $bc_array um array contendo array invertidos (array final primeiro, etc.)
     * @param string $parent chave estrangeira $parent.'_id' para recursividade mas deve conter no array acima um indice 'obj'
     * @param string $separador
     * @example
     * echo $this->helper->navegacao()->BC(array(
        array(
            'url'=>getBaseUrl($categ->permalink),
            'label' => $categ->nome,
            'obj' => $categ
            )
        ),'categorias');
     * 
     * echo $this->helper->navegacao()->BC(array(
        array(
            'url'=>getBaseUrl('clientes/pedidos'),
            'label' => 'Meus Pedidos'
            ),
        array(
            'url'=>getBaseUrl('clientes'),
            'label' => 'Minha Área'
            )
        ),null); 
     */
    function BC($bc_array,$parent='areas',$separador=' &gt; '){
        $first = $bc_array[0];
        $cache = Cylix_Cache::me();
        $cache->lifetime(5*60);
        $cache->type(Cylix_Cache::TYPE_VIEW);
        $ret = '';$name='bc-'.$first['url'];
        if($cache->isValid($name) && ENV =='online'){
            $ret = $cache->get($name);
        }else{
            //no $bc_array jah deve vir  primero nó já pronto, abaixo eh o teste pra ver se existe parent
            if($parent){
                $this->includeNode($bc_array,$first['obj'],$parent);
            }
            //home
            $bc_array[] = array(
                'url' => getBaseUrl(''),
                'label' => 'HOME'
            );
            $bc_array = array_reverse($bc_array);
            //renderizar
            $html = array();
            foreach($bc_array as $i=>$item){
                ob_start();
            ?> <a href="<?php echo ($item['url']==null)?'javascript:':$item['url'];?>">
                <?php echo $item['label'];?>
            </a>
                <?php
                $html[] = ob_get_clean();
            }
            $ret = implode($separador, $html);
            $cache->set($name, $ret);
        }
        
        return $ret;
    }
    function includeNode(&$bc_array,$obj,$parent){
        if($obj){
            $p_id = $parent."_id";
            if($obj->{$p_id} > 0){
                //existe parent
                $aux = $obj->pai(false);
                if($aux){
                    $bc_array[] = array(
                        'url' => getBaseUrl($aux->permalink),
                        'label' => $aux->nome
                    );
                    $this->includeNode($bc_array,$aux,$parent);
                }
            }
        }
    }
    
}

?>
