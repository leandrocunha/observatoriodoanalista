<?php
class MidiasModel extends AbstractModel {
    const ARQUIVOS = 0;
    const FOTOS = 1;
    const VIDEOS = 2;
    const AUDIOS = 3;
    const STREAM = 4;
    const PROIBIDOS = -1;
    
    protected $_file_path = 'uploads/midias';
    
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return MidiasModel
     */
    public static function me(){
        $table = 'midias';
        if(!isset(self::$_instance[$table])){
            self::$_instance[$table] = new self($table);
        }
        return self::$_instance[$table];
    }
    /**
     * Passa uma linha de Paginas e retorna as midias
     * @param Cylix_Model_Row $pagina result row
     * @param int $tipo Tipo de midia ex: MidiasModelo::FOTOS
     * @param string $order criterio de ordenação do resultado da busca de midias (cadastro DESC)
     * @return array array de Cylix_Conexao_Linha's da tabela Midias
     */
    static function getMidias(Cylix_Model_Row $pagina,$tipo=null,$order=null){
        $order = ($order)?$order:'cadastro DESC';
        $m = self::me();
        $sql = $m->select()->where('paginas_id = ?',$pagina->id)->orderBy($order);
        if($tipo){
            $sql->where('tipo = ?',$tipo);
        }
        $r = $m->exec($sql);
        return $r;
    }
    
    function getExtArray($tipo){
        $array = array();
        switch($tipo){
            case $tipo == self::AUDIOS: $array = array('mp3','mpeg','wav');
                break;
            case $tipo == self::FOTOS: $array = array('jpeg','jpg','gif','png');
                break;
            case $tipo == self::PROIBIDOS: $array = array('bat');
                break;
        }
        return $array;
    }
}
?>