<?php
class PaginasModel extends AbstractModel {
    
    protected $_file_path = 'uploads/paginas';
    
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return PaginasModel
     */
    public static function me(){
        $table = 'paginas';
        if(!isset(self::$_instance[$table])){
            self::$_instance[$table] = new self($table);
        }
        return self::$_instance[$table];
    }
	
	function getFiltroDatas($areas_id=0){
		$sql = "SELECT DATE_FORMAT(p.`data`,'%m-%Y') AS data_p FROM areas a INNER JOIN paginas p ON p.areas_id = a.id WHERE a.id = $areas_id AND p.`data` IS NOT NULL GROUP BY data_p ORDER BY data_p DESC LIMIT 50";
		return $this->exec($sql);
	}
}
?>