<?php
class ConfigGruposModel extends AbstractModel {
    
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return ConfigGruposModel
     */
    public static function me(){
        $table = 'config_grupos';
        if(!isset(self::$_instance[$table])){
            self::$_instance[$table] = new self($table);
        }
        return self::$_instance[$table];
    }
}
?>