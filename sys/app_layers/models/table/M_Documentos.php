<?php
class DocumentosModel extends AbstractModel {
    
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return DocumentosModel
     */
    public static function me(){
        $table = 'documentos';
        if(!isset(self::$_instance[$table])){
            self::$_instance[$table] = new self($table);
        }
        return self::$_instance[$table];
    }
	
	function recentes($limit=1){
		$sql = $this->select()->where('status = 1')->orderBy('data DESC,id DESC')->limit($limit);
		$ret = $this->exec($sql);
		if($limit<2){
			$ret = first($ret);
		}
		return $ret;
	}
	function getFiltroDatas(){
		$sql = "SELECT DATE_FORMAT(`data`,'%m-%Y') AS data_d FROM documentos WHERE `data` IS NOT NULL GROUP BY data_d ORDER BY data_d DESC LIMIT 50";
		return $this->exec($sql);
	}
}