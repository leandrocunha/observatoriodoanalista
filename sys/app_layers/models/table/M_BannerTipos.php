<?php
class BannerTiposModel extends AbstractModel {
    
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return BannerTiposModel
     */
    public static function me(){
        $table = 'banner_tipos';
        if(!isset(self::$_instance[$table])){
            self::$_instance[$table] = new self($table);
        }
        return self::$_instance[$table];
    }
	
	function findByChave($chave){
		$sql = $this->select()->where('chave = ?',$chave);
		return first($this->exec($sql));
	}
}
?>