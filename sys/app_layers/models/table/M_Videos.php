<?php
class VideosModel extends AbstractModel {
    
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return VideosModel
     */
    public static function me(){
        $table = 'videos';
        if(!isset(self::$_instance[$table])){
            self::$_instance[$table] = new self($table);
        }
        return self::$_instance[$table];
    }
	
	function recentes($limit=1){
		$sql = $this->select()->where('status = 1')->orderBy('criacao DESC,id DESC')->limit($limit);
		$ret = $this->exec($sql);
		if($limit<2){
			$ret = first($ret);
		}
		return $ret;
	}
	function getFiltroDatas(){
		$sql = "SELECT DATE_FORMAT(`criacao`,'%m-%Y') AS data_v FROM videos WHERE `criacao` IS NOT NULL GROUP BY data_v ORDER BY data_v DESC LIMIT 50";
		return $this->exec($sql);
	}
}
?>