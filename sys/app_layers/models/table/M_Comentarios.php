<?php
class ComentariosModel extends AbstractModel {
    
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return ComentariosModel
     */
    public static function me(){
        $table = 'comentarios';
        if(!isset(self::$_instance[$table])){
            self::$_instance[$table] = new self($table);
        }
        return self::$_instance[$table];
    }
	function pendentes(){
		return $this->getTotalRows('status = 0');
	}
}
?>