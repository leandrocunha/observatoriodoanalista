<?php
class BannersModel extends AbstractModel {
    
    protected $_file_path = 'uploads/banners';
    
    protected $_validate = array(
        'titulo' => array(
            'required' => array(true, 'Título é obrigatório')
        ),
        'banner_tipos_id' => array(
            'required' => array(true, 'Especifique o tipo de Banner')
        )
    );
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return BannersModel
     */
    public static function me(){
        $table = 'banners';
        if(!isset(self::$_instance[$table])){
            self::$_instance[$table] = new self($table);
        }
        return self::$_instance[$table];
    }
    
    static function todosPorChave($chave,$order='ordem, titulo'){
        $m = BannerTiposModel::me();
        $sql = $m->select()->where('chave = ?',$chave)->limit();
        $tipo = first($m->exec($sql));
        //buscando os itens relacionados
        $m = self::me();
        $sql = $m->select()->where('banner_tipos_id = ?',$tipo->id)
                ->where('status = 1')
                ->orderBy($order);
        return $m->exec($sql);
    }
    
    
}
?>