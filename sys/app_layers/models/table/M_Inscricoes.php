<?php
class InscricoesModel extends AbstractModel {
    
	protected $_validate = array(
		'email' => array('uniq'=>array(true,'E-mail já cadastrado no sistema'))
	);
	
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return InscricoesModel
     */
    public static function me(){
        $table = 'inscricoes';
        if(!isset(self::$_instance[$table])){
            self::$_instance[$table] = new self($table);
        }
        return self::$_instance[$table];
    }
	/**
	 * retorna o total de linhas
	 * @return int 
	 */
	function inscritos(){
		return $this->getTotalRows();
	}
	
}
?>