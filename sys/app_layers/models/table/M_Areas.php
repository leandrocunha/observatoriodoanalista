<?php

/**
 * Description of M_Areas
 *
 * @author Leandro
 */
class AreasModel extends AbstractModel {
    
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return AreasModel
     */
    public static function me(){
        $table = 'areas';
        if(!isset(self::$_instance[$table])){
            self::$_instance[$table] = new self($table);
        }
        return self::$_instance[$table];
    }
	
	function findByPermalink($permalink){
		$sql = $this->select()->where('permalink = ?',$permalink)->where('status = 1')->limit();
		return first($this->exec($sql));
	}
}

?>
