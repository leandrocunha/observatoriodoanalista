<?php
class ConfigsModel extends AbstractModel {
    
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return ConfigsModel
     */
    public static function me(){
        $table = 'configs';
        $pk = 'chave';
        if(!isset(self::$_instance[$table])){
            self::$_instance[$table] = new self($table,$pk);
        }
        return self::$_instance[$table];
    }
    
    public static function getValor($chave){
        if(FLOW == MODULE_SITE){
            //cache
            $cache = Cylix_Cache::me();
            $cache->type(Cylix_Cache::TYPE_QUERY);
            $cache->lifetime(30);
            $name = "conf_{$chave}";
            if($cache->isValid($name)){
                $cfg = $cache->get($name);
            }else{
                $cfg = self::me()->find($chave);
                $cache->set($name, $cfg);
            }
        }else{
            //normal
            $cfg = self::me()->find($chave);
        }
        if($cfg){
            $v = $cfg->valor;
            $v = str_replace(array('’','&rsquo;'), array("'","'"), $v);
            return $v;
        }else{
            return '<!-- Chave não encontrada: '.$chave.' -->';
        }
    }
}
?>