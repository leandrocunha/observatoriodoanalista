<?php
class MenusModel extends AbstractModel {
    
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return MenusModel
     */
    public static function me(){
        $table = 'menus';
        if(!isset(self::$_instance[$table])){
            self::$_instance[$table] = new self($table);
        }
        return self::$_instance[$table];
    }
    static function getMenuPorChave($chave){
        $m = self::me();
        $sql = $m->select()->where('chave = ?',$chave)->limit();
        $menu = first($m->exec($sql));
        //buscando os itens relacionados
        $sql = $m->select()->where('menus_id = ?',$menu->id)
                ->where('status = 1')
                ->orderBy('ordem, titulo');
        try{
        $ret = $m->exec($sql);
        }catch(Cylix_Exception $e){
            die($e);
        }
        return $ret;
    }
}
?>