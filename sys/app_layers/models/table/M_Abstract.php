<?php

class AbstractModel extends Cylix_Model {

    const EXEC_TYPE_ALL = 'all';
    const EXEC_TYPE_FIND = 'find';
    const EXEC_TYPE_REMOVE = 'remove';
    const EXEC_TYPE_CREATE = 'create';
    const EXEC_TYPE_ALTER = 'alter';

    protected $_file_path = 'uploads';

    /**
     * Busca rápida de todos os registros, podendo ordenar
     * @param string $orderBy colunas e nivelamento (titulo,id DESC)
     * @return array|boolean
     * @example Cylix_Modelo::me()->all('id DESC')
     */
    function all($orderBy = null) {
        return $this->exec(parent::all($orderBy), self::EXEC_TYPE_ALL);
    }

    /**
     * Busca rápida pela chave primária (uma chave somente)
     * @param mixed $PK inteiro ou array das primarias
     * @return Cylix_Model_Row
     */
    function find($PK = 0) {
        $res = $this->exec(parent::find($PK), self::EXEC_TYPE_FIND);
		if(is_array($res))
			return $res[0];
		else
			return $res;
    }

    /**
     * Remove diretamente um registro  pela sua PK
     * @param int $PK
     * @return boolean
     */
    function remove($PK = 0) {
        return $this->exec(parent::remove($PK), self::EXEC_TYPE_REMOVE);
    }

    /**
     * criação de registro por meio de um array associativo [array('coluna'=>'valor')]
     * @param array $valores
     * @return boolean
     * @example M_Modelo::eu()->criar(array('titulo'=>'teste','autor'=>'Cylix'))
     */
    function create($valores, $seguro = true) {
        $ret = $this->exec(parent::create($valores, $seguro), self::EXEC_TYPE_CREATE);
        $this->afterCreate($ret);
        return $ret;
    }

    /**
     * por meio de uma PK(definida no modelo) altera-se os valores
     * @param string $PK nome da coluna PK
     * @param array $valores valores em array = ao criar()
     * @param boolean $seguro flag para ativar/desativar o anti-sql-injection
     * @return boolean execução do script
     */
    function alter($PK = 0, $valores = array(), $seguro = true) {
        $ret = $this->exec(parent::alter($PK, $valores, $seguro), self::EXEC_TYPE_ALTER);
        $this->afterAlter($ret);
        return $ret;
    }

    /**
     * retorna o path correspondente ao modelo, ex.: upload/banners
     * @return string caminho relativo para uploads 
     */
    public function getFilePath() {
        return $this->_file_path;
    }

    /**
     * remove arquivos semelhantes baseando-se em um nome
     * @param string $nome 
     */
    public function unlinkFiles($nome = '?') {
        $nome = explode('/', $nome);
        $nome = end($nome);
        $path = $this->getFilePath();
        $files = _listDir($path, 'is_file', '*', $nome);

        foreach ($files as $f) {
            @unlink($path . '/' . $f);
        }
    }
	
	function getTotalRows($where=null){
		$sql = $this->select('count(*) as total');
		if($where){
			$sql->where($where);
		}
		$row = first($this->exec($sql));
		return $row->total;
	}

    /**
     * validação de data e hora brasileira
     * @param mixed $val valor avaliado
     * @param mixed $param true: usar a chave $key como campo| outro: campo especificado
     * @param type $key chave do array sob avaliação (post)
     * @return boolean 
     */
    function checkDataTempo($val, $param, $key = null) {
        $ret = true;
        if ($param == 'data') {
            $data = explode("/", "$val"); // fatia a string $dat em pedados, usando / como referência
            $d = $data[0];
            $m = $data[1];
            $y = $data[2];
            $res = @checkdate($m, $d, $y);
            $ret = ($res == 1);
        }
        return $ret;
    }
    
    /**
     * verifica via regex o valor para email
     * @param string $val
     * @param null $param
     * @param null $key
     * @return boolean 
     */
    function checkEmail($val, $param=null, $key = null) {
        return $this->checkRegex($val, "/^[a-z0-9_\+-]+(\.[a-z0-9_\+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.([a-z]{2,4})$/");
    }

}
