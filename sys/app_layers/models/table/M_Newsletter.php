<?php
class NewsletterModel extends AbstractModel {
    
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return NewsletterModel
     */
    public static function me(){
        $table = 'newsletter';
        if(!isset(self::$_instance[$table])){
            self::$_instance[$table] = new self($table);
        }
        return self::$_instance[$table];
    }
}
?>