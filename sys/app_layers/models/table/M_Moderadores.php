<?php

class ModeradoresModel extends AbstractModel {
    
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return ModeradoresModel
     */
    public static function me(){
        $table = 'moderadores';
        if(!isset(self::$_instance[$table])){
            self::$_instance = array();
            self::$_instance[$table] = new ModeradoresModel($table);
        }
        return self::$_instance[$table];
    }
    public static function exists($login,$senha){
        $m = new ModeradoresModel('moderadores');
        $sql = $m->select()
                ->where('email = ?',$login)
                ->where('senha = ?', md5($senha))
                ->limit(1);
        //die($sql);
        $row = $m->exec($sql);
        return (count($row) >= 1) ? first($row) : false;
    }
}
?>