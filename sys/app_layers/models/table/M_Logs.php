<?php
class LogsModel extends AbstractModel {
    
    /**
     * retorna a instancia da classe pra economizar memoria
     * @return LogsModel
     */
    public static function me(){
        $table = 'logs';
        if(!isset(self::$_instance[$table])){
            self::$_instance[$table] = new self($table);
        }
        return self::$_instance[$table];
    }
}
?>