<?php
class PaginasModel_Row extends AbstractModel_Row {
    /**
    * retorna o Model correspondente
    * @return PaginasModel 
    */
    function getModel(){
        return PaginasModel::me();
    }
	
	function area($where=''){
		return $this->assocNto1($this, AreasModel::me(), '*', $where);
	}
	function midias($order='ordem,id DESC',$where='', $limit=null){
		return $this->assoc1toN($this, MidiasModel::me(), '*', $where, $order, $limit);
	}
	
	function getUrl(){
		return ($this->permalink) ? getBaseUrl($this->permalink) : getBaseUrl('index/paginas/id/'.$this->id);
	}
	
	function countComentarios(){
		$sql = "SELECT COUNT(*) AS total FROM comentarios WHERE paginas_id = {$this->id} AND `status`=1;";
		$row = first($this->getModel()->exec($sql));
		return (int)$row->total;
	}
	
	function comentarios($order='comentarios.id',$where='and comentarios.status = 1', $limit=null){
		return $this->assoc1toN($this, ComentariosModel::me(), '*', $where, $order, $limit);
	}
}
?>