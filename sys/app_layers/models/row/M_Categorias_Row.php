<?php
class CategoriasModel_Row extends AbstractModel_Row {
    
    function filhos($campos='*',$order='ordem, nome',$where=null){
        return $this->assoc1toN($this, CategoriasModel::me(), $campos,$where,$order);
    }
    
    function filhosMenu(){
        $sql = "SELECT cat.*,COUNT(c_p.parceiros_id) AS total_parc FROM categorias AS cat INNER JOIN categorias_parceiros c_p ON c_p.categorias_id = cat.id INNER JOIN parceiros p ON p.id = c_p.parceiros_id WHERE cat.categorias_id = {$this->id} AND p.status = 1 GROUP BY cat.id ORDER BY ordem, nome;";
        return CategoriasModel::me()->exec($sql);
    }
    function parceiros($where='AND parceiros.status=1',$order='parceiros.nome'){
        return $this->assocNtoN($this, ParceirosModel::me(), '*', $where,$order);
    }
    function pai($has_root=true){
        $ret = null;
        $pai = CategoriasModel::me()->find($this->categorias_id);
        if($pai){
            if($pai->status == '1'){
                if(!$has_root){
                    if(substr($pai->nome, 0,1)!=':'){
                        $ret = $pai;//nao puxara o :raiz:
                    }
                }else{
                    $ret = $pai;
                }
                
            }
        }
        return $ret;
    }
}
?>