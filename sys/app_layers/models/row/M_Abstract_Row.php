<?php
class AbstractModel_Row extends Cylix_Model_Row {
    public $helper;
    function __construct($table = '', $PK = 'id', $cfg_db = 'default', $cols = array()) {
        $this->helper = new Cylix_Helper();
        parent::__construct($table, $PK, $cfg_db, $cols);
    }
}
