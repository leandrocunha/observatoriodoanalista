<?php
class VideosModel_Row extends AbstractModel_Row {
    /**
    * retorna o Model correspondente
    * @return VideosModel 
    */
    function getModel(){
        return VideosModel::me();
    }
	function getThumb($small=false){
		return $this->helper->video($this->url)->getThumb($small);
    }
	function getIframe($width,$height){
		$this->helper->video($this->url)->getIframe($width,$height);
	}
}
?>