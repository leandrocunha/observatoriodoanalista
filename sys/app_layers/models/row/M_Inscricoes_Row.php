<?php
class InscricoesModel_Row extends AbstractModel_Row {
    /**
    * retorna o Model correspondente
    * @return InscricoesModel 
    */
    function getModel(){
        return InscricoesModel::me();
    }
	
	function beforeSave($type = null, $model = null, $post = array()) {
		$this->cadastro = now();
		parent::beforeSave($type, $model, $post);
	}
}
?>