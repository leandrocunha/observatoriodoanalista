<?php
class ComentariosModel_Row extends AbstractModel_Row {
    /**
    * retorna o Model correspondente
    * @return ComentariosModel 
    */
    function getModel(){
        return ComentariosModel::me();
    }
	
	function pagina($fields='*',$where=''){
		return $this->assocNto1($this, PaginasModel::me(), $fields, $where);
	}
	
	function beforeSave($type = null, $model = null, $post = array()) {
		if($type == ComentariosModel_Row::TYPE_SAVE_INSERT){
			$this->criacao = now();
			$this->status = '0';
		}
		parent::beforeSave($type, $model, $post);
	}
}
?>