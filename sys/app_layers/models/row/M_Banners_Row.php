<?php
class BannersModel_Row extends AbstractModel_Row {
    
    
    function thumbAdmin(){
        return $this->helper->imagem($this->imagem)->crop(180, 80);
    }
    
    function beforeSave($type = null, $model = null, $post = array()) {
        $m = $this->getModelTable();
        $post['ordem'] = $post['ordem']==null ? '0' : $post['ordem'];
        //helper para uploads
        $h_file = $this->helper->crudFile('imagem');
        $h_file->setPost($post);
        $h_file->setModels($m,$this);
        //checkbox de remover
        $h_file->hasUnlink();
        //salvando imagem
        $post = $h_file->save(2000);
        
        //------
        $this->put($post);//update values
        
        parent::beforeSave($type, $model, $post);
    }
}
?>