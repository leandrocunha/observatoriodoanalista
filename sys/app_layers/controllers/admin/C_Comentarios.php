<?php

class Admin_ComentariosController extends Admin_AbstractController {

	protected $_scopes = array(
                'email' => array('email LIKE ?','%$%',true),
                'status' => array('status = ?','$')
            );
    private $_orderBy = array(
        'sa' => 'status ASC,id DESC',
        'sd' => 'status DESC,id DESC',
        'cri-a' => 'criacao ASC'
    );
	
	function getModelo() {
		return ComentariosModel::me();
	}

	function actionIndex() {
		$t = $this->getModelo();
		$sql = $t->select('id,criacao,nome,email,status,paginas_id');
            $o = $this->getParam('order');
            if($o && isset($this->_orderBy[$o])){
				$sql = $sql->orderBy($this->_orderBy[$o]);
            }else{
				$sql = $sql->orderBy('criacao DESC');
			}
            $this->applyEscopes($sql);
		$sql = $this->helper->navegacao()->paginador($sql, $this->view->pgr, 50);
		$this->view->rows = $t->exec($sql);
	}

	function actionEditar() {
		$id = $this->getParam('id');
		$m = $this->getModelo();
		$reg = $this->view->reg = $m->find($id);
		if ($post = $this->getPost()) {
			$reg->put($post);
			//crando
			if ($reg->save()) {//$m->create($post)
				$this->setAlert("Comentário alterado com sucesso", $this->_ctrl, Cylix_View::ALERT_OK);
			} else {
				$this->setAlert("Houve uma falha ao salvar o registro", $this->_ctrl, Cylix_View::ALERT_ERROR);
			}
		}
	}

}

?>