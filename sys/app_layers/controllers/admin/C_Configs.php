<?php
 class Admin_ConfigsController extends Admin_AbstractController{
     
     function getModelo(){
        return ConfigsModel::me();
    }
    
    function  actionIndex() {
        $m = $this->getModelo();
        //buscando o grupo
        $this->view->g_id = $g_id = $this->getParam('config-grupos');
        //post
        if($post = $this->getPost()){
            #vardump($post);
            foreach($post as $chave=>$valor){
                if(!is_array($valor)){//valor simples
                    if(strpos($chave, '_remove') === false){
                        $f = $m->alter($chave, array('valor' => $valor), false);/*acionei o flag 'inseguro' para não lançar a rotina padrão de anti-sql-injection*/
                    }else{
                        //eh pra remover imagem?
                        if($post[$chave] == '1'){
                            //remove
                            $chave = str_replace('_remove', '', $chave);
                            $cfg = $m->find($chave);
                            @unlink($cfg->valor);//apaga a foto
                            $m->alter($chave, array('valor' => ''), false);/*acionei o flag 'inseguro' para não lançar a rotina padrão de anti-sql-injection*/
                        }
                    }
                }else{
                    //provavelmente eh um FILES
                    if($valor['size'] > 0){
                        $arquivo = new Cylix_Files($valor);
                        $arquivo->setFolder('uploads/configs');
                        $cfg = $m->find($chave);
                        /*if(strlen($cfg->valor)>0){
                            $nome = explode('/', $cfg->valor);
                            $nome = end($nome);
                            $nome = explode('.', $nome);
                            $nome = $nome[0];//captura o nome original para trocar com mesmo nome
                        }else{
                            $nome = uniqid();
                        }*/
                        if(strlen($cfg->valor)>0){
                            @unlink($cfg->valor);//apaga a foto
                        }
                        $nome = uniqid();
                        $arquivo->setName($nome);
                        if(strpos($arquivo->getType(), 'image') === false){
                            //nao eh imagem
                            if($arquivo->copy()){
                                $m->alter($chave, array('valor' => $arquivo->getFile()), false);
                            }else{
                                return new Cylix_Excecao("Não foi possível salvar o arquivo ".$arquivo->getOriginalName(), 'Falha em upload');
                            }
                        }else{
                            //eh imagem
                            if(strlen($cfg->options) > 0){
                                //tem o tamanho
                                $n = $arquivo->resizeMe($cfg->options, null, 85);
                            }else{
                                $n = $arquivo->resizeMe();//1024x1024
                            }
                            if($n != 'erro'){
                                //upload tranquilo
                                $m->alter($chave, array('valor' => $n), false);
                            }else{
                                return new Cylix_Exception("Não foi possível salvar o arquivo ".$arquivo->getOriginalName(), 'Falha em upload');
                            }
                        }
                    }
                }
            }
            $this->setAlert("Registros salvos com sucesso", $this->_ctrl, Cylix_View::ALERT_OK);
        }
    }
} 
?>