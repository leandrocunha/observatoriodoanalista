<?php

class Admin_MidiasController extends Admin_AbstractController {

    protected $_aux_acl = array(
        'upload' => 'novo'
    );
    

    function getModelo() {
        return MidiasModel::me();
    }
    
    function actionIndex(){
        if($this->getParam('layout') == 'ajax'){
            $this->view->setContext('ajax');
        }
        $this->view->flag_videos  = $this->view->flag_arquivos = true;
        
        //puxando a pagina relacionada
        $p_id = $this->getParam('id');
        $this->view->pagina = $pagina = PaginasModel::me()->find($p_id);
        
        if($pagina->id > 0){
            $m = $this->getModelo();
            $sql = $m->select()
                    ->where('paginas_id = ?', $p_id)
                    ->orderBy('tipo, ordem, cadastro DESC');
            
            $this->view->midias = $m->exec($sql);
            if($this->getParam('layout') != 'ajax'){
                //formulário de vídeos
                $f_videos = new Cylix_Form(getBaseUrl($this->_permalink.'/midias/novo'));
                $e = $f_videos->string('titulo');
                $f_videos->label('Título do Vídeo', $e);
                $e = $f_videos->string('caminho');
                $f_videos->label('Caminho / URL', $e);
                $f_videos->hidden('tipo', MidiasModel::VIDEOS);
                $f_videos->hidden('cadastro', date('Y-m-d H:i:s'));
                $f_videos->hidden('paginas_id', $p_id);
                $f_videos->hidden('ordem', '99');
                $f_videos->submit('s', 'Guardar');
                $this->view->f_videos = $f_videos;
                //requisitos: swfupload.js, swfupload.queue.js, fileprogress.js, handlers.js
                $lib = "lib/SWFUpload";
                Cylix_App::setJS('swfupload', getBaseUrl("$lib/swfupload.js"));
                Cylix_App::setJS('swfupload.queue', getBaseUrl("$lib/swfupload.queue.js"));
                Cylix_App::setJS('fileprogress', getBaseUrl("$lib/fileprogress.js"));
                Cylix_App::setJS('handlers', getBaseUrl("$lib/handlers.js"));
                Cylix_App::setJS('prettyPhoto', getBaseUrl("lib/prettyPhoto/prettyPhoto.js"));
                Cylix_App::setCSS('prettyPhoto', getBaseUrl("lib/prettyPhoto/prettyPhoto.css"));
                //formulario de edição
                $f_midia = new Cylix_Form(getBaseUrl($this->_permalink.'/midias/editar'),'post','form_edit');
                //somente id, titulo, tipo podem ser alterados
                $f_midia->hidden('id');
                $f_midia->hidden('c',$p_id);
                $e = $f_midia->int('ordem');
                $f_midia->label('Ordem', $e);
                $e = $f_midia->string('titulo');
                $f_midia->label('Título', $e);
                $e = $f_midia->select('tipo', '',
                        array(
                            MidiasModel::ARQUIVOS=>'Arquivo',
                            MidiasModel::FOTOS=>'Foto',
                            MidiasModel::AUDIOS=>'Áudio',
                            MidiasModel::VIDEOS=>'Vídeo'
                            )
                        );
                $f_midia->label('Tipo', $e);
                $f_midia->submit('m', 'Alterar');
                $this->view->f_midia = $f_midia;
            }else{
                $this->view->setContext('ajax');
            }
            
        }else{
            throw new Cylix_Exception('Página não encontrada','Midias erro');
        }
    }
    
    function actionNovo(){
        //somente vídeos
        if($post = $this->getPost()){
            if($this->getModelo()->create($post)){
                $this->setAlert("Mídia inserida com sucesso",'midias',  Cylix_View::ALERT_OK);
            }else{
                $this->setAlert("Não foi possível inserir a mídia",'midias',  Cylix_View::ALERT_ERROR);
            }
        }
        $this->redirecionar($this->_permalink.'/midias/index/id/'.$this->getParam('paginas_id'));
    }
    
    function actionEditar(){
        if($post = $this->getPost()){
            $m_id = $post['id'];
            unset($post['id']);
            if($this->getModelo()->alter($m_id, $post)){
                $this->setAlert("Mídia alterada com sucesso",'midias',  Cylix_View::ALERT_OK);
            }else{
                $this->setAlert("Não foi possível alterar a mídia selecionada",'midias',  Cylix_View::ALERT_ERROR);
            }
        }
        $this->redirecionar($this->_permalink.'/midias/index/id/'.$this->getParam('c'));
    }
    
    public function  actionDeletar() {
        //deletar o array de midias
        $midias = $this->getParam('midias');
        if(count($midias)){
            $m = $this->getModelo();
            foreach($midias as $row_id){
                $reg = $m->find($row_id);
                if($m->remove($row_id)){
                    //retira os arquivos a partir do name
                    $m->unlinkFiles($reg->caminho);//apaga as fotos
                }
            }
            $this->setAlert("Mídias removidas com sucesso",'midias',  Cylix_View::ALERT_OK);
        }
        $this->redirecionar($this->_permalink.'/midias/index/id/'.$this->getParam('c'));
    }
    
    function actionUpload(){
        try{
            $this->view->flag_render = false;
            $paginas_id = $_POST["paginas_id"];
            $m = $this->getModelo();
            if((int)$paginas_id > 0){
                set_time_limit(3600);
                if (isset($_POST["PHPSESSID"])) {
                    session_id($_POST["PHPSESSID"]);
                }
                if (!isset($_FILES["Filedata"]) || !is_uploaded_file($_FILES["Filedata"]["tmp_name"]) || $_FILES["Filedata"]["error"] != 0) {
                    echo "ERRO: arquivo enviado incorretamente";
                    exit(0);
                }
                //arquivo
                $arq = new Cylix_Files($_FILES["Filedata"]);
                $nome = $arq->getOriginalName();//o nome do arquivo será o titulo
                $ext = $arq->getExt();
                $nome = explode('.',$nome);
                array_pop($nome);//o ultimo ponto com certeza será a extensão
                $nome = implode('.', $nome);
                //tipos
                $arrayI = $m->getExtArray(MidiasModel::FOTOS);
                $arrayA = $m->getExtArray(MidiasModel::AUDIOS);
                //pasta e nome do arquivo
                $arq->setFolder($m->getFilePath());
                $n_arq = $arq->getName().".$ext";
                if(in_array($ext, $arrayI)){
                    //imagem
                    $tipo = MidiasModel::FOTOS;
                }elseif(in_array($ext, $arrayA)){
                    //audio
                    $tipo = MidiasModel::AUDIOS;
                }else{
                    //arquivo normal
                    $tipo = MidiasModel::ARQUIVOS;
                }
                $arq->copy();
                //cadastrando a midia no banco
                $caminho = $m->getFilePath()."/$n_arq";
                $cadastro = date('Y-m-d H:i:s');
                //craindo o registro
                $m->create(
                        array(
                            'caminho'=>$caminho,
                            'titulo'=>$nome,
                            'tipo'=>$tipo,
                            'cadastro'=>$cadastro,
                            'paginas_id'=>$paginas_id,
                            'ordem'=>'99'
                            )
                        );
            }else{
                die('Sem referência de páginas.');
            }
        }catch(Cylix_Exception $e){
            die('Erro interno');
        }
    }

}

?>