<?php

class Admin_MenusController extends Admin_AbstractController {

    
    function getModelo() {
        return MenusModel::me();
    }

    function actionIndex() {
        $t = $this->getModelo();
        $sql = $t->select()->where('menus_id IS NULL')->orderBy('ordem,titulo');
        $this->view->rows = $t->exec($sql);
    }
    
    function actionNovo(){
        $this->view->pai = $this->getModelo()->find($this->getParam('p'));
        //post
        if($post = $this->getPost()){
            $parent = $this->getParam('p');
            $post['menus_id'] = $parent;
            if((int)$parent > 0){
                $post['url'] = str_replace('/flag/usuario_'.FLOW, '', $post['url']);
                if($this->getModelo()->create($post)){
                    $this->setAlert("Item de Menu criado com sucesso", $this->_ctrl, Cylix_View::ALERT_OK);
                    $this->redirecionar($this->_ctrl);
                }else{
                    $this->setAlert("Houve uma falha ao salvar o registro", $this->_ctrl, Cylix_View::ALERT_ERROR);
                }
            }else{
                $this->setAlert('Devido à falta de parâmetros, não foi possível realizar o registro.', $this->_ctrl, Cylix_View::ALERT_ERROR);
                $this->redirecionar($this->_ctrl);
            }
        }
    }
    
    function actionEditar(){
        $id = $this->getParam('id');
        //post
        if($post = $this->getPost()){
            if((int)$id > 0){
                $post['url'] = str_replace('/flag/usuario_'.FLOW, '', $post['url']);
                $m = $this->getModelo();
                //alterando o registro
                if($m->alter($id, $post)){
                    $texto = "Item de Menu alterado com sucesso!";
                    $tipo = Cylix_View::ALERT_OK;
                }else{
                    $tipo = Cylix_View::ALERT_ERROR;
                    $texto = "Houve uma falha ao alterar o registro";
                }
                $this->setAlert($texto, $this->_ctrl, $tipo);
            }else{
                $this->setAlert('Devido à falta de parâmetros, não foi possível realizar o registro.', $this->_ctrl, Cylix_View::ALERT_ERROR);
                $this->redirecionar($this->_ctrl);
            }
            
        }
        $this->view->reg = $this->getModelo()->find($id);
     }

}

?>