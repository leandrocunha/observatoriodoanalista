<?php

class Admin_PaginasController extends Admin_AbstractController {
    
    protected $_scopes = array(
                'titulo' => array('titulo LIKE ?','%$%',true),
                'status' => array('status = ?','$')
            );
    private $_orderBy = array(
        'a-z' => 'titulo ASC,id DESC',
        'z-a' => 'titulo DESC,id DESC',
        'sa' => 'status ASC,id DESC',
        'sd' => 'status DESC,id DESC',
        'id-a' => 'id ASC'
    );

    function getModelo() {
        return PaginasModel::me();
    }

    function actionDeletar() {
        //removendo a imagem
        $m = $this->getModelo();
        $reg = $m->find($this->getParam('id'));
        if (strlen($reg->capa) > 5) {
            $m->unlinkFiles($reg->capa);
        }
        parent::actionDeletar('/index/area/' . $this->getParam('area'));
    }

    private function _settings(){
        $area_id = $this->getParam('area');
        $this->view->area = AreasModel::me()->find($area_id);
        $this->view->logado = $this->getLogado();
    }
    
    function actionIndex() {
        $this->_settings();
        //buscando a área
        $area_id = $this->getParam('area');
        if((int)$area_id > 0){
            $t = $this->getModelo();
            $sql = $t->select()
                    ->where('areas_id = ?', $area_id);
            $o = $this->getParam('order');
            $criterios = 'id DESC';//default
            if($o && isset($this->_orderBy[$o])){
                $criterios = $this->_orderBy[$o];
            }
            $this->applyEscopes($sql);
            $sql = $sql->orderBy($criterios);
            //die($sql);
            $this->view->filters = $this->getFilterScopes();
            $sql = $this->helper->navegacao()->paginador($sql, $this->view->pgr, $this->n_pagina, array('order'));//
            //paginação
            $this->view->rows = $t->exec($sql);
        }else{
            $this->redirecionar('');
        }
    }
    
    public function actionPublicar() {
        //pagina
        $m = $this->getModelo();
        $pag = $m->find($this->getParam('id'));
        $status = ($pag->status == 1) ? 0 : 1;
        if($m->alter($this->getParam('id'), array('status'=>$status))){
            $this->view->status = $status;
        }else{
            throw new Cylix_Exception('Erro ao alterar status', 'Páginas');
        }
        //$this->redirecionar($this->_ctrl . '/index/area/' .$this->getParam('area'));
    }

    function actionNovo() {
        $this->_settings();
        if ($post = $this->getPost()) {
            dateToSQL(array('data'), $post);
            
            if(Cylix_Form::requiredFields(array('status','titulo'))){
                //testando se possui permissao para publicar
                $nav = $this->helper->navegacao();
                if(!$nav->permissao($this->_ctrl.'-'.$this->getParam('area'),'publicar')){
                    $post['status'] = '0';//se não tem permissão de publicar, então vai ficar o default
                }
                //ajustando o seo title
                //$post['titulo'] = trim($post['titulo']);
                $post['seo_title'] = (strlen($post['seo_title']) > 1) ? $post['seo_title'] : $post['titulo'];
                //gerar ou não um permalink
                if ($this->getParam('autolink') == '1') {
                    //buscando a area
                    $area = AreasModel::me()->find($this->getParam('area'));
                    if($area->back_link == '1'){
                        $post['permalink'] = transPermalink($area->permalink.'/'.$post['titulo']);
                    }else{
                        $post['permalink'] = transPermalink($post['titulo']);
                    }
                }
                $m = $this->getModelo();
                
                //tratando imagem de capa
                if($post['capa']['size'] > 0){
                    //salvando a imagem
                    $arquivo = new Cylix_Files($post['capa']);
                    $arquivo->setFolder($m->getFilePath());
                    $nome = uniqid();
                    $arquivo->setName($nome);
                    if(strpos($arquivo->getType(), 'image') !== false){
                        //resize
                        $n = $arquivo->resizeMe(800);//800x800
                        //jah enviada
                        if($n != 'erro'){
                            //upload tranquilo, pega o nome para salver
                            $post['capa'] = $n;
                        }else{
                            return new Cylix_Exception("Não foi possível salvar o arquivo ".$arquivo->getOriginalName(), 'Falha em upload');
                        }
                    }
                }else{
                    unset($post['capa']);
                }
                
                //criando
                if ($m->create($post,false)) {
                    $this->setAlert("Conteúdo criado com sucesso", $this->_ctrl, Cylix_View::ALERT_OK);
                    $this->redirecionar($this->_ctrl . '/index/area/' .$this->view->area->id);
                } else {
                    $this->setAlert("Houve uma falha ao salvar o registro", $this->_ctrl, Cylix_View::ALERT_ERROR);
                }
            }else{
                $this->setAlert("Favor preencher os campos obrigatórios", $this->_ctrl, Cylix_View::ALERT_WARNING);
            }
        }
    }
    
    public function actionEditar(){
        $id = $this->getParam('id');
        $m = $this->getModelo();
        $c_antes = $m->find($id);
        //post
        if ($post = $this->getPost()) {
            dateToSQL(array('data'), $post);
            if(Cylix_Form::requiredFields(array('titulo')) === true){
                //testando se possui permissao para publicar
                $nav = $this->helper->navegacao();
                if(!$nav->permissao($this->_ctrl.'-'.$this->getParam('area'),'publicar')){
                    unset($post['status']);
                }
                //ajustando o seo title
                $post['titulo'] = trim($post['titulo']);
                $post['seo_title'] = (strlen($post['seo_title']) > 1) ? $post['seo_title'] : $post['titulo'];
                //gerar ou não um permalink
                if ($this->getParam('autolink') == '1') {
                    //buscando a area
                    $area = AreasModel::me()->find($this->getParam('area'));
                    if($area->back_link == '1'){
                        $post['permalink'] = $area->permalink.'/'.transPermalink($post['titulo']);
                    }else{
                        $post['permalink'] = transPermalink($post['titulo']);
                    }
                }
                
                //eh pra remover imagem?
                if($post['capa_remove'] == 1){
                    //remove
                    $m->unlinkFiles($c_antes->capa);
                    $post['capa'] = '';
                }else{
                    //se mandou outra imagem
                    if($post['capa']['size'] > 0){
                        //salvando a imagem
                        $arquivo = new Cylix_Files($post['capa']);
                        $arquivo->setFolder($m->getFilePath());
                        $nome = uniqid();
                        $arquivo->setName($nome);
                        if(strpos($arquivo->getType(), 'image') !== false){
                            //resize
                            $n = $arquivo->resizeMe(800);//800x800
                            //jah enviada
                            if($n != 'erro'){
                                //upload tranquilo, pega o nome para salver
                                $post['capa'] = $n;
                            }else{
                                return new Cylix_Exception("Não foi possível salvar o arquivo ".$arquivo->getOriginalName(), 'Falha em upload');
                            }
                        }
                    }else{
                        $post['capa'] = $c_antes->capa;
                    }
                }
                #vardump($post);
                //alterando o registro
                if ($m->alter($id, $post,false)) {
                    $texto = "Conteúdo alterado com sucesso!";
                    $tipo = Cylix_View::ALERT_OK;
                } else {
                    $tipo = Cylix_View::ALERT_ERROR;
                    $texto = "Houve uma falha ao alterar o registro";
                }
                $this->setAlert($texto, $this->_ctrl, $tipo);
            }else{
                $this->setAlert("Favor preencher os campos obrigatórios", $this->_ctrl, Cylix_View::ALERT_WARNING);
            }
            
        }
        $this->view->reg = $m->find($this->getParam('id'));
    }

}

?>