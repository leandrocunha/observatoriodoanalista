<?php

class Admin_VideosController extends Admin_AbstractController {

	protected $_scopes = array(
		'titulo' => array('titulo LIKE ?', '%$%', true),
		'status' => array('status = ?', '$'),
		'criacao' => array('DATE(criacao) = ?', '$')
	);
	private $_orderBy = array(
		'a-z' => 'titulo ASC,id DESC',
		'z-a' => 'titulo DESC,id DESC',
		'sa' => 'status ASC,id DESC',
		'sd' => 'status DESC,id DESC',
		'cri-a' => 'criacao ASC,id ASC'
	);

	function getModelo() {
		return VideosModel::me();
	}

	function actionIndex() {
		$t = $this->getModelo();
		$sql = $t->select();
		$o = $this->getParam('order');
		$criterios = 'status, criacao DESC'; //default
		if ($o && isset($this->_orderBy[$o])) {
			$criterios = $this->_orderBy[$o];
		}
		$this->applyEscopes($sql);
		$sql = $sql->orderBy($criterios);
        $this->view->filters = $this->getFilterScopes();
		$sql = $this->helper->navegacao()->paginador($sql, $this->view->pgr, $this->n_pagina);
		$this->view->rows = $t->exec($sql);
	}

	function actionNovo() {
		if ($post = $this->getPost()) {
			$m = $this->getModelo();
			$post['url'] = (strpos($post['url'], 'http') !== false) ? $post['url'] : 'http://' . $post['url'];
			$post['criacao'] = (strlen($post['criacao']) > 1) ? $post['criacao'] : date('d/m/Y H:i:s');
			dateToSQL('criacao', $post);
			//pegando o id do video pra usar em embeds
			$post['v_id'] = (strpos($post['url'], 'vimeo')!==false) ? getVimeoIdFromURL($post['url']) : getYouTubeIdFromURL($post['url']);
			$row = $m->newRow();
			$row->put($post);
			//crando
			if ($row->save()) {//$m->create($post)
				$this->setAlert("Vídeo criado com sucesso", $this->_ctrl, Cylix_View::ALERT_OK);
				$this->redirecionar($this->_ctrl);
			} else {
				$this->setAlert("Houve uma falha ao salvar o registro", $this->_ctrl, Cylix_View::ALERT_ERROR);
			}
		}
	}

	function actionEditar() {
		$id = $this->getParam('id');
		$m = $this->getModelo();
		$reg = $this->view->reg = $m->find($id);
		if ($post = $this->getPost()) {
			$post['url'] = (strpos($post['url'], 'http') !== false) ? $post['url'] : 'http://' . $post['url'];
			$post['criacao'] = (strlen($post['criacao']) > 1) ? $post['criacao'] : date('d/m/Y H:i:s');
			dateToSQL('criacao', $post);
			//pegando o id do video pra usar em embeds
			$post['v_id'] = (strpos($post['url'], 'vimeo')!==false) ? getVimeoIdFromURL($post['url']) : getYouTubeIdFromURL($post['url']);
			$reg->put($post);
			//crando
			if ($reg->save()) {//$m->create($post)
				$this->setAlert("Vídeo alterado com sucesso", $this->_ctrl, Cylix_View::ALERT_OK);
			} else {
				$this->setAlert("Houve uma falha ao salvar o registro", $this->_ctrl, Cylix_View::ALERT_ERROR);
			}
		}
	}

}

?>