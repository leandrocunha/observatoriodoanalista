<?php

class Admin_ModeradoresController extends Admin_AbstractController {

    protected $_aux_acl = array(
        'senha' => false
    );
    function getModelo() {
        return ModeradoresModel::me();
    }

    function actionDeletar() {
        $m = $this->getModelo();
        $logado = $this->getLogado();
        $id = $this->getParam('id');
        $sql = $m->delete()->where($m->_pk.' = ?', $id);
        if ($logado->is_root != 1) {
            $sql->where('is_root <> ?', 1); //mocando os moderadores root caso o cara não seja root
        }
        if($m->exec($sql)){
            $this->setAlert("Registro removido com sucesso.", $this->_ctrl, Cylix_View::ALERT_OK);
        }else{
            $this->setAlert("Não foi possível remover o registro", $this->_ctrl, Cylix_View::ALERT_ERROR);
        }
        $this->redirecionar($this->_ctrl);
    }

    function actionIndex() {
        $t = $this->getModelo();
        $logado = $this->getLogado();
        $sql = $t->select();
        if ($logado->is_root != 1) {
            $sql->where('is_root <> ?', 1); //mocando os moderadores root caso o cara não seja root
        }
        $sql->orderBy('nome');
        $this->view->rows = $t->exec($sql);
    }
    
    public function actionSenha(){
        $mod = $this->getLogado();
        //post
        if($post = $this->getPost()){
            if($post['senha'] == $post['conf']){
                $m = $this->getModelo();
                $mod = $m->find($mod->id);
                $sql = $m->select()->where('id = ?',$mod->id);
                $row = first($m->exec($sql));
                $atual1 = $row->senha;
                $atual2 = md5($post['atual']);
                if($atual1 == $atual2){
                    //agora pode mudar
                    $sql = $m->update()
                            ->set(array('senha'=>md5($post['senha'])),false)
                            ->where('id = ?',$mod->id);
                    //notificação
                    $area = getUrl($this->_permalink.'/login');
                    $email = $row->email;
                    $nome = $row->nome;
                    $senha = $post['senha'];
                    $emailS = new Cylix_Email('Nova senha', "<p>
                        Prezado Sr(a) $nome, sua senha foi alterada<br/>
                        Área: $area <br/>
                        Login: $email <br/>
                        Senha: $senha
                    ");
                    $emailS->from(ConfigsModel::getValor('con_email_saida'), 'Sistema');
                    $emailS->to($email, $mod->nome);
                    if($emailS->send()){
                        $r = '<br/>Uma nova senha foi enviada ao seu e-mail';
                    }else{
                        $r = '';
                    }
                    if($m->exec($sql)){
                        $this->setAlert("Senha alterada com sucesso".$r, $this->_ctrl, Cylix_View::ALERT_OK);
                    }else{
                        $this->setAlert("Sua atual senha não foi alterada, favor tentar novamente", $this->_ctrl, Cylix_View::ALERT_WARNING);
                    }
                }else{
                    $this->setAlert("Sua atual senha não é esta!", $this->_ctrl, Cylix_View::ALERT_ERROR);
                }
            }else{
                $this->setAlert("A nova senha não está igual ao confirmar", $this->_ctrl, Cylix_View::ALERT_ERROR);
            }
        }
    }
    
    function getForm(){
        $form = new Cylix_Form();
        $form->radio('status', is_string($this->view->reg->status)?(int)$this->view->reg->status:(int)$this->getParam('status'), array('Bloqueado','Ativado'),true);
        $form->label('Status','status');
        $form->string('nome', is_string($this->view->reg->nome)?$this->view->reg->nome:$this->getParam('nome'), true, 3);
        $form->label('Nome', 'nome');
        $form->email('email', is_string($this->view->reg->email)?$this->view->reg->email:$this->getParam('email'));
        $form->label('E-mail', 'email');
        $form->radio('editor', is_string($this->view->reg->editor)?$this->view->reg->editor:$this->getParam('editor'), array('root'=>'Completo','basico'=>'Básico'));
        $form->label('Tipo de editor para conteúdos', 'editor');
        $form->radio('is_root', is_string($this->view->reg->is_root)?(int)$this->view->reg->is_root:(int)$this->getParam('is_root'), array('Comum','Root'));
        $form->label('Tipo', 'is_root');
        $form->submit('s', 'Salvar');
        $form->button('c', 'Voltar', 'button', null, array('onclick' => "window.location = '".  getBaseUrl($this->_permalink.'/'.$this->_ctrl)."';"));
        return $form;
    }
    
    private function _getACL(){
        $this->view->form = $this->getForm();
        $this->view->logado = $this->getLogado();
        $this->view->pIs_root = $this->getParam('is_root');
        include SYS_PATH.'/etc/acl/admin.php';
        $acl_conteudos = array();
        //todas os conteudos das areas
        $areas = AreasModel::me()->all('titulo');
        foreach($areas as $area){
            $acl_conteudos['paginas-'.$area->id] = array(
                'label' => $area->titulo,
                'permitir' => array(
                    'novo' => 'Criar conteúdos',
                    'publicar' => 'Publicar',
                    'index' => 'Ver',
                    'editar' => 'Editar',
                    'deletar' => 'Excluir'
                )
            );
        }
        $this->view->conteudos_perm = $acl_conteudos;
        $this->view->outras_perm = $acl_geral;
    }
    
    function actionNovo(){
        $this->_getACL();
        //post
        if($post = $this->getPost()){
            $senha = substr(md5(uniqid()),rand(10, 20),7);
            $post['senha'] = md5($senha);
            //permissões
            $p=array();
            if($post['acoes']){
                foreach ($post['acoes'] as $chave=>$acoes){
                    foreach ($acoes as $cada){
                        $p[$chave][$cada] = true;
                    }
                }
            }
            $post['permissoes'] = (count($p))?serialize($p):null;
            $m = $this->getModelo();
            if($m->uniq('email',$post['email'])){
                $this->setAlert("Este email já consta em nosso sistema.", $this->_ctrl, Cylix_View::ALERT_ERROR);
            }else{
                //salvando o registro
                if($m->create($post)){
                    $texto = "Moderador cadastrado com sucesso!";
                    //manda o usuário por email
                    $P = array('email'=>$post['email'],'senha'=>$senha,'dominio'=>  getUrl(PERMALINK_ADMIN.'/login'),'host'=>$_SERVER['HTTP_HOST']);
                    $template = Cylix_View::getTemplate('moderadores.html', $P);
                    $email = new Cylix_Mailer('Cadastro de Moderador', $template);
                    $email->from('sistema@'.$_SERVER['HTTP_HOST'], 'Administrador');
                    $email->to($post['email']);
                    $email->send();
                    $tipo = Cylix_View::ALERT_OK;
                    $this->setAlert($texto, $this->_ctrl, $tipo);
                    $this->redirecionar($this->_ctrl);
                }else{
                    $tipo = Cylix_View::ALERT_ERROR;
                    $texto = "Houve uma falha ao salvar o registro";
                    $this->setAlert($texto, $this->_ctrl, $tipo);
                }
            }
        }
    }
    function actionEditar(){
        //post
        if($post = $this->getPost()){
            //permissões
            $p=array();
            if($post['acoes']){
                foreach ($post['acoes'] as $chave=>$acoes){
                    foreach ($acoes as $cada){
                        $p[$chave][$cada] = true;
                    }
                }
            }
            
            $post['permissoes'] = (count($p))?serialize($p):null;
            $m = $this->getModelo();
            //verificando o email
            $sql = $m->select('id,email')
                    ->where('email = ?', $post['email'])
                    ->where('id NOT IN (?)', $this->getParam('id'));
            
            if(count($m->exec($sql)) > 0){
                $this->setAlert("Este email já consta em nosso sistema.", $this->_ctrl, Cylix_View::ALERT_ERROR);
            }else{
                //alterando o registro
                if($m->alter($this->getParam('id'), $post,false)){
                    $texto = "Moderador alterado com sucesso!";
                    $tipo = Cylix_View::ALERT_OK;
                }else{
                    $tipo = Cylix_View::ALERT_ERROR;
                    $texto = "Houve uma falha ao alterar o registro";
                }
                $this->setAlert($texto, $this->_ctrl, $tipo);
            }
        }
        
        $this->view->reg = $registro = $this->getModelo()->find($this->getParam('id'));
        $logado = $this->getLogado();
        if ($logado->is_root != 1 && $registro->is_root == '1') {
            //opa!
            $this->redirecionar('index');
        }
        
        $this->view->acoes = ($registro->permissoes)?unserialize(raw($registro->permissoes)):array();
        
        $this->_getACL();

    }

}

?>