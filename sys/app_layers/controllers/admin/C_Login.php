<?php

class Admin_LoginController extends Admin_AbstractController {
    /* protected $_aux_acl = array(
      'index'=>false,
      'sair'=>false,
      'redefinir-senha'=>false,
      ); */

    function actionIndex() {
        if ($post = $this->getPost()) {
            if ($row = ModeradoresModel::exists($post['login'], $post['senha'])) {
                if ($row->status == '1') {
                    $log = new stdClass();
                    $log->id = $row->id;
                    $log->email = $row->email;
                    $log->nome = $row->nome;
                    $log->status = $row->status;
                    $log->is_root = $row->is_root;
                    $log->editor = (strlen($row->editor) > 2) ? $row->editor : 'basico';
                    $log->permissoes = (strlen($row->permissoes) > 2) ? unserialize(raw($row->permissoes)) : array();
                    //ativado, guarda a sessão
                    $this->setLogado($log);
                    if ($this->isLogado()) {
                        $m = ModeradoresModel::me();
                        $m->alter($row->id, array('ultimo_ip'=>$_SERVER['REMOTE_ADDR']));
                        //soh pra ter certeza q session esta funcionando
                        $retorna = 'index';
                        if(parent::hasRetorno()){
                            $retorna = parent::getLoginReturnUrl();
                            parent::unsetLoginReturn();
                        }
                        $this->redirecionar($retorna); //vai pra index do admin
                    } else {
                        throw new Cylix_Exception('Recurso de session não está funcionando corretamente', 'erro $_SESSION');
                    }
                } else {
                    $this->setAlert('Seu usuário encontra-se desativado', $this->_ctrl, Cylix_View::ALERT_WARNING);
                }
            }else{
				$this->setAlert('Seu usuário não foi encontrado', $this->_ctrl, Cylix_View::ALERT_ERROR);
			}
        }
    }
    function actionSair(){
        $this->view->flag_render=false;
        $this->unsetLogado();
        $this->redirecionar('login');
    }
    
    function actionRedefinirSenha(){
        $this->view->flag_render = false;
        if($post = $this->getPost()){
            $senha = substr(md5(uniqid()), 1, 8);
            $email = $post['email'];
            $m = ModeradoresModel::me();
            $sql = $m->select('id,senha,nome')->where('email = ?',$email);
            $mod = $m->exec($sql);
            if($mod){
                first($mod);
                //mudar a senha
                $mod->senha = md5($senha);
                if($mod->save()){
                    $area = getUrl($this->_permalink.'/login');
                    $emailS = new Cylix_Mailer();
                    $emailS->subject('Nova senha');
                    $emailS->msg(utf8_decode("<p>Prezado Sr(a) {$mod->nome}, sua senha foi renovada<br/>
                        Área: $area <br/>
                        Login: $email <br/>
                        Senha: $senha"));
                    $emailS->from('observatorio@observatoriodoanalista.org', 'Sistema');
                    $emailS->to($email, $mod->nome);
                    if($emailS->send()){
                        $this->setAlert('Uma nova senha foi enviada ao seu e-mail', $this->_ctrl, Cylix_View::ALERT_INFO);
                    }else{
                        $this->setAlert('Sua senha foi alterada', $this->_ctrl, Cylix_View::ALERT_WARNING);
                    }
                }else{
                    $this->setAlert('Sua senha não foi alterada devido a problemas técnicos', $this->_ctrl, Cylix_View::ALERT_WARNING);
                }
            }else{
                $this->setAlert('E-mail não encontrado', $this->_ctrl, Cylix_View::ALERT_ERROR);
            }
        }
        $this->redirecionar('login');
    }

}

?>