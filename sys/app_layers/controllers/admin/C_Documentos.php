<?php

class Admin_DocumentosController extends Admin_AbstractController {

	protected $_scopes = array(
		'titulo' => array('title LIKE ?', '%$%', true),
		'doc_id' => array('doc_id = ?', '$')
	);
	private $_orderBy = array(
		'a-z' => 'title ASC,id DESC',
		'z-a' => 'title DESC,id DESC',
		'sa' => 'status ASC,id DESC',
		'sd' => 'status DESC,id DESC',
		'cri-a' => 'data ASC,id ASC'
	);

	function getModelo() {
		return DocumentosModel::me();
	}

	function actionIndex() {
        $doc_id = (int)$this->getParam('doc_id');
        if($doc_id > 0){
            //ajax
            $uri = 'http://api.yumpu.com/2.0/document.json?id='.$doc_id;//&return_fields=id,url,image,title,embed_code
            $ch = curl_init($uri);
            curl_setopt_array($ch, array(
                CURLOPT_HTTPHEADER  => array('X-ACCESS-TOKEN: '.ConfigsModel::getValor('cfg_yumpu_token')),
                CURLOPT_RETURNTRANSFER  =>true,
                CURLOPT_VERBOSE     => 1
            ));
            $out = curl_exec($ch);
            curl_close($ch);
            echo $out;
            $this->view->flag_render = false;
        }else{
            $t = $this->getModelo();
            $sql = $t->select();
            $o = $this->getParam('order');
            $criterios = 'status, data DESC'; //default
            if ($o && isset($this->_orderBy[$o])) {
                $criterios = $this->_orderBy[$o];
            }
            $this->applyEscopes($sql);
            $sql = $sql->orderBy($criterios);
            $this->view->filters = $this->getFilterScopes();
            $sql = $this->helper->navegacao()->paginador($sql, $this->view->pgr, $this->n_pagina);
            $this->view->rows = $t->exec($sql);
        }
		
	}

	function actionNovo() {
		if ($post = $this->getPost()) {
			$m = $this->getModelo();
            $post['embed_code'] = str_replace('"',"'",$post['embed_code']);
            $created = explode(' ',$post['create_date']);
            if(strlen($post['data']) > 1){
                dateToSQL('data', $post);
            }else{
                $post['data'] = $created[0];
            }
			$row = $m->newRow();
			$row->put($post);
			//crando
			if ($row->save()) {//$m->create($post)
				$this->setAlert("Registro criado com sucesso", $this->_ctrl, Cylix_View::ALERT_OK);
				$this->redirecionar($this->_ctrl);
			} else {
				$this->setAlert("Houve uma falha ao salvar o registro", $this->_ctrl, Cylix_View::ALERT_ERROR);
			}
		}
	}

	function actionEditar() {
		$id = $this->getParam('id');
		$m = $this->getModelo();
		$reg = $this->view->reg = $m->find($id);
		if ($post = $this->getPost()) {
			$created = explode(' ',$post['create_date']);
            $post['embed_code'] = str_replace('"',"'",$post['embed_code']);
            if(strlen($post['data']) > 1){
                dateToSQL('data', $post);
            }else{
                $post['data'] = $created[0];
            }
			$reg->put($post);
			//crando
			if ($reg->save()) {//$m->create($post)
				$this->setAlert("Registro alterado com sucesso", $this->_ctrl, Cylix_View::ALERT_OK);
			} else {
				$this->setAlert("Houve uma falha ao salvar o registro", $this->_ctrl, Cylix_View::ALERT_ERROR);
			}
		}
	}

}

?>