<?php

class Admin_BannersController extends Admin_AbstractController {

    function getModelo() {
        return BannersModel::me();
    }
    
    function actionDeletar() {
        //removendo a imagem
        $m = $this->getModelo();
        $reg = $m->find($this->getParam('id'));
        $m->unlinkFiles($reg->imagem);//apaga as fotos
        parent::actionDeletar('/index/banner-tipos/'.$this->getParam('banner-tipos'));
    }

    function actionIndex() {
        Cylix_App::setJS('prettyphoto', getBaseUrl('lib/prettyPhoto/prettyPhoto.js'));
        Cylix_App::setCSS('prettyphoto', getBaseUrl('lib/prettyPhoto/prettyPhoto.css'));
        
        $t = $this->getModelo();
        $tipo = $this->getParam('banner-tipos');
        $sql = $t->select()
                ->where('banner_tipos_id = ?', $tipo)
                ->orderBy('banner_tipos_id DESC,ordem,titulo');
        
        $sql = $this->helper->navegacao()->paginador($sql, $this->view->pgr, $this->n_pagina, array('banner-tipos'));
        
        $this->view->rows = $t->exec($sql);
    }
    
    private function _settings(){
        $this->view->tipo = BannerTiposModel::me()->find($this->getParam('banner-tipos'));
    }
    
    function actionNovo(){
        $this->_settings();
        if($post = $this->getPost()){
            $m = $this->getModelo();
            $post['banner_tipos_id'] = $banner_tipos = $this->getParam('banner-tipos');
            $row = $m->newRow();
            if($m->isValid($post)){
                $row->put($post);
                if($row->save()){
                    $this->setAlert("Banner criado com sucesso", 'banners', Cylix_View::ALERT_OK);
                    $this->redirecionar($this->_ctrl.'/index/banner-tipos/'.$banner_tipos);
                }else{
                    $this->setAlert("Houve uma falha ao salvar o registro", 'banners', Cylix_View::ALERT_ERROR);
                }
            }else{
                $this->setAlert("Seu formulário contém erros:<br/>".$m->getErrorValidation(), $this->_ctrl, Cylix_View::ALERT_ERROR);
            }
        }
    }
    
    function actionEditar(){
        $this->_settings();
        $id = $this->getParam('id');
        $banner_tipos = $this->getParam('banner-tipos');
        $m = $this->getModelo();
        $reg = $this->view->reg = $m->find($id);
        if($this->view->tipo != null && $reg != null){
            if($post = $this->getPost()){
                $post['banner_tipos_id'] = $banner_tipos;
                if($m->isValid($post)){
                    $reg->put($post);
                    if ($reg->save(false) !== false) {
                        $texto = "Registro alterado com sucesso!";
                        $tipo = Cylix_View::ALERT_OK;
                        $this->setAlert($texto, $this->_ctrl, $tipo);
                        $this->redirecionar($this->_ctrl.'/index/banner-tipos/'.$banner_tipos);
                    }else{
                        $tipo = Cylix_View::ALERT_ERROR;
                        $texto = "Houve uma falha ao alterar o registro";
                        $this->setAlert($texto, $this->_ctrl, $tipo);
                    }
                }else{
                    $this->setAlert("Seu formulário contém erros:<br/>".$m->getErrorValidation(), $this->_ctrl, Cylix_View::ALERT_ERROR);
                }
            }
        }else{
            $this->setAlert("Banner não encontrado", 'banners', Cylix_View::ALERT_ERROR);
            $this->redirecionar($this->_ctrl.'/index/banner-tipos/'.$banner_tipos);
        }
    }

}

?>