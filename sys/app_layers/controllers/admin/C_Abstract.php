<?php

class Admin_AbstractController extends Cylix_Controller {

    /**
	 * numero de registros por pagina
	 * @var int padrao = 30
	 */
	protected $n_pagina = 30;
    protected $_scopes = array();
    /**
     * array contendo as acls pra cada action do controller selecionado
     * @example array('acaoT1'=>'index', 'acaoT2'=>false) onde acaoT2 não passará por verificação
     * @var array 
     */
    protected $_aux_acl;
    
    public function beforeAction() {
        $this->_aux_acl = (isset($this->_aux_acl)) ? $this->_aux_acl : array();
        //procurar permissoes
        $livre = false;
        
        if(isset($this->_aux_acl[$this->_action])){
            $action = $this->_aux_acl[$this->_action];
            $livre = ($action == false) ? true : false;
            if(is_array($action)){
                $aux = $action;
                $action = $aux['action'];
                $_ctrl = $aux['ctrl'];
            }else{
                $_ctrl = $this->_ctrl;
            }
        }else{
            $action = $this->_action;
            $_ctrl = $this->_ctrl;
        }
        if($_ctrl != 'login'){
            //o restante precisa verificar se está logado
            if($this->isLogado()){
                $u = $this->getLogado();
                if($u->is_root != '1' && !$livre && $_ctrl != 'index'){
                    //usuario comum, terá q ver se tem autorização para tais controles e ações
                    $perm = $u->permissoes;
                    if($_ctrl == 'paginas'){//somente para os conteudos
                        //ex.: 'conteudo-1'=> array('index','publicar')
                        $id = $this->getParam('area');
                        if(!isset($perm["paginas-$id"][$action])){
							$this->setAlert('Acesso Negado', 'index', Cylix_View::ALERT_ERROR);
							$this->redirecionar('index');
                            #throw new Cylix_Exception('Usuário '.$u->nome.' tentando acessar: '.  "paginas-$id".'/'.$this->_action,'Acesso Negado');
                        }
                    }elseif(!isset($perm[$_ctrl][$action])){
						$this->setAlert('Acesso Negado', 'index', Cylix_View::ALERT_ERROR);
						$this->redirecionar('index');
                        #throw new Cylix_Exception('Usuário '.$u->nome.' tentando acessar: '.  $_ctrl.'/'.$this->_action,'Acesso Negado');
                    }
                    
                }//senao ele pode acessar tudo
                
                //titulo padrão
                $this->view->MetaTagTitle = "Sistema de Gerenciamento de Conteúdos";
                $this->view->MetaTagDesc = "";
                $this->view->MetaTagKeys = "";
            }else{
                $this->setAlert('Para acessar esta área precisamos de sua identificação', 'login', Cylix_View::ALERT_ERROR);
                $params = array();
                foreach($_GET as $k => $v){
                    $params[] = $k."=".urlencode($v);
                }
                if(count($params)){
                    $params='?'.  implode('&', $params);
                }else{
                    $params='';
                }
                $this->setLoginReturn($this->_ctrl.'/'.$this->_action.$params);
                $this->redirecionar('login');
            }
        }
    }
    
    public function afterAction() {
        $this->view->logado = $this->getLogado();
    }
    /**
     *
     * @return Cylix_Model 
     */
    function getModelo(){
        return AbstractModelo::eu();
    }
    /*
     * Ações Ver/Publicar/Alterar/Excluir/Criar
     */
    function actionIndex(){}
    function actionPublicar(){}
    function actionEditar(){}
    function actionDeletar($param='',$obj_id='id',$msg="Registro removido com sucesso"){
        $m = $this->getModelo();
        $id = $this->getParam($obj_id);
        $sql = $m->delete()->where($m->_pk.' = ?', $id);
        if($m->exec($sql)){
            $this->setAlert($msg, $this->_ctrl, Cylix_View::ALERT_OK);
        }else{
            $this->setAlert("Não foi possível remover o registro", $this->_ctrl, Cylix_View::ALERT_ERROR);
        }
        $this->redirecionar($this->_ctrl.$param);
    }
    function acaoNovo(){}
    
    public function getLogado(){
        return self::session();
    }
    public function isLogado(){
        return isset ($_SESSION['moderadores_' . FLOW]);
    }
    public function hasRetorno(){
        return isset ($_SESSION['login_return_' . FLOW]);
    }
    public function unsetLogado(){
        unset($_SESSION['moderadores_' . FLOW]);
    }
    public function setLogado($obj){
        $_SESSION['moderadores_' . FLOW] = $obj;
    }
    public function unsetLoginReturn(){
        unset($_SESSION['login_return_' . FLOW]);
    }
    public function setLoginReturn($url){
        $_SESSION['login_return_' . FLOW] = $url;
    }
    public function permissao($chave='index',$acao='index'){
        $u = $this->getLogado();
        $permissoes = $u->permissoes;
        //die("[$chave][$acao]");
        $is_root = $u->is_root;
        if($is_root == '1'){
            return true;
        }else{
            return isset($permissoes[$chave][$acao]);
        }
    }
    static function session(){
        return $_SESSION['moderadores_' . FLOW];
    }
    static function loginReturnUrl(){
        return $_SESSION['login_return_' . FLOW];
    }
    public function getLoginReturnUrl(){
        return self::loginReturnUrl();
    }
    /**
     * alias do redirecionar (somente para o flow)
     */
    function redirecionar($redirect) {
        parent::_redirect($this->_permalink.'/'.$redirect);
    }
    
    public function setAlert($texto,$chave='admin', $tipo=Cylix_View::ALERT_INFO) {
        $this->view->setAlert($texto, $chave, $tipo);
    }
    public function getAlert($chave='admin'){
        return $this->view->getAlert($chave);
    }
    
    
    public function createMailer($post,$to,$titulo=null,$from_email=null,$from_nome=null,$msg_key=null,$tpl=null){
        return MailerHelper::createMailer($this->view, $post, $to, $titulo, $from_email, $from_nome, $msg_key, $tpl, $this->_action);
    }
}

?>
