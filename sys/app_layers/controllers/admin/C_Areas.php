<?php
 class Admin_AreasController extends Admin_AbstractController{
     
     function getModelo() {
         return AreasModel::me();
     }
     
     function actionIndex(){
         $t = $this->getModelo();
         $sql = $t->select()->orderBy('titulo');
         $sql = $this->helper->navegacao()->paginador($sql, $this->view->pgr, $this->n_pagina);
         $this->view->rows = $t->exec($sql);
     }
     
     function actionNovo(){
        if($post = $this->getPost()){
            $m = $this->getModelo();
            //ajustando o seo title
            $post['seo_title'] = (strlen($post['seo_title']) > 1) ? $post['seo_title'] : $post['titulo'];
            //gerar ou não um permalink
            if($this->getParam('autolink') == '1'){
                $post['permalink'] = transPermalink($post['titulo']);
            }
            $row = $m->newRow();
            $row->put($post);
            //crando
            if($row->save()){//$m->create($post)
                $this->setAlert("Área criada com sucesso", 'areas', Cylix_View::ALERT_OK);
                $this->redirecionar($this->_ctrl);
            }else{
                $this->setAlert("Houve uma falha ao salvar o registro", 'areas', Cylix_View::ALERT_ERROR);
            }
        }
    }
    
    function actionEditar(){
        $id = $this->getParam('id');
        $m = $this->getModelo();
        $reg = $this->view->reg = $m->find($id);
        if($post = $this->getPost()){
            //ajustando o seo title
            $post['seo_title'] = (strlen($post['seo_title']) > 1) ? $post['seo_title'] : $post['titulo'];
            //gerar ou não um permalink
            if($this->getParam('autolink') == '1'){
                $post['permalink'] = transPermalink($post['titulo']);
            }
            $reg->put($post);
            //crando
            if($reg->save()){//$m->create($post)
                $this->setAlert("Área alterada com sucesso", 'areas', Cylix_View::ALERT_OK);
            }else{
                $this->setAlert("Houve uma falha ao salvar o registro", 'areas', Cylix_View::ALERT_ERROR);
            }
        }
    }
} 
?>