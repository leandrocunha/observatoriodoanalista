<?php
 class Site_FormulariosController extends Site_AbstractController{
	 
	 function actionContato(){
		 $this->addSeoTitle('Contato');
		 if ($post = $this->getPost()) {
            if (Cylix_Form::requiredFields(array('nome', 'email', 'msg'))) {
                $verif_c = Cylix_Form::getCaptcha();

                if ($verif_c == strtolower($post['captcha'])) {
                    $to = ConfigsModel::getValor('cfg_contato');
                    if ($this->createMailer($post, $to, null, $post['email'], $post['nome'], 'msg')) {
                        $this->setAlert('<b>Contato enviado com sucesso!</b>', $this->_ctrl, Cylix_View::ALERT_OK);
                    } else {
                        $this->setAlert('<b>Não foi possível enviar sua mensagem.</b> Favor tentar novamente em outro instante.', $this->_ctrl, Cylix_View::ALERT_WARNING);
                    }
                } else {
                    $this->setAlert('Informe o <b>código</b> novamente.', $this->_ctrl, Cylix_View::ALERT_ERROR);
                }
            } else {
                $this->setAlert('Os campos <b>Nome</b>, <b>E-mail</b> e <b>Mensagem</b> são obrigatórios.', $this->_ctrl, Cylix_View::ALERT_ERROR);
            }
        }
	 }
	 
	 function actionCadastreSe(){
		 $this->addSeoTitle('Cadastre-se');
		 $this->view->enviado=false;
		 if ($post = $this->getPost()) {
            if (Cylix_Form::requiredFields(array('nome', 'email', 'cidade','estado'))) {
                $verif_c = Cylix_Form::getCaptcha();

                if ($verif_c == strtolower($post['captcha'])) {
					//testando
					$m = InscricoesModel::me();
					$cadastro = $m->newRow();
					$cadastro->put($post);
					if($m->isValid($post)){
						if ($cadastro->save()) {
							$this->view->enviado=true;
							$this->setAlert(ConfigsModel::getValor('cad_texto_final'), $this->_ctrl, Cylix_View::ALERT_OK);
						} else {
							$this->setAlert('<b>Não foi possível enviar seu cadastro.</b>', $this->_ctrl, Cylix_View::ALERT_WARNING);
						}
					}else{
						$this->setAlert('<b>Não foi possível enviar seu cadastro.</b><br/>'.$m->getErrorValidation(), $this->_ctrl, Cylix_View::ALERT_WARNING);
					}
                } else {
                    $this->setAlert('Informe o <b>código</b> novamente.', $this->_ctrl, Cylix_View::ALERT_ERROR);
                }
            } else {
                $this->setAlert('<b>Todos os campos são obrigatórios.</b> Favor preenchê-los', $this->_ctrl, Cylix_View::ALERT_ERROR);
            }
        }
	 }
} 
?>