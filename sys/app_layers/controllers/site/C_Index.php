<?php

class Site_IndexController extends Site_AbstractController {

	protected $_scopes = array(
		'data' => array("DATE_FORMAT(DATE(data),'%m-%Y') = ?", '$', false),
		'datav' => array("DATE_FORMAT(DATE(criacao),'%m-%Y') = ?", '$', false),
		'datad' => array("DATE_FORMAT(DATE(data),'%m-%Y') = ?", '$', false)
	);

	function actionIndex() {
		parent::actionIndex();
	}

	function actionPaginas() {

		$pg = $this->view->pagina = PaginasModel::me()->find($this->getParam('id'));

		if ($pg && ($pg->status == '1') || isset($_GET['flag_status'])) {
			$area = $this->view->area = $pg->area();

			$context = (strlen($area->contexto) > 1) ? $area->contexto : 'index';
			$this->view->setContext($context);
			if ($pg->permalink == 'quem-somos') {
				$this->view->setContext('quem-somos');
			}

			if ($aux = $pg->seo_title) {
				$this->addSeoTitle($aux);
			} else {
				$this->addSeoTitle($pg->titulo);
			}
			if ($aux = $pg->seo_keywords) {
				$this->view->content_seo_keywords = $aux;
			}
			if ($post = $this->getPost()) {
				if (Cylix_Form::requiredFields(array('nome', 'email'))) {
					$verif_c = Cylix_Form::getCaptcha();
					$post['paginas_id'] = $pg->id;
					if ($verif_c == strtolower($post['captcha'])||true) {
						//testando
						$m = ComentariosModel::me();
						$cadastro = $m->newRow();
						$cadastro->put($post);
						if($m->isValid($post)){
							if ($cadastro->save()) {
								$this->setAlert('Comentário enviado com sucesso e aguardando aprovação', $this->_ctrl, Cylix_View::ALERT_OK);
							} else {
								$this->setAlert('<b>Não foi possível enviar seu cadastro.</b>', $this->_ctrl, Cylix_View::ALERT_WARNING);
							}
						}else{
							$this->setAlert('<b>Não foi possível enviar seu cadastro.</b><br/>'.$m->getErrorValidation(), $this->_ctrl, Cylix_View::ALERT_WARNING);
						}
					} else {
						$this->setAlert('Informe o <b>código</b> novamente.', $this->_ctrl, Cylix_View::ALERT_ERROR);
					}
				} else {
					$this->setAlert('<b>Todos os campos são obrigatórios.</b> Favor preenchê-los', $this->_ctrl, Cylix_View::ALERT_ERROR);
				}
			}
		} else {
			$this->redirecionar('404.html');
		}
	}

	function actionAreas() {
		$id = (int) $this->getParam('id');
		if ($id > 0) {
			$m = AreasModel::me();
			$area = $this->view->area = $m->find($id);
			if ($area && ($area->status == '1') || isset($_GET['flag_status'])) {
				$context = (strlen($area->contexto) > 1) ? $area->contexto : 'index';
				$this->view->setContext($context);

				if ($aux = $area->seo_title) {
					$this->addSeoTitle($aux);
				} else {
					$this->addSeoTitle($area->titulo);
				}
				if ($aux = $area->seo_keywords) {
					$this->view->content_seo_keywords = $aux;
				}
				$this->view->content_seo_description = substr($area->descricao, 0, 255);
				//paginas
				$m_pg = PaginasModel::me();
				$sql = $m_pg->select()->where('areas_id = ? AND status = 1', $area->id);
				$this->applyEscopes($sql);
				$sql = $sql->orderBy('data DESC, id DESC');
				//$this->view->filters = $this->getFilterScopes();
				switch ($area->contexto) {
					case 'fotos': $n_pagina = 9;
						break;
					default : $n_pagina = 3;
						break;
				}

				$sql = $this->helper->navegacao()->paginador($sql, $this->view->pgr, $n_pagina);
				$this->view->rows = $m_pg->exec($sql);
			} else {
				$this->redirecionar('404.html');
			}
		} else {
			$this->redirecionar('500.html');
		}
	}
    
    function actionDocumentos() {
		$m = DocumentosModel::me();
		$sql = $m->select()->where('status = 1')->orderBy('data DESC,id DESC');

		$this->addSeoTitle('Revista');
		
		$this->applyEscopes($sql,false);

		$sql = $this->helper->navegacao()->paginador($sql, $this->view->pgr, 12);
		$this->view->rows = $m->exec($sql);
	}

	function actionVideos() {
		$m = VideosModel::me();
		$sql = $m->select()->where('status = 1')->orderBy('criacao DESC,id DESC');

		$this->addSeoTitle('Vídeos');
		
		$this->applyEscopes($sql,false);

		$sql = $this->helper->navegacao()->paginador($sql, $this->view->pgr, 9);
		$this->view->rows = $m->exec($sql);
	}
    
    function actionBuscar(){
        $q = trim($this->getParam('q'));
        if(strlen($q) > 2){
            $termos = explode(' ', $q);
            $or = array();
            $model = PaginasModel::me();
            $sql = $model->select()->where('status = 1 AND areas_id <> 3');
            foreach($termos as $tr){
                if(strlen($tr) > 2){
                    $or[] = "titulo LIKE '%$tr%' OR seo_keywords LIKE '%$tr%' OR descricao LIKE '%$tr%'";
                }
            }
            if(count($or)){
                foreach($or as $w){
                    $sql->where($w);
                }
                $sql->orderBy('data DESC, id DESC');
                $sql = $this->helper->navegacao()->paginador($sql, $this->view->pgr, 4, array('q'));
                $this->view->rows = $model->exec($sql);
            }else{
                $this->redirecionar('/');
            }
        }else{
            $this->redirecionar('/');
        }
    }
	
}