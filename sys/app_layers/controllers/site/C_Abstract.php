<?php

class Site_AbstractController extends Cylix_Controller {

    protected $n_pagina = 30;
    protected $_scopes = array();
    protected $_cache_name_termos = 'aceita_termos';


    public function beforeAction() {
        //titulo padrão
        $this->view->content_seo_title = ConfigsModel::getValor('seo_title');
        $this->view->content_seo_description = ConfigsModel::getValor('seo_description');
		$this->view->content_seo_keywords = ConfigsModel::getValor('seo_keywords');
    }
    function actionIndex(){}
    public function afterAction() {}
    
    
    
    /**
     * alias do redirecionar (somente para o flow)
     */
    function redirecionar($redirect) {
        parent::_redirect($this->_permalink.'/'.$redirect);
    }
    
    public function setAlert($texto,$chave='site', $tipo=Cylix_View::ALERT_INFO) {
        $this->view->setAlert('<button data-dismiss="alert" class="close" type="button">×</button>'.$texto, $chave, $tipo);
    }
    public function getAlert($chave='site'){
        return $this->view->getAlert($chave);
    }
    
    
    public function addSeoTitle($txt){
        if(strlen($txt))
            $this->view->content_seo_title = $txt . ' - '.$this->view->content_seo_title;
    }
    
    public function createMailer($post,$to,$titulo=null,$from_email=null,$from_nome=null,$msg_key=null,$tpl=null){
        return MailerHelper::createMailer($this->view, $post, $to, $titulo, $from_email, $from_nome, $msg_key, $tpl, $this->_action);
    }
    
    

}