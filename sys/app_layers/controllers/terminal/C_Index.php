<?php
/**
 * será a tela do terminal e realização de comandos sem necessidade de senha
 */
class Terminal_IndexController extends Terminal_AbstractController{

    private $_params;
    
    public function beforeAction() {
        if($this->_action != 'index'){
            $this->_params = $this->getParam('params');
        }
        parent::beforeAction();
    }
    
    public function  actionIndex() {
    }
    
    public function actionLogin(){
        if(ENV == 'online'){
			$file = 'http://ferramentasparaweb.com.br/seriais/'.SERIAL;
		}else{
			$file = INDEX_PATH.'../'.SERIAL;
		}
        $pass = @file_get_contents($file);
        if($pass){
            if(strlen($pass)>3){
                if($pass == trim($this->_params)){
                    $_SESSION['terminal'] = date('Ymd');
                    die('#login-ok#');//wildcard
                }else{
                    die('Chave incorreta.');
                }
            }else{
                die('Dados remotos não encontrados.');
            }
            
        }else{
            die('Dados inconsistentes.');
        }
    }
    
    public function actionLogout(){
        unset($_SESSION['terminal']);
        die('Identificação removida.');
    }
    
    public function actionLock(){
        if(Cylix_App::lockSite()){
            die('Site bloqueado.');
        }else{
            die('A flag já existe');
        }
    }
    
    public function actionHelp(){
        $path = SYS_PATH.'etc/help';
        $file = 'terminal.html';//$this->_params.'.html';
        $help = @file_get_contents("$path/$file") or die('Impossível ler '.$file);
        die($help);
    }
    
}