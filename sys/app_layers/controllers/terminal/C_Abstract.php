<?php

class Terminal_AbstractController extends Cylix_Controller {

    public function beforeAction() {
        //titulo padrão
        $this->view->MetaTagTitle = "Terminal de Comandos";
    }
    /**
     * alias do redirecionar (somente para o flow)
     */
    function redirecionar($redirect) {
        parent::_redirect($this->_mod.'/'.$redirect);
    }
}
