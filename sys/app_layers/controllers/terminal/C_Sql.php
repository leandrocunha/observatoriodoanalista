<?php

class Terminal_SqlController extends Terminal_AbstractController {

    private $_params;

    public function beforeAction() {
        $this->_params = trim($this->getParam('params'));
        if (isset($_SESSION['terminal'])) {
            $this->view->yield = "Comando realizado com sucesso."; //retorno ajax padrão
        } else {
            die('Acesso negado. Favor se identificar.');
        }
    }
    /*
     * sql create table-name
     */
    function actionCreate() {
        $nome = str_replace(array(' ','_'), '-', $this->_params);
        if(strlen($nome) > 2){
            $path = SYS_PATH.'db/sql/';
            $mt = explode(' ',microtime());
            $arq = date('Y').$mt[1].'_'.$nome.'.sql';
            $fh = fopen($path.$arq, 'w') or die("impossível obter $arq");
            fwrite($fh, '/* SQL '.$nome.' criado em '.date('d/m/Y H:i:s')." */\n") or die("impossível escrever em $arq");
            fclose($fh);
            die("arquivo $arq criado com sucesso.");
        }else{
            die('nome de arquivo muito pequeno.');
        }
    }

    /*
     * os primeiro parametros devem ser numeros das versões
     * caso precise referenciar outro banco que não seja o deafult, passa por ultimo
     * sql run {version1 version2 etc...} [config]
     * 
     * ex:
     * sql run 20122001015728
     * sql run 20122001015728 20122001015729 20122001015730
     * sql run 20122001015728 default
     * sql run 20122001015728 sistema-externo
     */
    function actionRun() {
        $params = explode(' ', $this->_params);
        if(count($params) >= 1){
            $last = end($params);
            if($last > 99999999999){
                //nao foi passado o cfg
                $cfg_db = 'default';
            }else{
                $cfg_db = array_pop($params);
                $cfg_db = ($cfg_db) ? $cfg_db : 'default';
            }
            if(count($params) >= 1){
                $path = SYS_PATH.'db/sql';
                $sqls = _listDir($path, 'is_file', 'sql');
                //passando em cada arquivo sql
                foreach($sqls as $sql_file){
                    $aux = explode('_', $sql_file);
                    //20122001015728_marcas.sql deve ficar somente o 20122001015728 para ser a versão
                    $versao = array_shift($aux);
                    if(in_array($versao, $params)){
                        //se está então eh pra rodar ela
                        $_sql = @file_get_contents("$path/$sql_file") or die('Imposs�vel ler '.$sql_file);
                        $aux = explode(';',$_sql);
                        $linhas = array();
                        foreach($aux as $linha){
                            $linhas[] = trim(preg_replace('/\/\*.+?\*\//', '', str_replace("\n", '', $linha)));//tirando comentarios e quebras de linhas
                        }
                        if(strlen($_sql) > 10){
                            $DB = new Cylix_DataBase($cfg_db);
                            //hora de rodar ele
                            try{
                                $DB->start();
                                foreach($linhas as $linha){
                                    if(strlen($linha) > 10){
                                        $res = $DB->exec($linha, 'abstract', true);
                                        if($res === null || $res === false){
											$DB->rollback();
                                            die('Falha grave ao executar '.$sql_file.'<br/><small>'.$linha.'</small>');
                                        }
                                    }
                                }
                                $DB->commit();
                                //informar q foi executada
                                $u = umask(0);
                                $f_db = SYS_PATH.'tmp/terminal/db_last_sql';
								if(!file_exists($f_db)){
									@fputs($f_db, '?', '1');
								}
                                $fh = fopen($f_db, 'w') or die("impossível obter $f_db");
                                @fwrite($fh, $versao) or die("acesso negado para $f_db");
                                fclose($fh);
                                umask($u);
                            }catch(Cylix_Exception $e){
                                $DB->rollback();
                                die($e);
                            }
                        }else{
                            die($sql_file.' em branco');
                        }
                    }
                }
            }else{
                die('Falta parâmetros de quais versões');
            }
        }else{
            die('Falta parâmetros');
        }
    }

    /*
     * sql status
     */
    function actionStatus() {
        $path = SYS_PATH.'db/sql';
        $sqls = _listDir($path, 'is_file', 'sql');
        $linhas = array();
        $f_db = SYS_PATH.'tmp/terminal/db_last_sql';
        if(file_exists($f_db)){
			if(filesize($f_db)==0){@file_put_contents($f_db, '?');}
            $last_sql = @file_get_contents($f_db) or die('Impossível ler '.$f_db);
        }else{
            $last_sql = '???';
        }
        foreach($sqls as $f){
            $linha = str_replace(array('.sql','_'), array('',' -&gt; '), $f);
            $linhas[] = $linha;
        }
        sort($linhas);
        foreach($linhas as $i => $li){
            $linhas[$i] = (strpos($li, trim($last_sql)) === 0) ? "<b>$li</b> &laquo;" : $li;
        }
        die(implode('<br/>', $linhas));
    }
    
    /*
     * sql set-status 000000000
     */
    function actionSetStatus(){
        $u = umask(0);
        $f_db = SYS_PATH.'tmp/terminal/db_last_sql';
        $fh = fopen($f_db, 'w') or die("impossível obter $f_db");
        @fwrite($fh, $this->_params) or die("acesso negado para $f_db");
        fclose($fh);
        umask($u);
    }
    
    function afterAction() {
        die('Comando realizado com sucesso');//caso skeco d colocar um die nos caras acima
    }

}
