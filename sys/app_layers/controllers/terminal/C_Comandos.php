<?php

class Terminal_ComandosController extends Terminal_AbstractController {

    private $_params;

    public function beforeAction() {
        $this->_params = trim($this->getParam('params'));
        if (isset($_SESSION['terminal'])) {
            $this->view->yield = "Comando realizado com sucesso."; //retorno ajax padrão
        } else {
            die('Acesso negado. Favor se identificar.');
        }
    }
    
    public function actionTouch(){
        $params = explode(' ',$this->_params);
        if($params[0]=='sass'){
            //toca os arquivos principais nao incluidos
            $flow = $params[1];
            if($flow){
                $diretorio = SYS_PATH.'stylesheets/'.$params[0].'/'.$flow;
                $dir = _listDir($diretorio);
                echo "touching: ";
                foreach($dir as $d){
                    if(strpos($d, '_')!==0){
                        touch("$diretorio/$d");//toca eles
                        echo $d.' | ';
                    }
                }
            }
        }else{
            //faz algo diferente neh
        }
        die('#');
    }

    public function actionLogout() {
        unset($_SESSION['terminal']);
        die('Identificação removida.');
    }

    /**
     * unlock 1/N - desblokea
     * unlock 0 - blokeia
     */
    public function actionUnlock() {
        if ($this->_params != '0') {
            $flag = SYS_PATH . 'tmp/terminal/maintenance';
            if (file_exists($flag)) {
                $u = umask(0);
                @unlink($flag);
                umask($u);
                die('Site desbloqueado.');
            } else {
                die('Flag inexistente.');
            }
        } else {
            if (Cylix_App::lockSite()) {
                die('Site bloqueado.');
            } else {
                die('A flag já existe');
            }
        }
    }

    /**
     * executa chmod em todas as pasta por meio do parametro ' * '
     * ou
     * executa chmod em uma pasta especifica a partir da www
     */
    function actionChmod() {
        $u = umask(0);
        $ret = false;
        if ($this->_params == '*') {
            #####
            # maratona de pastas q devem ter 0777
            #####
            //sass
            $p = SYS_PATH . 'lib/PHPCSSParser/sass/sass-cache';
            chmod($p, 0777) or die('chmod failure: ' . $p);
            //tmp
            $tmp_path = SYS_PATH . 'tmp';
            $dir = _listDir($tmp_path, 'is_dir');
            foreach ($dir as $p) {
                $p = "$tmp_path/$p";
                chmod($p, 0777) or die('chmod failure: ' . $p);
            }
            //uploads
            $up = INDEX_PATH . 'uploads';
            chmod($up, 0777) or die('chmod failure: ' . $up);
            $dir = _listDir($up, 'is_dir');
            foreach ($dir as $p) {
                $p = "$up/$p";
                chmod($p, 0777) or die('chmod failure: ' . $p);
            }
            //email (caso seja linux)
            $p = INDEX_PATH . 'email';
            chmod($p, 0777) or die('chmod failure: ' . $p);
            //tudo bem
            $ret = true;
        } else {
            //se vira pra colocar no que vier no parametro
            chmod($this->_params, 0777);
        }
        umask($u);
        $die = ($ret) ? 'chmod para "' . $this->_params . '" realizado com sucesso' : 'Falha na execução';
        die($die);
    }

    /*
     * clear-tmp sass view etc...
     */

    function actionClearTmp() {
        $tmp_path = SYS_PATH . 'tmp';
        $ret = $this->_clearPaths($tmp_path);
        die($ret);
    }

    /*
     * clear-uploads midias banners etc...
     */

    function actionClearUploads() {
        $up_path = INDEX_PATH . 'uploads';
        $ret = $this->_clearPaths($up_path);
        die($ret);
    }

    /*
     * create php:
     * - controller flow class-name
     * - model table|row class-name
     * - skin skin-name
     */

    function actionCreate() {
        $params = explode(' ', $this->_params);
        $aux = '';
        if (count($params) > 1) {
            //1º param é o tipo de criação
            $tipo = array_shift($params);
            //controller
            if ($tipo == 'controller') {
                if (count($params) > 1) {
                    $flow = $params[0];
                    $classe = $params[1];
                    $path = SYS_PATH . 'app_layers/controllers/' . $flow;
                    if(!is_dir($path)){
                        mkdir($path);
                    }
                    //arquivo C_Abstract.php
                    $arq = 'C_' . camelcase($classe) . '.php';
                    $fh = fopen("$path/$arq", 'w') or die("impossível obter $arq");
                    fwrite($fh, "<?php\n class " . camelcase($flow) . "_" . camelcase($classe) . "Controller extends " . camelcase($flow) . "_AbstractController{\n} \n?>");
                    fclose($fh);
                    $aux = '<br/>' . $tipo . '-&gt;' . $classe;
                } else {
                    die('parâmetros insuficientes para o tipo ' . $tipo);
                }
            }
            if ($tipo == 'model') {
                if (count($params) > 1) {
                    $nivel = $params[0];
                    $classe = $params[1];
                    $path = SYS_PATH . 'app_layers/models/' . $nivel;
                    //arquivo M_Abstract.php ou M_Abstract_Row.php
                    $class_name = camelcase($classe);
                    if ($nivel == 'row') {
                        $arq = 'M_' . $class_name . '_Row.php';
                        $str = "<?php\nclass " . $class_name . "Model_Row extends AbstractModel_Row {\n    /**\n    * retorna o Model correspondente\n    * @return " . $class_name . "Model \n    */\n    function getModel(){\n        return " . $class_name . "Model::me();\n    }\n}\n?>";
                    } else {
                        $arq = 'M_' . $class_name . '.php';
                        $str = "<?php\nclass " . $class_name . "Model extends AbstractModel {\n    \n    /**\n     * retorna a instancia da classe pra economizar memoria\n     * @return " . $class_name . "Model\n     */\n    public static function me(){\n        \$table = '" . str_replace('-', '_', $classe) . "';\n        if(!isset(self::\$_instance[\$table])){\n            self::\$_instance[\$table] = new self(\$table);\n        }\n        return self::\$_instance[\$table];\n    }\n}\n?>";
                    }
                    $fh = fopen("$path/$arq", 'w') or die("impossível obter $arq");
                    fwrite($fh, $str);
                    fclose($fh);
                    $aux = '<br/>' . $tipo . '-&gt;' . $classe;
                } else {
                    die('parâmetros insuficientes para o tipo ' . $tipo);
                }
            }
            if ($tipo == 'skin') {
                $nome = $params[0];
                $pastas = array(
                    SYS_PATH . "app_layers/views/$nome/actions",
                    INDEX_PATH . "themes/$nome/css",
                    INDEX_PATH . "themes/$nome/img",
                    INDEX_PATH . "themes/$nome/js"
                );
                foreach ($pastas as $p) {
                    mkdir($p, 0755, true) or die('acesso negado para criar ' . $p);
                }
                $aux = '<br/>' . $tipo . '-&gt;' . $nome;
            }
            die("Comando realizado com sucesso." . $aux);
        } else {
            die('parâmetros insuficientes.');
        }
    }

    /*
     * ############
     * 
     *      auxiliares
     * 
     * ##########
     */

    private function _clearPaths($path) {
        $u = umask(0);
        $params = explode(' ', $this->_params);
        $ret = "Comando realizado com sucesso nas pastas tmp: " . $this->_params;
        $erro = array(); //pastas erradas
        foreach ($params as $param) {
            if (strlen($param) > 1) {
                //pasta especifica
                $pasta = "$path/$param";
                if (!is_dir($pasta)) {
                    $erro[] = $param;
                } else {
                    $dir = _listDir($pasta);
                    foreach ($dir as $file) {
                        if ($file != 'index.htm') {
                            $file = "$pasta/$file";
                            unlink($file) or die('Falha ao excluir ' . $file);
                        }
                    }
                }
            } else {
                die('pasta inválida: ' . $param);
            }
        }
        umask($u);
        if (count($erro) > 0) {
            $ret = "Comando realizado com sucesso, porém houveram problemas em: " . implode(', ', $erro);
        }
        return($ret);
    }
    
    function actionEmailNotify(){
        $flag = SYS_PATH . 'tmp/terminal/no-notify';
        if(strtolower($this->_params) == 'on'){
            touch($flag);
            $ret = 'touched flag';
        }else{
            @unlink($flag);
            $ret = 'removed flag';
        }
        die($ret);
    }

}
