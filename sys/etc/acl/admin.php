<?php

/*
 * Por padrão as as permissões para conteudos(listagem das áreas) serão buscadas primeiro
 * os demais seguirão este padrão abaixo
 */
$acl_geral = array(
    'banners' => array(
        'label' => 'Banners',
        'sub-item' => 'banner-tipos',
        'visivel' => true,
        'permitir' => array(
            'index' => 'Ver',
            'editar' => 'Alterar',
            'novo' => 'Criar',
            'deletar' => 'Excluir'
        )
    ),
    'midias' => array(
        'label' => 'Mídias dos Conteúdos',
        'visivel' => false,
        'permitir' => array(
            'index' => 'Ver',
            'novo' => 'Upload',
            'editar' => 'Alterar',
            'deletar' => 'Excluir'
        )
    ),
    'videos' => array(
        'label' => 'Vídeos',
        'visivel' => true,
        'permitir' => array(
            'index' => 'Ver',
            'editar' => 'Alterar',
            'novo' => 'Criar',
            'deletar' => 'Excluir'
        )
    ),
    'documentos' => array(
        'label' => 'Revistas',
        'visivel' => true,
        'permitir' => array(
            'index' => 'Ver',
            'editar' => 'Alterar',
            'novo' => 'Criar',
            'deletar' => 'Excluir'
        )
    ),
    'comentarios' => array(
        'label' => 'Comentários',
        'visivel' => true,
        'permitir' => array(
            'index' => 'Ver',
            'editar' => 'Alterar',
            'deletar' => 'Excluir'
        )
    ),
    'inscricoes' => array(
        'label' => 'Download de Inscrições',
        'visivel' => true,
		#'url' => 'inscricoes/exportar',
        'permitir' => array(
            'index' => 'Exportar'
        )
    ),
    'avancado' => array(
        'label' => 'Avançado',
        'visivel' => true,
        'folder' => array(
            'areas' => array(
                'label' => 'Áreas dos Conteúdos',
                'visivel' => true,
                'permitir' => array(
                    'index' => 'Ver',
                    'editar' => 'Alterar'
                )
            ),
//            'menus' => array(
//                'label' => 'Menu do site',
//                'visivel' => true,
//                'permitir' => array(
//                    'index' => 'Ver',
//                 'novo' => 'Criar Itens',
//                  'editar' => 'Editar',
//                  'deletar' => 'Deletar'
//                )
//            ),
            'moderadores' => array(
                'label' => 'Moderadores',
                'visivel' => true,
                'permitir' => array(
                    'index' => 'Ver',
                    'editar' => 'Alterar',
                    'novo' => 'Criar',
                    'deletar' => 'Excluir'
                )
            ),
            'cache' => array(
                'label' => 'Cache',
                'visivel' => true,
                'url' => 'index/cache',
                'permitir' => array(
                    'all' => 'Limpeza'
                )
            )
        )
    ),
    'configs' => array(
        'label' => 'Configurações',
        'sub-item' => 'config-grupos',
        'visivel' => true,
        'permitir' => array(
            'index' => 'Alterar'
        )
    )
);
?>
