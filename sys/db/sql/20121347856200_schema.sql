/* SQL schema criado em 01/06/2012 02:26:03 */
DROP TABLE IF EXISTS `areas`;

CREATE TABLE `areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `contexto` varchar(255) DEFAULT 'index' COMMENT 'ex.: contexto = teste a view de contato seria algo como contato_teste.phtml',
  `status` int(11) NOT NULL DEFAULT '0',
  `permalink` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) NOT NULL DEFAULT 'Área',
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `back_link` int(1) NOT NULL DEFAULT '0' COMMENT '1 = os filhos terão o permalink da área. Ex.: area/pagina-teste',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `banners` */

DROP TABLE IF EXISTS `banners`;

CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `banner_tipos_id` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `target` varchar(255) NOT NULL DEFAULT '_self',
  `status` int(1) NOT NULL DEFAULT '0',
  `ordem` int(11) NOT NULL DEFAULT '99' COMMENT 'quanto menor primeiro fica',
  PRIMARY KEY (`id`),
  KEY `FK_banners_idx` (`banner_tipos_id`),
  CONSTRAINT `FK_banners` FOREIGN KEY (`banner_tipos_id`) REFERENCES `banner_tipos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `banner_tipos` */

DROP TABLE IF EXISTS `banner_tipos`;

CREATE TABLE `banner_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chave` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `largura` int(11) NOT NULL DEFAULT '300',
  `altura` int(11) NOT NULL DEFAULT '300',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `comentarios` */

DROP TABLE IF EXISTS `comentarios`;

CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `criacao` datetime NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mensagem` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `paginas_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


/*Table structure for table `conteudos` */

DROP TABLE IF EXISTS `paginas`;

CREATE TABLE `paginas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `conteudo` text,
  `areas_id` int(11) NOT NULL COMMENT 'pra formar contextos diferentes para cada tipo de área',
  `status` int(1) NOT NULL DEFAULT '0',
  `cadastro` datetime NOT NULL,
  `alteracao` datetime DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) NOT NULL DEFAULT 'Titulo',
  `seo_keywords` varchar(255) DEFAULT NULL,
  `permalink` varchar(255) DEFAULT NULL,
  `capa` varchar(255) DEFAULT NULL,
  `ordem` tinyint(3) DEFAULT '99',
  `data` date DEFAULT NULL,
  `proteger` int(1) NOT NULL DEFAULT '0' COMMENT 'eliminar o botao excluir',
  PRIMARY KEY (`id`),
  KEY `FK_paginas_idx` (`areas_id`),
  CONSTRAINT `FK_paginas` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `contextos` */

DROP TABLE IF EXISTS `contextos`;

CREATE TABLE `contextos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chave` varchar(255) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `config_grupos` */

DROP TABLE IF EXISTS `config_grupos`;

CREATE TABLE `config_grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '0-oculto,1-visivel no admin',
  `root` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-somente root vai ver',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `config` */

DROP TABLE IF EXISTS `config`;

CREATE TABLE `configs` (
  `chave` varchar(255) NOT NULL,
  `valor` text,
  `tipo` varchar(255) NOT NULL DEFAULT 'string' COMMENT 'int,string,text,html,file,boolean,select,radio ou hidden',
  `descricao` varchar(255) DEFAULT NULL COMMENT 'descrição pra área admin',
  `options` varchar(255) DEFAULT NULL COMMENT 'opções personalizadas ex: array(0=>Não,1=>Sim) pro tipo select ou [comum] pro tipo html',
  `config_grupos_id` int(11) NOT NULL,
  `ordem` tinyint(4) NOT NULL DEFAULT '99',
  `help` text COMMENT 'texto livre para ajudar usuários',
  PRIMARY KEY (`chave`),
  KEY `FK_config` (`config_grupos_id`),
  CONSTRAINT `FK_configs` FOREIGN KEY (`config_grupos_id`) REFERENCES `config_grupos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `target` varchar(255) DEFAULT NULL,
  `classe` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `menu_id` int(11) DEFAULT NULL,
  `ordem` tinyint(3) NOT NULL DEFAULT '99',
  `chave` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `midias` */

DROP TABLE IF EXISTS `midias`;

CREATE TABLE `midias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caminho` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `cadastro` datetime NOT NULL,
  `descricao` text,
  `paginas_id` int(11) NOT NULL,
  `ordem` tinyint(3) NOT NULL DEFAULT '99',
  PRIMARY KEY (`id`),
  KEY `FK_midias_idx` (`paginas_id`),
  CONSTRAINT `FK_midias` FOREIGN KEY (`paginas_id`) REFERENCES `paginas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8

/*Table structure for table `moderadores` */

DROP TABLE IF EXISTS `moderadores`;

CREATE TABLE `moderadores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT 'Moderador',
  `email` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `is_root` int(1) NOT NULL DEFAULT '0' COMMENT 'usuarios root com acesso pleno',
  `editor` varchar(45) NOT NULL DEFAULT 'basico',
  `permissoes` text,
  `criacao` datetime DEFAULT NULL,
  `modificacao` datetime DEFAULT NULL,
  `ultimo_acesso` datetime DEFAULT NULL,
  `ultimo_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `newsletter` */

DROP TABLE IF EXISTS `newsletter`;

CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `temas` */

DROP TABLE IF EXISTS `temas`;

CREATE TABLE `temas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tema` varchar(255) NOT NULL DEFAULT 'site' COMMENT 'site,natal,etc',
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `videos` */

DROP TABLE IF EXISTS `videos`;

CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `criacao` datetime NOT NULL,
  `v_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;