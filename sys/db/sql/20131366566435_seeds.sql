/* SQL seeds criado em 21/04/2013 14:47:15 */
TRUNCATE TABLE `banners_tipos`;
INSERT INTO `banners_tipos`(`id`,`chave`,`nome`,`largura`,`altura`) VALUES ( NULL,'principal','Principal','1500','250'); 

TRUNCATE TABLE `areas`;
INSERT INTO `areas`(`id`,`titulo`,`contexto`,`status`,`permalink`,`seo_title`,`seo_keywords`,`seo_description`,`back_link`) VALUES ( NULL,'Gerais','geral','1','pgs-gerais','Geral',NULL,NULL,'0'); 
INSERT INTO `areas`(`id`,`titulo`,`contexto`,`status`,`permalink`,`seo_title`,`seo_keywords`,`seo_description`,`back_link`) VALUES ( NULL,'Notícias','index','1','noticias','Notícias',NULL,NULL,'1'); 
INSERT INTO `areas`(`id`,`titulo`,`contexto`,`status`,`permalink`,`seo_title`,`seo_keywords`,`seo_description`,`back_link`) VALUES ( NULL,'Fotos','fotos','1','fotos','Fotos',NULL,NULL,'1'); 

TRUNCATE TABLE `paginas`;
INSERT INTO `paginas`(`id`,`titulo`,`conteudo`,`areas_id`,`status`,`cadastro`,`alteracao`,`descricao`,`seo_title`,`seo_keywords`,`permalink`,`capa`,`ordem`,`data`) VALUES ( NULL,'Quem Somos',NULL,'1','1','2013-04-21 20:44:58',NULL,'','Quem Somos',NULL,'quem-somos',NULL,'0',NULL); 

TRUNCATE TABLE `moderadores`;
INSERT INTO `moderadores`(`id`,`email`,`senha`,`nome`,`status`,`is_root`,`permissoes`,`editor`) VALUES ( NULL,'lsc999@ig.com.br','5416d7cd6ef195a0f7622a9c56b55e84','Suporte','1','1',NULL,'root'); 

TRUNCATE TABLE `config_grupos`;
INSERT INTO `config_grupos`(`id`,`nome`,`status`,`root`) VALUES ( NULL,'Geral','1','0');
INSERT INTO `config_grupos`(`id`,`nome`,`status`,`root`) VALUES ( NULL,'SEO','1','0');

TRUNCATE TABLE `configs`;
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`) VALUES ( 'cfg_contato','contato@mudeme.com.br','string','E-mail para recebimento de contatos',NULL,'1','20');
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'cfg_contato_txt','<p>Your text here</p>','html','Texto da área Contato','root','1','20',NULL);
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'cfg_banner_topo_tempo','5','int','Tempo do banner do topo',NULL,'1','30','Tempo de exibição para cada imagem do banner, em segundos'); 
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'seo_title','Nome do Site','string','Metatag Title',NULL,'2','1','Durante todo o site este título ficará na barra de título dos navegadores.<br/>Coloque o nome do seu site, slogan da empresa, etc. Isto será importante para mecanismos de busca como o Google.');
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'seo_keywords','palavras sobre o site','text','Metatag Keywords',NULL,'2','5','Inclua possíveis termos de busca, conjuntos de palavras-chave e expressões que levariam pessoas à achar seu site. Isto será importante para mecanismos de busca como o Google.');
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'seo_description','meu site','text','Metatag Description',NULL,'2','10','Uma breve descrição de seu site. Isto será importante para mecanismos de busca como o Google.');
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'seo_g_a','<!-- G. A. -->','text','Google Analytics Code',NULL,'2','99','Script de acompanhamento'); 