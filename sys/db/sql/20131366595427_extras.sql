/* SQL extras criado em 21/04/2013 22:50:27 */
DROP TABLE `newsletter`;

CREATE TABLE `inscricoes`( `id` INT(11) NOT NULL AUTO_INCREMENT, `nome` VARCHAR(255) NOT NULL, `cidade` VARCHAR(255) NOT NULL, `estado` varchar(45) NOT NULL,`email` VARCHAR(255) NOT NULL, `cadastro` DATETIME, PRIMARY KEY (`id`) ) ENGINE=MYISAM CHARSET=utf8 COLLATE=utf8_general_ci; 

/* seeds extras */
INSERT INTO `config_grupos`(`id`,`nome`,`status`,`root`) VALUES ( 3,'Links no rodapé','1','0'); 
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'rp_facebook','#','string','URL no facebook',NULL,'3','10',NULL); 
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'rp_twitter','#','string','URL no twitter',NULL,'3','10',NULL); 
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'rp_google','#','string','URL no Google+',NULL,'3','10',NULL); 
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'rp_instagram','#','string','URL no Instagram',NULL,'3','10',NULL); 
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'rp_linkedin','#','string','URL no Linkedin',NULL,'3','10',NULL); 
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'rp_pinterest','#','string','URL no Pinterest',NULL,'3','10',NULL); 

INSERT INTO `config_grupos`(`id`,`nome`,`status`,`root`) VALUES ( 4,'Tela Cadastre-se','1','0'); 
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'cad_texto_intro','<p>Texto de Junte-se a nós!</p>','html','Texto de apresentação','root','4','10',NULL); 
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'cad_texto_home','<p>Texto home. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, neque rutrum erat, eu congue orci lorem eget lorem.</p>','text','Texto para Home',NULL,'4','5',NULL); 
INSERT INTO `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) VALUES ( 'cad_texto_final','<p>Obrigado por se cadastrar!</p>','html','Texto exibido após cadastro','root','4','20',NULL); 
