/*
SQLyog Community v12.03 (64 bit)
MySQL - 5.6.21 : Database - analista_mais
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`analista_mais` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `analista_mais`;

/*Table structure for table `areas` */

DROP TABLE IF EXISTS `areas`;

CREATE TABLE `areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `contexto` varchar(255) DEFAULT 'index' COMMENT 'ex.: contexto = teste a view de contato seria algo como contato_teste.phtml',
  `status` int(11) NOT NULL DEFAULT '0',
  `permalink` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) NOT NULL DEFAULT 'Área',
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `back_link` int(1) NOT NULL DEFAULT '0' COMMENT '1 = os filhos terão o permalink da área. Ex.: area/pagina-teste',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `areas` */

LOCK TABLES `areas` WRITE;

insert  into `areas`(`id`,`titulo`,`contexto`,`status`,`permalink`,`seo_title`,`seo_keywords`,`seo_description`,`back_link`) values (1,'Gerais','geral',1,'pgs-gerais','Geral',NULL,NULL,0);
insert  into `areas`(`id`,`titulo`,`contexto`,`status`,`permalink`,`seo_title`,`seo_keywords`,`seo_description`,`back_link`) values (2,'Notícias','index',1,'noticias','Notícias',NULL,NULL,1);
insert  into `areas`(`id`,`titulo`,`contexto`,`status`,`permalink`,`seo_title`,`seo_keywords`,`seo_description`,`back_link`) values (3,'Fotos','fotos',1,'fotos','Fotos',NULL,NULL,1);

UNLOCK TABLES;

/*Table structure for table `banner_tipos` */

DROP TABLE IF EXISTS `banner_tipos`;

CREATE TABLE `banner_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chave` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `largura` int(11) NOT NULL DEFAULT '300',
  `altura` int(11) NOT NULL DEFAULT '300',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `banner_tipos` */

LOCK TABLES `banner_tipos` WRITE;

insert  into `banner_tipos`(`id`,`chave`,`nome`,`largura`,`altura`) values (1,'principal','Principal',1500,250);

UNLOCK TABLES;

/*Table structure for table `banners` */

DROP TABLE IF EXISTS `banners`;

CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `banner_tipos_id` int(11) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `target` varchar(255) NOT NULL DEFAULT '_self',
  `status` int(1) NOT NULL DEFAULT '0',
  `ordem` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `banners` */

LOCK TABLES `banners` WRITE;

insert  into `banners`(`id`,`titulo`,`banner_tipos_id`,`imagem`,`url`,`target`,`status`,`ordem`) values (1,'Exemplo paisagem',1,'uploads/banners/517480b6cdc1f.jpg','https://www.google.com.br','_self',1,0);
insert  into `banners`(`id`,`titulo`,`banner_tipos_id`,`imagem`,`url`,`target`,`status`,`ordem`) values (2,'Exemplo vermelho',1,'uploads/banners/5177444bc893d.jpg',NULL,'_self',1,0);

UNLOCK TABLES;

/*Table structure for table `comentarios` */

DROP TABLE IF EXISTS `comentarios`;

CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `criacao` datetime NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mensagem` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `paginas_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `comentarios` */

LOCK TABLES `comentarios` WRITE;

insert  into `comentarios`(`id`,`criacao`,`nome`,`email`,`mensagem`,`status`,`paginas_id`) values (1,'2013-07-09 21:06:36','TEste','teste@teste.com','EEEEEEEEeieeeeeu\r\nipoipoi',1,8);
insert  into `comentarios`(`id`,`criacao`,`nome`,`email`,`mensagem`,`status`,`paginas_id`) values (3,'2013-05-05 01:43:47','Leandrovsky','lsc999@ig.com.br','asdasdhasudhsadsda\r\nasdasdd',0,8);

UNLOCK TABLES;

/*Table structure for table `config_grupos` */

DROP TABLE IF EXISTS `config_grupos`;

CREATE TABLE `config_grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '0-oculto,1-visivel no admin',
  `root` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-somente root vai ver',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `config_grupos` */

LOCK TABLES `config_grupos` WRITE;

insert  into `config_grupos`(`id`,`nome`,`status`,`root`) values (1,'Geral',1,0);
insert  into `config_grupos`(`id`,`nome`,`status`,`root`) values (2,'SEO',1,0);
insert  into `config_grupos`(`id`,`nome`,`status`,`root`) values (3,'Links rodapé',1,0);
insert  into `config_grupos`(`id`,`nome`,`status`,`root`) values (4,'Tela Cadastre-se',1,0);

UNLOCK TABLES;

/*Table structure for table `configs` */

DROP TABLE IF EXISTS `configs`;

CREATE TABLE `configs` (
  `chave` varchar(255) NOT NULL,
  `valor` text,
  `tipo` varchar(255) NOT NULL DEFAULT 'string' COMMENT 'int,string,text,html,file,boolean,select,radio ou hidden',
  `descricao` varchar(255) DEFAULT NULL COMMENT 'descrição pra área admin',
  `options` varchar(255) DEFAULT NULL COMMENT 'opções personalizadas ex: array(0=>Não,1=>Sim) pro tipo select ou [comum] pro tipo html',
  `config_grupos_id` int(11) NOT NULL,
  `ordem` tinyint(4) NOT NULL DEFAULT '99',
  `help` text COMMENT 'texto livre para ajudar usuários',
  PRIMARY KEY (`chave`),
  KEY `FK_config` (`config_grupos_id`),
  CONSTRAINT `FK_configs` FOREIGN KEY (`config_grupos_id`) REFERENCES `config_grupos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `configs` */

LOCK TABLES `configs` WRITE;

insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('cad_texto_final','<p>Obrigado por se cadastrar!</p>','html','Texto exibido após cadastro','root',4,20,NULL);
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('cad_texto_home','<p>Texto home. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, neque rutrum erat, eu congue orci lorem eget lorem.</p>','text','Texto para Home',NULL,4,5,NULL);
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('cad_texto_intro','<p>Texto de Junte-se a nós!</p>','html','Texto de apresentação','root',4,10,NULL);
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('cfg_contato','contato@mudeme.com.br','string','E-mail para recebimento de contatos',NULL,1,20,NULL);
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('cfg_contato_txt','<p>\r\n	Curabitur turpis orci, gravida non ornare id, venenatis nec enim. Praesent posuere condimentum leo eget volutpat. Maecenas in ligula in lacus aliquam ultricies scelerisque vitae nisl. Pellentesque quam dui, cursus in mollis sed, pretium quis dui. Proin ac nibh et leo malesuada congue.</p>\r\n<address>\r\n	Endere&ccedil;o:<br />\r\n	Rua Dom Cabral, 245 - sala 101<br />\r\n	Funcion&aacute;rios - Belo Horizonte/MG<br />\r\n	CEP 30.854-785<br />\r\n	<br />\r\n	Telefone:<br />\r\n	+ 55 31 3245-6598</address>\r\n','html','Texto da área Contato','root',1,20,NULL);
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('cfg_yumpu_token','fVIURBxgpLsPpvimtz846a5oHtiW01MQ','string','Access token - yumpu.com',NULL,1,30,'Obtenha em https://www.yumpu.com/pt/account/profile/api com seu login');
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('rp_facebook','https://www.facebook.com/FerramentasParaWeb','string','URL no facebook',NULL,3,10,NULL);
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('rp_google','#','string','URL no Google+',NULL,3,10,NULL);
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('rp_instagram','#','string','URL no Instagram',NULL,3,10,NULL);
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('rp_linkedin','#','string','URL no Linkedin',NULL,3,10,NULL);
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('rp_pinterest','#','string','URL no Pinterest',NULL,3,10,NULL);
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('rp_twitter','#','string','URL no twitter',NULL,3,10,NULL);
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('seo_description','meu site','text','Metatag Description',NULL,2,10,'Uma breve descrição de seu site. Isto será importante para mecanismos de busca como o Google.');
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('seo_g_a','123456','int','Numero da conta no Google Analytics',NULL,2,99,'Acesse seu painel do google analytics e coloque o numero de seu usuario aqui. Ex.: UX-1231456');
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('seo_keywords','palavras sobre o site','text','Metatag Keywords',NULL,2,5,'Inclua possíveis termos de busca, conjuntos de palavras-chave e expressões que levariam pessoas à achar seu site. Isto será importante para mecanismos de busca como o Google.');
insert  into `configs`(`chave`,`valor`,`tipo`,`descricao`,`options`,`config_grupos_id`,`ordem`,`help`) values ('seo_title','Analista +','string','Metatag Title',NULL,2,1,'Durante todo o site este título ficará na barra de título dos navegadores.<br/>Coloque o nome do seu site, slogan da empresa, etc. Isto será importante para mecanismos de busca como o Google.');

UNLOCK TABLES;

/*Table structure for table `contextos` */

DROP TABLE IF EXISTS `contextos`;

CREATE TABLE `contextos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chave` varchar(255) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `contextos` */

LOCK TABLES `contextos` WRITE;

UNLOCK TABLES;

/*Table structure for table `documentos` */

DROP TABLE IF EXISTS `documentos`;

CREATE TABLE `documentos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `status` char(1) NOT NULL DEFAULT '0',
  `doc_id` int(10) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `embed_code` varchar(255) DEFAULT NULL,
  `image_big` varchar(255) DEFAULT NULL,
  `image_small` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `documentos` */

LOCK TABLES `documentos` WRITE;

insert  into `documentos`(`id`,`status`,`doc_id`,`url`,`title`,`embed_code`,`image_big`,`image_small`,`create_date`,`data`) values (1,'1',52505737,'http://www.yumpu.com/pt/document/view/52505737/manual-do-ubuntupdf','Manual_do_Ubuntu.pdf','<iframe width=','http://img.yumpu.com/52505737/1/1129x1600/manual-do-ubuntupdf.jpg','http://img.yumpu.com/52505737/1/115x163/manual-do-ubuntupdf.jpg','2015-08-05 05:07:41','2015-08-05');
insert  into `documentos`(`id`,`status`,`doc_id`,`url`,`title`,`embed_code`,`image_big`,`image_small`,`create_date`,`data`) values (2,'1',52505737,'http://www.yumpu.com/pt/document/view/52505737/manual-do-ubuntupdf','Manual_do_Ubuntu 2','<iframe width=\"512px\" height=\"384px\" src=\"https://www.yumpu.com/pt/embed/view/AQiTtttrIfPVEAlO\" frameborder=\"0\" allowfullscreen=\"true\" allowtransparency=\"true\"></iframe>','http://img.yumpu.com/52505737/1/1129x1600/manual-do-ubuntupdf.jpg','http://img.yumpu.com/52505737/1/115x163/manual-do-ubuntupdf.jpg','2015-08-05 05:07:41','2015-07-05');
insert  into `documentos`(`id`,`status`,`doc_id`,`url`,`title`,`embed_code`,`image_big`,`image_small`,`create_date`,`data`) values (3,'1',52506940,'http://www.yumpu.com/en/document/view/52506940/anexo-01-processamento-em-lotes-v1','Anexo 01 Processamento em Lotes V1','<iframe width=\"512px\" height=\"384px\" src=\"https://www.yumpu.com/en/embed/view/com4xT9ih5Cu0KCc\" frameborder=\"0\" allowfullscreen=\"true\" allowtransparency=\"true\"></iframe>','http://img.yumpu.com/52506940/1/1240x1600/anexo-01-processamento-em-lotes-v1pdf.jpg','http://img.yumpu.com/52506940/1/117x151/anexo-01-processamento-em-lotes-v1pdf.jpg','2015-08-06 04:33:58','2015-08-06');

UNLOCK TABLES;

/*Table structure for table `inscricoes` */

DROP TABLE IF EXISTS `inscricoes`;

CREATE TABLE `inscricoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `estado` varchar(45) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `inscricoes` */

LOCK TABLES `inscricoes` WRITE;

insert  into `inscricoes`(`id`,`nome`,`cidade`,`estado`,`email`,`cadastro`) values (1,'TEste','BH','MG','teste@teste.com','2013-04-22 00:08:08');
insert  into `inscricoes`(`id`,`nome`,`cidade`,`estado`,`email`,`cadastro`) values (4,'Leandro','Belo Horizonte','MG','teste@teste.com.br','2013-04-24 23:16:05');
insert  into `inscricoes`(`id`,`nome`,`cidade`,`estado`,`email`,`cadastro`) values (5,'Teste','Afogados da Ingazeira','GO','teste@teste.com.br','2013-04-24 23:27:34');
insert  into `inscricoes`(`id`,`nome`,`cidade`,`estado`,`email`,`cadastro`) values (6,'Cliente Teste','Vitória','ES','sirbelmonth@yahoo.com.br','2013-04-24 23:34:15');
insert  into `inscricoes`(`id`,`nome`,`cidade`,`estado`,`email`,`cadastro`) values (7,'Outra Categs','Belo Horizonte','MG','teste@teste.com.br','2013-04-25 01:11:27');
insert  into `inscricoes`(`id`,`nome`,`cidade`,`estado`,`email`,`cadastro`) values (8,'Outra Categs','Areial','PB','teste2@teste.com.br','2013-04-25 01:15:06');

UNLOCK TABLES;

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `target` varchar(255) DEFAULT NULL,
  `classe` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `menu_id` int(11) DEFAULT NULL,
  `ordem` tinyint(3) NOT NULL DEFAULT '99',
  `chave` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `menu` */

LOCK TABLES `menu` WRITE;

UNLOCK TABLES;

/*Table structure for table `midias` */

DROP TABLE IF EXISTS `midias`;

CREATE TABLE `midias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caminho` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `cadastro` datetime NOT NULL,
  `descricao` text,
  `paginas_id` int(11) NOT NULL,
  `ordem` tinyint(3) NOT NULL DEFAULT '99',
  PRIMARY KEY (`id`),
  KEY `FK_midias_idx` (`paginas_id`),
  CONSTRAINT `FK_midias` FOREIGN KEY (`paginas_id`) REFERENCES `paginas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `midias` */

LOCK TABLES `midias` WRITE;

insert  into `midias`(`id`,`caminho`,`titulo`,`tipo`,`cadastro`,`descricao`,`paginas_id`,`ordem`) values (1,'uploads/midias/517488ee033d6.jpg','Koala','1','2013-04-21 21:48:46',NULL,5,99);
insert  into `midias`(`id`,`caminho`,`titulo`,`tipo`,`cadastro`,`descricao`,`paginas_id`,`ordem`) values (2,'uploads/midias/517488eedff4a.jpg','Lighthouse','1','2013-04-21 21:48:46',NULL,5,99);
insert  into `midias`(`id`,`caminho`,`titulo`,`tipo`,`cadastro`,`descricao`,`paginas_id`,`ordem`) values (3,'uploads/midias/517488efb770a.jpg','Penguins','1','2013-04-21 21:48:47',NULL,5,99);
insert  into `midias`(`id`,`caminho`,`titulo`,`tipo`,`cadastro`,`descricao`,`paginas_id`,`ordem`) values (4,'http://www.youtube.com/watch?v=NAinKUU-Mo8','Video de exemplo','2','2013-04-21 21:51:19',NULL,6,99);
insert  into `midias`(`id`,`caminho`,`titulo`,`tipo`,`cadastro`,`descricao`,`paginas_id`,`ordem`) values (5,'uploads/midias/517489dcdfdc6.jpg','Chrysanthemum','1','2013-04-21 21:52:44',NULL,6,99);
insert  into `midias`(`id`,`caminho`,`titulo`,`tipo`,`cadastro`,`descricao`,`paginas_id`,`ordem`) values (6,'uploads/midias/517489ddb17c4.jpg','Desert','1','2013-04-21 21:52:45',NULL,6,99);
insert  into `midias`(`id`,`caminho`,`titulo`,`tipo`,`cadastro`,`descricao`,`paginas_id`,`ordem`) values (7,'uploads/midias/517489de8742c.jpg','Hydrangeas','1','2013-04-21 21:52:46',NULL,6,99);
insert  into `midias`(`id`,`caminho`,`titulo`,`tipo`,`cadastro`,`descricao`,`paginas_id`,`ordem`) values (8,'uploads/midias/51748a1fdf166.rar','2','0','2013-04-21 21:53:51',NULL,6,99);
insert  into `midias`(`id`,`caminho`,`titulo`,`tipo`,`cadastro`,`descricao`,`paginas_id`,`ordem`) values (9,'http://vimeo.com/35396305','Yosemite','2','2013-07-02 02:57:13',NULL,9,99);

UNLOCK TABLES;

/*Table structure for table `moderadores` */

DROP TABLE IF EXISTS `moderadores`;

CREATE TABLE `moderadores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT 'Moderador',
  `email` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `is_root` int(1) NOT NULL DEFAULT '0' COMMENT 'usuarios root com acesso pleno',
  `editor` varchar(45) NOT NULL DEFAULT 'basico',
  `permissoes` text,
  `criacao` datetime DEFAULT NULL,
  `modificacao` datetime DEFAULT NULL,
  `ultimo_acesso` datetime DEFAULT NULL,
  `ultimo_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `moderadores` */

LOCK TABLES `moderadores` WRITE;

insert  into `moderadores`(`id`,`nome`,`email`,`senha`,`status`,`is_root`,`editor`,`permissoes`,`criacao`,`modificacao`,`ultimo_acesso`,`ultimo_ip`) values (1,'Suporte','leandro.fpw@gmail.com','e10adc3949ba59abbe56e057f20f883e',1,1,'root',NULL,NULL,NULL,NULL,'::1');
insert  into `moderadores`(`id`,`nome`,`email`,`senha`,`status`,`is_root`,`editor`,`permissoes`,`criacao`,`modificacao`,`ultimo_acesso`,`ultimo_ip`) values (3,'Teste','teste@teste.com.br','e10adc3949ba59abbe56e057f20f883e',1,0,'basico','a:5:{s:9:\"paginas-2\";a:5:{s:4:\"novo\";b:1;s:8:\"publicar\";b:1;s:5:\"index\";b:1;s:6:\"editar\";b:1;s:7:\"deletar\";b:1;}s:7:\"banners\";a:4:{s:5:\"index\";b:1;s:6:\"editar\";b:1;s:4:\"novo\";b:1;s:7:\"deletar\";b:1;}s:6:\"midias\";a:4:{s:5:\"index\";b:1;s:4:\"novo\";b:1;s:6:\"editar\";b:1;s:7:\"deletar\";b:1;}s:5:\"areas\";a:2:{s:5:\"index\";b:1;s:6:\"editar\";b:1;}s:11:\"moderadores\";a:4:{s:5:\"index\";b:1;s:6:\"editar\";b:1;s:4:\"novo\";b:1;s:7:\"deletar\";b:1;}}',NULL,NULL,NULL,'::1');
insert  into `moderadores`(`id`,`nome`,`email`,`senha`,`status`,`is_root`,`editor`,`permissoes`,`criacao`,`modificacao`,`ultimo_acesso`,`ultimo_ip`) values (4,'Douglas','douglassadesign@gmail.com','e10adc3949ba59abbe56e057f20f883e',1,1,'root','',NULL,NULL,NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `paginas` */

DROP TABLE IF EXISTS `paginas`;

CREATE TABLE `paginas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `conteudo` text,
  `areas_id` int(11) NOT NULL COMMENT 'pra formar contextos diferentes para cada tipo de área',
  `status` int(1) NOT NULL DEFAULT '0',
  `cadastro` datetime NOT NULL,
  `alteracao` datetime DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) NOT NULL DEFAULT 'Titulo',
  `seo_keywords` varchar(255) DEFAULT NULL,
  `permalink` varchar(255) DEFAULT NULL,
  `capa` varchar(255) DEFAULT NULL,
  `ordem` tinyint(3) DEFAULT '99',
  `data` date DEFAULT NULL,
  `proteger` int(1) NOT NULL DEFAULT '0' COMMENT 'eliminar o botao excluir',
  `comentar` int(1) NOT NULL DEFAULT '0' COMMENT 'pode lancar comentarios?',
  PRIMARY KEY (`id`),
  KEY `FK_paginas_idx` (`areas_id`),
  CONSTRAINT `FK_paginas` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `paginas` */

LOCK TABLES `paginas` WRITE;

insert  into `paginas`(`id`,`titulo`,`conteudo`,`areas_id`,`status`,`cadastro`,`alteracao`,`descricao`,`seo_title`,`seo_keywords`,`permalink`,`capa`,`ordem`,`data`,`proteger`,`comentar`) values (3,'Quem Somos','<p>\r\n	A inten&ccedil;&atilde;o deste <em>projeto </em>&eacute; colocar sempre em um &uacute;nico lugar um arquivo javascript que pode ser utilizado em qualquer site para criar duas combo-box (em html, select) com escolha de estados e cidades.<br />\r\n	<br />\r\n	Decidi hosped&aacute;-lo no servidor do google <u>porque </u>assim &eacute; mais improv&aacute;vel que hajam <a href=\"#teste\">problemas </a>de servidor em qualquer servidor que eu hospede. Para instal&aacute;-lo em seu site, coloque a seguinte instru&ccedil;&atilde;o entre suas tags head.</p>\r\n<p>\r\n	A inten&ccedil;&atilde;o deste projeto &eacute; colocar sempre em um &uacute;nico lugar um <strong>arquivo </strong>javascript que pode ser utilizado em qualquer site para criar duas combo-box (em html, select) com escolha de estados e cidades.<br />\r\n	<br />\r\n	Decidi hosped&aacute;-lo no servidor do google porque assim &eacute; mais improv&aacute;vel que hajam problemas de servidor em qualquer servidor que eu hospede. Para instal&aacute;-lo em seu site, coloque a seguinte instru&ccedil;&atilde;o entre suas tags head.</p>\r\n<ul>\r\n	<li>\r\n		item 1</li>\r\n	<li>\r\n		item 2</li>\r\n</ul>\r\n',1,1,'2013-04-21 20:44:58',NULL,'sdhbfhjsdb bfsdbfhjsbd ','Quem Somos','heeeeeiahhh','quem-somos',NULL,0,NULL,1,0);
insert  into `paginas`(`id`,`titulo`,`conteudo`,`areas_id`,`status`,`cadastro`,`alteracao`,`descricao`,`seo_title`,`seo_keywords`,`permalink`,`capa`,`ordem`,`data`,`proteger`,`comentar`) values (4,'Exemplo de Notícia 1','<p>\r\n	Estes marsupiais encontram-se em via de extin&ccedil;&atilde;o desde o in&iacute;cio da coloniza&ccedil;&atilde;o inglesa da Austr&aacute;lia, quando surgiu o h&aacute;bito de mat&aacute;-los para usar sua pele. Hoje, a ca&ccedil;a n&atilde;o &eacute; o maior risco mas sim as queimadas nas florestas, que matam muitos animais, e a elimina&ccedil;&atilde;o das &aacute;rvores onde vivem, tanto por queimadas quanto por lenhadores. Ao perder a sua casa e alimento, o coala se muda e pode chegar a povoamentos ou cidades, onde morre por atropelamento ou &eacute; ca&ccedil;ado por c&atilde;es.</p>\r\n<p>\r\n	Estes marsupiais encontram-se em via de extin&ccedil;&atilde;o desde o in&iacute;cio da coloniza&ccedil;&atilde;o inglesa da Austr&aacute;lia, quando surgiu o h&aacute;bito de mat&aacute;-los para usar sua pele. Hoje, a ca&ccedil;a n&atilde;o &eacute; o maior risco mas sim as queimadas nas florestas, que matam muitos animais, e a elimina&ccedil;&atilde;o das &aacute;rvores onde vivem, tanto por queimadas quanto por lenhadores. Ao perder a sua casa e alimento, o coala se muda e pode chegar a povoamentos ou cidades, onde morre por atropelamento ou &eacute; ca&ccedil;ado por c&atilde;es.</p>\r\n<p>\r\n	Estes marsupiais encontram-se em via de extin&ccedil;&atilde;o desde o in&iacute;cio da coloniza&ccedil;&atilde;o inglesa da Austr&aacute;lia, quando surgiu o h&aacute;bito de mat&aacute;-los para usar sua pele. Hoje, a ca&ccedil;a n&atilde;o &eacute; o maior risco mas sim as queimadas nas florestas, que matam muitos animais, e a elimina&ccedil;&atilde;o das &aacute;rvores onde vivem, tanto por queimadas quanto por lenhadores. Ao perder a sua casa e alimento, o coala se muda e pode chegar a povoamentos ou cidades, onde morre por atropelamento ou &eacute; ca&ccedil;ado por c&atilde;es.</p>\r\n',2,1,'2013-04-21 20:50:36',NULL,'Estes marsupiais encontram-se em via de extinção desde o início da colonização inglesa da Austrália, quando surgiu o hábito de matá-los para usar sua pele.','Exemplo de Notícia 1','coalas, marsupiais','noticias/exemplo-de-noticia-1','uploads/paginas/51747b8bd64dc.jpg',NULL,'2013-04-01',0,0);
insert  into `paginas`(`id`,`titulo`,`conteudo`,`areas_id`,`status`,`cadastro`,`alteracao`,`descricao`,`seo_title`,`seo_keywords`,`permalink`,`capa`,`ordem`,`data`,`proteger`,`comentar`) values (5,'Album de exemplo',NULL,3,1,'2013-04-21 21:35:20',NULL,'tesestestet','Album de exemplo',NULL,'fotos-album-de-exemplo',NULL,NULL,'2013-04-25',0,0);
insert  into `paginas`(`id`,`titulo`,`conteudo`,`areas_id`,`status`,`cadastro`,`alteracao`,`descricao`,`seo_title`,`seo_keywords`,`permalink`,`capa`,`ordem`,`data`,`proteger`,`comentar`) values (6,'Outra Noticia de Exemplo','<p>\r\n	One of the men selected for shipkeepers&mdash;that is, those not appointed to the boats, by this time relieved the Indian at the main-mast head. The sailors at the fore and mizzen had come down; the line tubs were fixed in their places; the cranes were thrust out; the mainyard was backed, and the three boats swung over the sea like three samphire baskets over high cliffs. Outside of the bulwarks their eager crews with one hand clung to the rail, while one foot was expectantly poised on the gunwale. So look the long line of man-of-war&#39;s men about to throw themselves on board an enemy&#39;s ship. But at this critical instant a sudden exclamation was heard that took every eye from the whale. With a start all glared at dark Ahab, who was surrounded by five dusky <a href=\"/\">phantoms </a>that seemed fresh formed out of air. The phantoms, for so they then seemed, were flitting on the other side of the deck, and, with a noiseless celerity, were casting loose the tackles and bands of the boat which swung there. This boat had always been deemed one of the spare boats, though technically called the captain&#39;s, on account of its hanging from the:</p>\r\n<ul>\r\n	<li>\r\n		starboard quarter.</li>\r\n</ul>\r\n<p>\r\n	The figure that now stood by its <strong>bows </strong>was tall and <u><em><strong>swart</strong></em></u>, with one white tooth evilly protruding from its steel-like lips.</p>\r\n',2,1,'2013-04-21 21:49:12',NULL,NULL,'Outra Noticia de Exemplo',NULL,'noticias/outra-noticia-de-exemplo','uploads/paginas/5174897ebe6f1.jpg',NULL,'2013-03-04',0,0);
insert  into `paginas`(`id`,`titulo`,`conteudo`,`areas_id`,`status`,`cadastro`,`alteracao`,`descricao`,`seo_title`,`seo_keywords`,`permalink`,`capa`,`ordem`,`data`,`proteger`,`comentar`) values (7,'Crisântemo como exemplo de notícia','<p>\r\n	ncjkxznckjxz</p>\r\n',2,1,'2013-04-24 01:53:18',NULL,'asbhdjbasjhdb kk','Crisântemo como exemplo de notícia',NULL,'noticias/crisantemo-como-exemplo-de-noticia','uploads/paginas/517765900f7fa.jpg',NULL,'2013-04-01',0,0);
insert  into `paginas`(`id`,`titulo`,`conteudo`,`areas_id`,`status`,`cadastro`,`alteracao`,`descricao`,`seo_title`,`seo_keywords`,`permalink`,`capa`,`ordem`,`data`,`proteger`,`comentar`) values (8,'Deserto como exemplo de notícia','<p>\r\n	iiiii exemplo</p>\r\n',2,1,'2013-04-24 01:54:47',NULL,'ashduashduh audhasuhd kkk','Deserto como exemplo de notícia',NULL,'noticias/deserto-como-exemplo-de-noticia','uploads/paginas/517765b76b336.jpg',NULL,'2013-04-03',0,0);
insert  into `paginas`(`id`,`titulo`,`conteudo`,`areas_id`,`status`,`cadastro`,`alteracao`,`descricao`,`seo_title`,`seo_keywords`,`permalink`,`capa`,`ordem`,`data`,`proteger`,`comentar`) values (9,'Hortênsia como notícia','<p>\r\n	testsset exemplo</p>\r\n',2,1,'2013-04-24 01:55:28',NULL,'desccscs','Hortênsia como exemplo de notícia',NULL,'noticias/hortensia-como-exemplo-de-noticia','uploads/paginas/517765e2d13c2.jpg',NULL,'2013-04-11',0,0);

UNLOCK TABLES;

/*Table structure for table `temas` */

DROP TABLE IF EXISTS `temas`;

CREATE TABLE `temas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tema` varchar(255) NOT NULL DEFAULT 'site' COMMENT 'site,natal,etc',
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `temas` */

LOCK TABLES `temas` WRITE;

UNLOCK TABLES;

/*Table structure for table `videos` */

DROP TABLE IF EXISTS `videos`;

CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `criacao` datetime NOT NULL,
  `v_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `videos` */

LOCK TABLES `videos` WRITE;

insert  into `videos`(`id`,`titulo`,`url`,`status`,`criacao`,`v_id`) values (2,'Eye of God','http://www.youtube.com/watch?v=NAinKUU-Mo8&teste=0',1,'2013-04-22 03:25:00','NAinKUU-Mo8');
insert  into `videos`(`id`,`titulo`,`url`,`status`,`criacao`,`v_id`) values (3,'Yosemite','http://vimeo.com/35396305',1,'2013-07-01 05:20:00',NULL);

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
